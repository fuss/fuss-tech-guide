Documentazione FUSS - Guida per il tecnico
==========================================

Qui puoi trovare la documentazione rivolta ai tecnici FUSS.

La documentazione è scritta in `reStructuredText <http://www.sphinx-doc.org/rest.html>`_.

Contribuisci
------------

Chiunque può contribuire a migliorare la documentazione.

Supporto
--------

Se ti serve aiuto, scrivi una mail ad info@fuss.bz.it

Licenze
-------

.. image:: https://img.shields.io/badge/code-GPLv3-blue.svg
    :target: https://www.gnu.org/licenses/gpl-3.0.en.html
    :alt: Code GPLv3

.. image:: https://img.shields.io/badge/documentation-CC%20BY--SA-lightgrey.svg
    :target: https://creativecommons.org/licenses/by-sa/4.0/
    :alt: Documentation CC BY-SA
