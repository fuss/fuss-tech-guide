*********
Politiche
*********

Questa sezione documenta le politiche adottate nelle scuole che adottano
FUSS, con informazioni sulle loro implementazioni.

Specifiche autenticazione
=========================

Con la versione 7.x del fuss-server sono state introdotti dei criteri di
gestione delle credenziali di autenticazione per essere conformi ai
requisiti correnti dettati dalla normativa sulla privacy, in particolare
per quanto riguarda la complessità e la scadenza delle password.

Sono soggetti a tali requisiti soltanto i docenti (in quanto gli studenti non
trattano dati altrui). Pertanto si è utilizzato un valore specifico
dell'attributo multivalore *unità operativa*, ``ou`` (assegnabile ad ogni
utente) per distinguere studenti e docenti. In particolare questo dovrè essere
impostato secondo la seguente tabella:

+----------+-------------------+
| Docenti  | ``ou=docenti``    |
+----------+-------------------+
| Studenti | ``ou=studenti``   |
+----------+-------------------+

Il sistema di gestione delle password coi comandi di sistema è stato
impostato usando la seguente riga di in /etc/pam.d/common-password::

   required pam_cracklib.so retry=3 minlen=8 difok=3 minclass=3

che implica lunghezza minima di otto caratteri, non ripetizione di più
di tre caratteri nel cambio password, presenza di almeno tre fra le
quattro classi di caratteri: maiuscole, minuscole, numeri,
punteggiatura.

L'applicativo web per il cambio password consente di evitare queste
restrizioni ma si applica solo agli utenti che hanno attributo
``ou=studenti``.

Per la scadenza delle password questa viene impostata in fase di creazione di
un utente LDAP impostando un valore in giorni nell'attributo ``ShadowMax``
dello stesso, lo stesso valore viene applicato.  I default impostati dalla
configurazione del FUSS server non prevedono la scadenza delle password, in
quanto questa non è necessaria per la maggior parte degli utenti (che sono gli
studenti). In particolare questa non viene impostata se si usano i comandi del
pacchetto ``smbldap-tools``.  Qualora si debba intervenire manualmente
occorre usare lo script ``chage.py`` installato come gli altri sotto
``/usr/share/fuss-server/scripts/``, specificando un valore in giorni con
l'opzione ``-M``.


Octofuss
--------

Octofuss imposta automaticamente il valore della ``ou=`` corrispondente
ad un utente, sulla base dei gruppi cui questo appartiene. Se l'utente
viene messo in un gruppo secondario chiamato '``docente``' o
'``docenti``', ottiene la ``ou=docenti``. Se non si trova in questi
gruppi, allora ottiene la ``ou=studenti``.

Allo stesso tempo per i docenti viene automaticamente impostato un valore di
scadenza della password di 90 giorni, come previsto dalla normativa della
privacy. Per gli studenti invece viene usato un default di 99999 giorni, che
implica la non scadenza della stessa.

Accesso a internet
==================

Le politiche di accesso ad internet impostate da fuss-server sono:

* Gli utenti non autenticati non possono navigare, con l'eccezione dei vari
  repository di software che sono autorizzati per semplicità di installazione,
  elencati nella ACL ``repository`` dentro ``/etc/squid3/squid.conf``, più
  quelli eventualmente aggiunti (funzionalità disponibile dalla versione
  8.0.38 del fuss server) in ``/etc/squid3/squid-added-repo.conf``.
* Gli utenti autenticati escono solo se fanno parte del gruppo ``internet``
  (locale) del fuss-server (permesso ``internet`` dentro Octonet).
* Nella configurazione del proxy è possibile restringere l'accesso alla rete
  Internet in orario ristretto da lunedì a venerdì dalle 7.00 alle 22.00 ed il
  sabato dalle 7.00 alle 14.00, per ottenere questa restrizione occorre
  modificare la configurazione di Squid, sostituendo in
  ``/etc/squid3/squid.conf`` la riga::

    http_access allow password internet

  con la riga::

    http_access allow password internet orario

* Gli accessi ai siti internet vengono registrati nei file di log del sistema.
* La navigazione su Internet viene svolta in modalità protetta (usando il
  filtro sui contenuti di Dansguardian/E2guardian). A partire dalla versione
  8.0.38 del fuss-server questa restrizione viene disabilitata di default
  sulla rete locale in quanto esiste già un filtro a monte della rete delle
  scuole, si può riabilitare l'uso del filtro modificando la variabile
  ``dans_exclude_localnet`` in ``fuss-server-defaults.yaml`` (vedi `File dei
  default del Fuss Server`).
* L'accesso ad ``internet`` è di default bloccata per le macchine Windows, lo
  si può riabilitare  modificando
  la variabile ``proxy_win_exclude`` in  ``fuss-server-defaults.yaml``
  (vedi `file-default-fuss-server`).


Verifiche
---------

È importante eseguire tutte le verifiche indicate per essere sicuri che
il proxy funzioni veramente.

* provare a navigare con un utente autorizzato (cioè inserito nel gruppo
  ``internet``) indicando correttamente username e password;
* provare a navigare con un utente autorizzato indicando correttamente
  username ma sbagliando la password;
* provare a navigare con un utente non autorizzato (cioè non inserito nel
  gruppo ``internet``) indicando correttamente username e password;
* provare a navigare con un utente autorizzato utilizzando le sue
  credenziali corrette, poi provare a toglierlo dal gruppo ``internet`` e
  verificare se il proxy lo blocca.

Si tenga presente che la rimozione o l'aggiunta al gruppo ``internet`` non
hanno effetto immediato. Se questa viene effettuate tramite Octonet o
``octofussctl`` infatti il permesso deve essere propagato da
``octofuss-client`` che può richiedere fino a 5 minuti, inoltre gli helpers
di Squid devono vedere i nuovi permessi (finché non terminano viene visto il
valore precedente) e occorre che la cache di ``nscd`` sia rinnovata.

Pertanto se si vuole essere sicuri che il permesso sia propagato subito
occorre (a partire dalla versione 8.0.42 di ``fuss-server``) fare
restart del servizio ``propagate-net-perm`` (disponibile anche tra i
servizi riavviabili dall'interfaccia di Octonet).

Per le versioni precedenti i comandi equivalenti sono::

  octofuss-client --dryrun
  nscd -i group
  systemctl stop squid3
  systemctl start squid3


FTP
---

Per abilitare il traffico FTP è necessario permettere il traffico di
questo servizio attraverso il firewall (sblocco porta 21/tcp).
