FUSS - Manuale per il tecnico
=============================

.. figure:: images/fuss.png
      :width: 100px
      :align: right

Il presente manuale è una guida rivolta ai tecnici FUSS per la
manutenzione di un'installazione `FUSS <https://fuss.bz.it>`_ GNU/Linux
basata su Debian 12 "Bookworm".

La versione più recente di questo manuale è leggibile all'indirizzo
https://fuss-tech-guide.readthedocs.io/it/latest/

.. toctree::
   :maxdepth: 2

   quick-install
   architettura
   gestione-dei-fuss-server
   gestione-dei-fuss-client
   interfaccia-di-gestione-octonet
   standard-installazioni-fuss
   fuss-manager
   installazioni_specializzate/index
   bug-reporting
   politiche
   restore
   fuss-server-version-upgrade
   miniguide_hardware/index
   miniguide_software/index


Contribuisci
""""""""""""

Chiunque può contribuire a migliorare questa documentazione che è scritta in `reStructuredText <http://www.sphinx-doc.org/rest.html>`_.

Autori
~~~~~~

Le seguenti persone hanno partecipato alla stesura di questa guida.

* Paolo Baratta
* Claudio Cavalli
* Piergiorgio Cemin
* Paolo Dongilli
* Donato Florio
* Elena Grandi, Truelite Srl
* Marco Marinello
* Davide Nobile
* Andrea Padovan
* Simone Piccardi, Truelite Srl
* Michael Von Guggenberg
* Fabian Zwischenbrugger

I dettagli e lo storico dei contributi sono disponibili sul `repository
git <https://gitlab.com/fusslab/fuss-tech-guide>`_.


Supporto
""""""""""""

Se ti serve aiuto, scrivi una mail ad info@fuss.bz.it

Licenze
"""""""""""

.. image:: https://img.shields.io/badge/code-GPLv3-blue.png
    :target: https://www.gnu.org/licenses/gpl-3.0.en.html
    :alt: Code GPLv3

.. image:: https://img.shields.io/badge/documentation-CC%20BY--SA-lightgrey.png
    :target: https://creativecommons.org/licenses/by-sa/4.0/
    :alt: Documentation CC BY-SA
