Modificare il tipo di disco del FUSS Server
=================================================

Questa modifica ha senso soltanto se lo storage per i dischi è di tipo
LVM-thin, qualora si stia usando uno storage di tipo LVM semplice la procedura
è inutile.

Se si è installato il FUSS server o una qualunque altra macchina virtuale
indicando come tipologia dell'hardware del disco `VirtIO` o `VirtIO block`
(dispositivo a blocchi virtuale) non sarà possibile possibile utilizzare il
comando ``fstrim`` per liberare l'occupazione di spazio disco da LVM. Questa
infatti è realizzata soltanto se disponibile il comando SCSI `discard`, che è
supportato solo dalla tipologia di disco `VirtIO SCSI` che è il default con
Proxmox 5.

Per effettuare il cambiamento occorre spegnere la macchina, se il suo
identificativo è 100 questo si fa a riga di comando con ``qm shutdown 100``
dopo di che si dovrà modificare manualmente il relativo file di
configurazione che è ``/etc/pve/qemu-server/100.conf``. L'operazione deve
essere effettuata a macchina spenta.

Nel caso si sia configurata la macchina inizialmente con `VirtIO block` il
contenuto del file avrà come configurazione per l'identificazione del disco
qualcosa del tipo::

  ...
  bootdisk: virtio0
  ...
  virtio0: local-lvm:vm-100-disk-0,size=32G
  ...

e comparirà nell'interfaccia web (selezionando la macchina e poi il menù
hardware) indicato come in figura:

.. figure:: images/virtio-block-disk.png

Per poter riconfigurare la macchina per l'uso di `VirtIO SCSI` occorre
anzitutto sostituire ``virtio0`` con ``scsi0`` (si sta facendo l'ipotesi di un
solo disco virtuale, se sono più di uno l'operazione andrà ripetuta per tutti,
indicando con ``bootdisk`` quale deve essere usato come disco di avvio);
inoltre deve essere indicato il tipo di supporto SCSI con ``scsihw`` (se non
presente).

In sostanza il file di configurazione dovrà essere modificato per contenere
qualcosa del tipo::

  ...
  bootdisk: scsi0
  ...
  scsihw: virtio-scsi-pci
  ...
  scsi0: local-lvm:vm-100-disk-0,size=32G
  ...

Per precauzione comunque si tenga una copia dell'originale, in questo modo si
potrà tornare indietro in qualunque momento semplicemente ricopiando indietro
la copia e ripristinando il contenuto precedente.

Una volta eseguite le modifiche nell'interfaccia web il disco verrà mostrato
nella sezione hardware della macchina virtuale come:

.. figure:: images/virtio-scsi-disk.png

e selezionandolo si potrà attivare l'uso del comando `discard`, con:

.. figure:: images/virtio-scsi-disk-discard.png

questa ultima operazione comunque si può effettuare anche direttamente in fase
di modifica, utilizzando come specificazione del disco::

  scsi0: local-lvm:vm-100-disk-0,discard=on,size=32G

invece del valore precedente. A questo punto una volta riavviato il server si
potrà utilizzare ``fstrim``.
