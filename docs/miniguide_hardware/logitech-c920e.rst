
Attivazione microfono webcam Logitech C920e
==========================================================

Il microfono della webcam Logitech C920e è disattivato per impostazione predefinita e può essere attivato solo utilizzando il software proprietario Logi Tune (Windows, Mac OS). 
Tuttavia uno script di Python scaricabile dal seguente link :

 `<https://github.com/MavChtz/C920e/>`_

permette di aggirare il problema e attivare il microfono lanciando un semplice comando.

Procedura
--------------------

Installare la seguente dipendenza se non già presente: ::

    sudo apt install git python3-usb

Scaricare la cartella contenente lo script: ::

    git clone https://github.com/MavChtz/C920e

Spostarsi nella cartella ::

    cd C920e

Attivare il microfono col comando: ::

    sudo ./C920e.py on

È poi sufficiente scollegare e ricollegare la webcam ed il microfono è già funzionante.

