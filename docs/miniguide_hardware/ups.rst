UPS
===

Premessa
--------

Attualmente in varie scuole sono collegati ai server vari UPS di questo
tipo, senza un setup che permetta lo spegnimento automatico in caso di
una prolungata assenza di corrente elettrica.


Il setup è stato testato *con collegamento USB* su sistema operativo
*Debian Stretch* (9.4 – ProxMox 5.x).

I modelli di UPS utilizzati per il test sono i seguenti:

* UPS 800 Raptor/Groups
   .. figure:: images/ups-raptor-groups-800.jpg
      :width: 50%
* UPS 800 Riello
   .. figure:: images/ups-riello-800.png
      :width: 50%
* UPS 1kVA Braga Moro
   .. figure:: images/ups-braga-moro-1kVA.png
      :width: 50%

Configurazione
--------------

Installazione del programma ``nut`` (Network UPS Tools)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Lanciare il comando::

   # apt install nut

Configurazione del file ``/etc/nut/ups.conf``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Alla fine del file aggiungere::

   [RIELLO]
      driver = riello_usb
      port = auto
      ignorelb
      override.battery.charge.low = 20
      override.battery.runtime.low = -1

Il nome ``RIELLO`` è una scelta come un’altra. Per i Raptor si puo
mettere ``RAPTOR`` o qualsiasi altro nome.

.. note::

    Il parametro ``override.battery.charge.low = 20`` è quello su cui si
    agisce per determinare a che stato di carica far eseguire lo
    shutdown al sistema operativo.  Se non indicato come nell'esempio il
    default è il 10% ( si potrebbe aumentare il valore per garantire uno
    shutdown sicuro).

.. note::

   *    Per RAPTOR il driver da mettere è ``usbhid-ups`` anziché ``riello_usb``
   *    Per BRAGA MORO il driver (compatibile) è ``blazer_usb``


Fatta la configurazione lanciamo il comando::

  #  upsdrvctl start

e dovremmo ottenere qualcosa del tipo::

   Network UPS Tools - UPS driver controller 2.7.2
   Network UPS Tools - Device simulation and repeater driver 0.13 (2.7.2)

Configurazione del file ``/etc/nut/upsd.conf``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Qui scommentiamo la riga::

   LISTEN 127.0.0.1 3493

Configurazione del file ``/etc/nut/nut.conf``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Qui cambiamo il parametro ``MODE=none`` in ``MODE=standalone``.

Fatto questo lanciamo il comando ``upsd`` (per avviare il demone).
Dovremmo cosí ottnere qualcosa del tipo::

   Network UPS Tools upsd 2.7.2
   fopen /var/run/nut/upsd.pid: No such file or directory
   listening on 127.0.0.1 port 3493
   Connected to UPS [RIELLO]: dummy-ups-RIELLO

.. note::

    Nel caso che il comando dia un errore per via del fatto che c'è giá
    un'istanza attiva, basterá lanciare il comando::

       # upsd -c reload

Verifica dello stato delle cose
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Con il comando ``upsc`` seguito dal nome dato all'UPS (nel file
ups.conf) possiamo monitorare lo stato dell'apparecchio. Ad esempio::

   # upsc RIELLO

Possiamo per esempio vedere se l'UPS è alimentato dalla rete elettrica
(``ups.status: OL``) oppure sta prendendo energia dalla batteria
(``ups.status: OB DISCHRG``).

Creazione di un utente di monitoring (file ``/etc/nut/upsd.users``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Aggiungere alla fine del file::

   [monuser]
       password = mypass
       actions = SET
       instcmds = ALL
       upsmon master

.. note::

   Non si tratta di creare un utente sul sistema, ma solo nel file
   appena visto.

Facciamo quindi un reload della configurazione con::

   # upsd -c reload

Creazione di una direttiva di monitoring (``/etc/nut/upsmon.conf``)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Qui ritroviamo l'utente che abbiamo creato nel file precedente
(``upsd.users``).

Alla fine del file aggiungere::

   MONITOR RIELLO 1 monuser mypass master

Fatto questo, lanciare il comando ``upsmon``.

Ad un eventuale riavvio della macchina non dovrebbe essere necessario
lanciare i vari comandi (da verificare… con un ultimo test).
