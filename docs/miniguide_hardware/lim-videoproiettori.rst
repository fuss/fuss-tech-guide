LIM, videoproiettori interattivi e MIM
======================================

Al termine della configurazione di ciascun modello, eseguire
all'occorrenza una calibrazione come indicato nella sezione omonima.

Riguardo all'utilizzo delle stesse, per una questione di uniformità
nelle diverse scuole, si conviene di utilizzare il software OpenBoard e
non prodotti proprietari forniti in dotazione con le stesse LIM.

Lavagne SMART BOARD
-------------------

SMART Board 480
^^^^^^^^^^^^^^^

In via di elaborazione

SMART Board M600 (con proiettore Epson EB-475W)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In via di elaborazione

.. _`LIM SMART Board SB680` :

SMART Board SB680
^^^^^^^^^^^^^^^^^

Per questo modello si seguano i seguenti passi:

    - installare il pacchetto xserver-xorg-input-evdev::

        apt install xserver-xorg-input-evdev

    - ridenominare il file /usr/share/X11/xorg.conf.d/10-evdev.conf
      aumentando il prefisso numerico in modo tale che sia superiore a
      40 (quello del file 40-libinput.conf)::

        mv /usr/share/X11/xorg.conf.d/10-evdev.conf /usr/share/X11/xorg.conf.d/90-evdev.conf

Se il computer collegato alla LIM è un notebook, nel file
/usr/share/X11/xorg.conf.d/90-evdev.conf commentare la sezione
"touchpad" lasciandola gestire a libinput.

    - riavviare l'X server::

        systemctl restart lightdm

Se necessario, procedere alla calibrazione della lavagna come illustrato
di seguito.

SMART Board 800 (con proiettore Epson EB-570)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Seguire gli stessi passi indicati per la `LIM SMART Board SB680`_ .

Hitachi
-------

`<https://github.com/mmuman/starboard-lsadrv>`_  (Il makefile è da rivedere per i kernel usati da Debian "stretch")

Hitachi Starboard FXT77
^^^^^^^^^^^^^^^^^^^^^^^
In via di elaborazione


Epson
-----

Non richiedono software aggiuntivo per la gestione dell'input. Deve
essere configurato correttamente il proiettore dal suo Menu.
I proiettori interattivi ``Epson EB595 Wi`` ed  ``EB695 Wi`` permettono
l’interattività attraverso:

- apposite penne in dotazione (pen) basate su tecnologia a infrarossi
- tocco con le dita (finger touch) basato su tecnologia laser

.. figure:: /images/lim/fig1.jpg

Per attivare la funzionalità ``penna interattiva`` bisogna selezionare:

.. figure:: /images/lim/fig2.jpg

- Selezionare l'impostazione ``Modo Funzion. Penna`` e premere il tasto
  [Enter].

- Selezionare ``Modalità Ubuntu`` e uscire.

Per attivare la funzionalità ``finger touch`` bisogna selezionare:

.. figure:: /images/lim/fig3.jpg


- Selezionare ``Imp. unità di tocco`` e premere il tasto [Enter].

- Selezionare ``Alimentazione`` e premere il tasto [Enter].

- Selezionare On e uscire.

.. figure:: /images/lim/fig4.jpg


Effettuare una calibrazione se necessario.
Per questa e altre configurazioni che dovessero essere necessarie
consultare il manuale:

  `<https://www.ceconet.ch/data/dokumente/00002981/Italienisch/Support/Anleitung/Instruzioni%20per%20l'uso_EB-695Wi-685Wi-680Wi-675Wi-685W-675W-680-670_IT.pdf>`_


Monitor Samsung
---------------

Samsung **DM65E-BR** non richiede software aggiuntivo per gestire il touchscreen.


Monitor IIYAMA
--------------

I monitor interattivi multimediali (MIM) IIYAMA (per ora i modelli **ProLite
TE8612MIS e TE7512** vengono visti come dispositivi di rete quando connnessi
via USB ad un PC. Questo può essere fonte di problemi per la
connettività dei client che si trovano pertanto ad avere due
dispositivi di rete.

La soluzione a questo problema è quella di mettere in *blacklist* il
driver di rete ``r8152`` che viene caricato dal client FUSS quando
connesso a questo MIM. Nello specifico:

- completare nel file ``/etc/default/grub`` la seguente riga::

    GRUB_CMDLINE_LINUX="modprobe.blacklist=r8152"
  
- aggiornare GRUB::

    update-grub

- riavviare il computer.


QOMO QWB100WS-PS
----------------
Per questo modello si seguano gli stessi passi indicati per la `LIM
SMART Board SB680`_ .
Se necessario, procedere alla calibrazione della lavagna come illustrato
di seguito.

NOTA: NON usare più i vecchi driver che erano scaricabili da:

- `<https://drive.google.com/file/d/0B_NIgwPdq93QRC1BdGt2QTNILUU/view?usp=sharing>`_

oppure da

- `<https://nc.fuss.bz.it/index.php/s/WWYoX5rHMtMLrNK>`_



Calibrazione
------------
Se non presente, installare il pacchetto ``xinput-calibrator``
Lanciare come root l'applicativo::

    xinput_calibrator --list

ed annotarsi l'ID (numerico) del proprio dispositivo di input (lavagna).
Importante individuare correttamente il device poiché potrebbero esserci
altri dispositivi di input (p.es. touchpad del notebook).
Lanciare poi di nuovo il comando xinput_calibrator con i seguenti parametri::

   xinput_calibrator --output-filename /usr/share/X11/xorg.conf.d/99-calibration.conf --device ID-DELLA-LAVAGNA

e procedere alla calibrazione. Al termine della calibrazione verrà
scritto il file sopra indicato il quale conterrà una configurazione
analoga alla seguente::

    Section "InputClass"
        Identifier    "calibration"
        MatchProduct    "EPSON EPSON EPSON 695Wi/695WT"
        Option    "Calibration"    "55 32624 -10 32331"
        Option    "SwapAxes"    "0"
    EndSection

In caso di misclick detection durante la calibrazione, ridurre l'area di
proiezione sulla lavagna e rilanciare il comando di calibrazione.
Riavviare infine l'X server::

    systemctl restart lightdm


Specchiatura ottimale di due device video collegati allo stesso pc
------------------------------------------------------------------

.. note::

  Dalla distribuzione Fuss 10 la configurazione del server grafico Xserver non è più affidata al file xorg.conf che veniva inserito nella cartella /etc/X11/.

L'ultimo ``fuss-client`` installa su tutte le macchine il pacchetto
``autorandr`` ed uno script ``arandr-setup`` che è stato messo in
``/usr/sbin``.

In molti casi autorandr è in grado di individuare automaticamente la
specchiatura ottimale. Nel caso non vi riesca o si voglia ottenere una
coppia di risoluzioni personalizzata si deve procedere nel seguente
modo.


Solo sulle macchine collegate a proiettori/LIM/monitor per le quali si
desidera, oppure è necessaria, una soluzione "personalizzata":

   1) ci si logga come root

   2) si lancia lo script ``arandr-setup``

   3) si sceglie la risoluzione migliore "specchiata" su entrambi gli schermi



Il contenuto dello script ``arandr-setup`` è il seguente::

    #!/bin/bash
    arandr
    autorandr --save $HOSTNAME
    mkdir -p /etc/xdg/autorandr/
    cp -r /root/.config/autorandr/$HOSTNAME /etc/xdg/autorandr/

Lo script permette di creare una configurazione video "personalizzata",
condivisa da tutti gli utenti. Se necessario, può essere successivamente
modificata rilanciando lo stesso script.


Riavviare il PC e verificare che la risoluzione sia quella scelta:
    - sia prima del login
    - sia dopo il login


Per evitare interferenze da parte di eventuali configurazioni fatte via
``xfce4-display-settings`` e salvate in
``~/.config/xfce4/xfconf/xfce-perchannel-xml/displays.xml``, lato server
è stato previsto un cron-job ``/etc/cron.d/home-cleanup`` per la
cancellazione periodica di ``displays.xml`` agli utenti.
