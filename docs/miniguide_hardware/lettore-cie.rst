
Installazione di lettore Carta d'identità elettronica
=====================================================

Premessa
--------

Il lettore NFC USB utilizzato per redigere la presente guida è il
modello ``ACR122U USB NFC`` Reader dell’azienda Advanced Card Systems
Ltd (`<https://www.acs.com.hk/en/products/3/acr122u-usb-nfc-reader/>`_)
ed è compatibile con i principali sistemi operativi (Linux, MacOS,
Windows, Android) con un costo che si aggira attorno ai 40 €.

.. figure:: images/cie/Lettore-CIE.png
   :align: center

Installazione
---------------------

- Usare un pc ``standalone`` con il sistema operativo uguale o superiore a Fuss 10 (buster)
  aggiornato e che esca diretto dal proxy; verificare che siano installati i pacchetti ``pcscd`` e ``pcsc-tools`` .
  Nel caso si operi su un pc joinato alla rete fuss utilizzare un ``utente locale`` (è sempre disponibile l'utente 
  **local-fuss-user** con password **local-fuss-user**)
  
- Disattivare il modulo del kernel ``pn533_usb`` (che a cascata disabilita "nfc" e "pn533") creando il file ``/etc/modprobe.d/cie.conf`` contenente la riga: ::

    blacklist pn533_usb

- Scaricare il middleware del Ministero dell'Interno cliccando sul seguente link:

`<https://github.com/italia/cie-middleware-linux/releases/download/1.4.3.7/CIE-Middleware-1.4.3-7.amd64.deb/>`_

ed installarlo con il comando: ::

    dpkg -i CIE-Middleware-1.4.3-7.amd64.deb
    
oppure installarlo dai repository fuss bullseye e bookworm con: ::

    apt install cie-middleware

- Configurare il browser Firefox, come indicato nel manuale del middleware:

  `<https://github.com/italia/cie-middleware-linux/releases/download/1.4.3.1/CIE.3.0.-.Manuale.d.uso.del.middleware.Linux.pdf/>`_

   ovvero:

1. Avviare Firefox e accedere alla sezione ``Impostazioni`` del browser:

.. figure:: images/cie/Impostazioni_firefox.png
         :align: center
         :width: 150

2. Selezionare la scheda ``Privacy e Sicurezza``

.. figure:: images/cie/PrivacyeSicurezza.png

3. Cliccare su ``Dispositivi di sicurezza``

.. figure:: images/cie/Carica_modulo.png
      :width: 500
      :align: center

4. Cliccare su ``Carica`` e inserire le seguenti informazioni:

   - Nome modulo: **CIE PKI**

   - Nome file modulo: **/usr/local/lib/libcie-pkcs11.so**

.. note::

    Per fare in modo che il lettore venga letto correttamente, è
    necessario che il lettore sia collegato prima dell’avvio di Firefox
    (in caso contrario non viene avviato il servizio **pcscd**).
