.. _miniguida-stampanti:

Gestione e installazione di stampanti e scanner
===============================================

Le stampanti di una rete FUSS vengono gestite tramite OctoNet, come
descritto nella sezione :ref:`stampanti-di-rete` della guida relativa.

Questa sezione descrive le procedure di installazione di alcuni modelli
di stampanti di recente produzione e per le quali la procedura di
installazione prevede passi aggiuntivi.

HP PageWide Pro 452dw
---------------------

Dalla lista delle stampanti HP pubblicata su
`<https://developers.hp.com/hp-linux-imaging-and-printing/supported_devices/index>`_
si vede che questa stampante necessita di una versione del pacchetto
``hplip`` (HP Linux Imaging and Printing) almeno uguale alla ``3.16.3``.

La versione di ``hplip`` presente sul server FUSS (Debian 8 "Jessie") è
la ``3.14.16`` che non prevede questo modello di stampante. Pertanto è
necessario installare ``hplip`` nella versione ``3.16.11`` presente nel
repository ``jessie-backports`` che va prima incluso nel file
``/etc/apt/sources.list``::

    deb http://httpredir.debian.org/debian jessie-backports main contrib

Aggiornare i pacchetti del server ed installare hplip da jessie-backports::

    apt update
    apt -t jessie-backports install hplip

Dopo esserci collegati al server via ssh in modalità X11 Forwarding con
``ssh -X NOMESERVER``), apriamo ``firefox`` connettendoci all'URL
`<http://localhost:631>`_ al quale risponde l'interfaccia web di CUPS.

.. _`HP PageWide Pro 452dw` :

Si scelga *Amministrazione - Aggiungi una stampante*, seguendo i passi
come mostrato nei seguenti screenshot.

Si scelga innanzitutto l'opzione AppSocket/HP JetDirect premendo poi il
bottone *Continua*.

.. figure:: /images/gestione-delle-stampanti/hp-pagewide-pro-452dw-01.png

Indicare nel campo connessione ``socket://IP_DELLA_STAMPANTE:9100`` dopo
aver assegnato alla stampante un IP statico.

.. figure:: /images/gestione-delle-stampanti/hp-pagewide-pro-452dw-02.png

Inserire nella schermata successiva *Nome*, *Descrizione* e *Posizione*
della stampante ed aggiungendo il flag di condivisione.

.. figure:: /images/gestione-delle-stampanti/hp-pagewide-pro-452dw-03.png

Scegliere la marca della stampante (Make)

.. figure:: /images/gestione-delle-stampanti/hp-pagewide-pro-452dw-04.png

ed a seguire il modello. Si scelga *HP Officejet Pro 251dw* che è il
nome del modello compatibile suggerito da HPLIP.

.. figure:: /images/gestione-delle-stampanti/hp-pagewide-pro-452dw-05.png

Verificare che il *Media size* sia correttamente impostato sul formato *A4*

.. figure:: /images/gestione-delle-stampanti/hp-pagewide-pro-452dw-06.png

ed infine, selezionando la voce *Opzioni installate*, indicare che il
*Vassoio 2* è installato e premere il bottone *Imposta le opzioni
predefinite*.

.. figure:: /images/gestione-delle-stampanti/hp-pagewide-pro-452dw-07.png

Ora la stampante è correttamente configurata. Resta l'ultimo passo che è
quello di abilitare i client alla stampa, operazione che va fatta in
OctoNet come descritto nella sezione dedicata.

Brother MFC-L6900DW
---------------------

Scaricare sul server i due pacchetti DEB necessari
(``mfcl6900dwcupswrapper-3.5.1-1.deb`` e ``mfcl6900dwlpr-3.5.1-1.deb``
al seguente link:

    http://fuss.bz.it/utility/printers/Brother_MFC-L6900DW

Installarli con::

    dpkg -i mfcl6900dw*

Dopo esserci collegati al server via ssh in modalità X11 Forwarding con
``ssh -X NOMESERVER``), apriamo ``firefox`` connettendoci all'URL
`<http://localhost:631>`_ al quale risponde l'interfaccia web di CUPS.
In alternativa ci colleghiamo via ssh con il server aprendo un tunnel
con ``ssh root@NOMESERVER -L 13631:localhost:631`` e apriamo ``firefox``
in sessione utente connettendoci all'URL `<http://localhost:13631>`_ .

Si segue poi una procedura di installazione simile a quella della
stampante `HP PageWide Pro 452dw`_

Si scelga ``Amministrazione - Aggiungi una stampante``.

Si scelga innanzitutto l'opzione ``AppSocket/HP JetDirect`` premendo poi
il bottone *Continua*.

Indicare nel campo connessione ``socket://IP_DELLA_STAMPANTE:9100`` dopo
aver assegnato alla stampante un IP statico.

Inserire nella schermata successiva *Nome*, *Descrizione* e *Posizione*
della stampante ed aggiungendo il flag di condivisione.

Scegliere la marca della stampante (``Brother``).

ed a seguire il modello: ``Brother MFCL6900DW for CUPS`` .

Quindi selezionare la voce ``Opzioni installate`` e confermare cliccando
sul bottone ``Imposta le opzioni predefinite``.

Infine abilitare i client alla stampa, operazione che va fatta in
``OctoNet`` come descritto nella sezione dedicata.



Scanner Epson Perfection 3490
-----------------------------

Il seguente tutorial è stato ricavato dal link
`<http://www.autodidacts.io/how-to-get-epson-perfection-3490-flatbed-scanner-working-on-ubuntu-linux>`_
segnalato e testato dal tecnico Paolo Baratta.

Innanzitutto, è necessario installare SANE (che sta per Scanner Access
Now Easy,  un popolare back-end per scanner Linux che consente di
utilizzare tutti i tipi di scanner diversi con tutti i tipi di app di
scansione. Puoi installarlo con il seguente comando::

   sudo apt-get install sane

Crea una directory chiamata snapscan in /usr/share/sane ::

   sudo mkdir  /usr/share/sane/snapscan/

.. _`Esfw52.bin` :

Puoi scaricare il firmware  ``Esfw52.bin`` facendo clic su `<http://cdn.autodidacts.io/misc/Esfw52.bin>`_

oppure trovarlo altrove su Internet cercando il nome del file.


Sposta il file in /usr/share/sane/snapscan/Esfw52.bin ::

   sudo mv ~/Scaricati/Esfw52.bin    /usr/share/sane/snapscan/


Cambia le autorizzazioni del file del firmware::

   sudo chmod 777 /usr/share/sane/snapscan/Esfw52.bin


Apri il  file di configurazione ::

   sudo gedit /etc/sane.d/snapscan.conf

Cambia la riga in alto che dice ::

   "firmware /usr/share/sane/snapscan/your-firmwarefile.bin"

in ::

   "firmware /usr/share/sane/snapscan/Esfw52.bin"

Salva ed esci.

Apri /etc/default/saned (che contiene i valori predefiniti per lo script
di inizializzazione saned) in un editor di testo::

   sudo gedit  /etc/default/saned

e cambia la linea che dice  ``RUN = no``   in   ``RUN = yes``.

Salva ed esci.


Scanner Epson Perfection V370 Photo
-----------------------------------

Installare i driver iscan-perfection-v370-bundle-2.30.4.x64 che si
trovano al seguente link:
`<https://download2.ebz.epson.net/iscan/plugin/perfection-v370/deb/x64/iscan-perfection-v370-bundle-2.30.4.x64.deb.tar.gz>`_
.

Nella directory scompattata si trova uno script ``install.sh`` che
esegue l'installazione dei driver.

Seguire poi la procedura indicata per lo scanner Epson Perfection 3490
sostituendo al firmware   `Esfw52.bin`_   il firmware PS1208MFG_FW_ver
1_29.bin scaricabile all'indirizzo:
`<https://kb.epson.eu/pf/12/webfiles/Article%20ZIP%20files/PS1208MFG_FW_ver%201_29.zip>`_
.


Configurazione job accounting (contabilità processi)
----------------------------------------------------

Testato con Olivetti dCOPIA 5000MF E Kyocera TASKALFA 4053ci

Il seguente tutorial permette di configurare i PC affinché si possa
stampare verso le stampanti in cui è stato configurato il Job accounting
(contabilità processi).

Cosa si deve fare sulla stampante
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il Job Accounting è una funzione offerta dai due modelli di cui sopra e
si attiva manualmente, tramite la tastiera della stampante oppure dalla
LAN, oppure tramite l’interfaccia Web della stampante, dopo averla
configurata con indirizzo IP.

Una volta attivata la funzione, vanno inseriti gli account (uno per
ciascun utente/reparto), costituiti ciascuno da un nome utente e da un
ID (numerico).

Questa operazione può eventualmente essere eseguita con un import di
massa, tramite file `csv`_ (link al file contenente le istruzioni
specifiche).

Cosa si deve fare sui pc
^^^^^^^^^^^^^^^^^^^^^^^^

Installate il pacchetto ``kyodialog_4.0-1_amd64.deb`` contenuto nel file
compresso (versione testata): ``Linux_8.7114_TA...xx53ci_x003``
reperibile al seguente link:
`<https://www.kyoceradocumentsolutions.de/index/serviceworld/downloadcenter.html?initial=false&search=any&searchTerm=Linux&category=driver&language=EN#>`_

Installato il pacchetto (deb), tra le applicazioni troverete “Kyocera
Print Panel”

.. figure:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/1-logo.png
        :align: center

Installate la stampante tramite il tool “Impostazioni di stampa”
(system-config-printer), assicurandovi di selezionare il protocollo IPP

.. figure:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/2-stampante.png

- Selezionate la modalità di installazione che prevede di scegliere il
  PPD e impostate quello fornito al seguente link:
  `<http://fuss.bz.it/utility/ppd/OLIVETTI-KYOCERA-(KPDL).ppd>`_

- Aprite CUPS (http://localhost:631), e nella sezione “Gestione
  stampanti”, sotto “Amministrazione”, cliccate sulla coda di stampa
  appena creata.

- Aprite il menu a tendina “Amministrazione”, selezionate “Imposta
  opzioni di default” e cliccate su “Job Accounting”.

- Aprite il menu a tendina “Contabilità processi” (Job Accounting) e
  selezionate 00000000

- Cliccate su “Imposta opzioni di default” e chiudete CUPS

- Riavviate CUPS #service cups restart

Impostazione dell'id personale
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le operazioni seguenti devono essere eseguite da ciascun utente per
poter impostare il proprio id personale

- Lanciate il tool “Kyocera Print Panel e selezionate la coda di stampa
  che si vuole utilizzare.

- Cliccate su “Lavoro” (Job) e successivamente spuntate la casella
  corrispondente a “Contabilità processi” (Job Accounting).

- Nel menu a tendina selezionate “Usa ID account specifico” ed inserite
  il vostro ID.

- Cliccate su “OK” e chiudete il tool “Kyocera Print Panel”.

Esempio di stampa utilizzando libreoffice writer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- Una volta prodotto/aperto il file, cliccate sull’icona stampante,

.. figure:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/3-icona-stampante.png
        :align: center

- selezionate la coda di stampa e cliccare su “Proprietà”.

.. figure:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/4-stampante.png

- Aperta la scheda “Proprietà”, cliccare su “Dispositivo” e nel menu
  “Opzione:” selezionate “Contabilità processi” (Job Accounting)
- Nel menu “Valore attuale:” selezionare “Personalizzato” (Custom).
- In alto, sempre sotto il menu “Valore attuale:”, si aprirà un campo
  vuoto nel quale andrà inserito il proprio ID.
- Cliccare due volte su “OK” e la stampa verrà inviata con il codice ID.


.. _`csv` :

Import di massa account per stampanti Kyocera e Olivetti
-------------------------------------------------------------

- Installare in Windows il programma Kyocera Net Viewer (pacchetto KNV
  5.12.1029)

  LINK: https://cl.kyoceradocumentsolutions.com/content/dam/kdc/kdag/downloads/technical/executables/utilities/kyoceradocumentsolutions/cl/en/KNV%20v5.12.1029.zip

  ATTENZIONE: durante l’installazione  installate tutti i pacchetti,
  soprattutto la contabilità.

- Nel menù principale troverete tutte le stampanti presenti in rete,
  anche di marche differenti.

.. figure:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/01-import.jpg


- Come mostrato qui sotto, cliccate col tasto destro del mouse sulla
  stampante desiderata, poi su  “Impostazioni di comunicazione” e
  inserite username: ``Admin`` ; password: ``Admin``

.. figure:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/02-import.png

- Inserimanto account (ID, cognome e quota di stampa) tramite fie csv
  (vedi in fondo al presente tutorial come si crea il file csv)

- Posizionate il mouse nella sezione contabilità e attivate la stampante
  desiderata con tasto destro, verificando che il bollino
  corrispondente, da rosso, diventi verde.

.. figure:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/03-import.png

- Cliccate col tasto destro del mouse sulla stampante desiderata e
  selezionate “Imposta più dispositivi account”.

.. figure:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/04-import.png

- Dopo aver scelto il modello di stampante, selezionate ``Impostazioni
  account``, ``Crea da file`` e infine cliccate su ``Sfoglia``  per
  indirizzare il programma sul file ``*.csv`` .


.. figure:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/05-import.png
.. figure:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/06-import.png
.. figure:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/07-import.png

- Selezione file ``*.csv``

- Nella fotocopiatrice verranno creati gli account con le restrizioni scelte

.. figure:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/08-import.png


COME SI CREA IL FILE CSV ::

    | AccountID | AccountName | AccountSubName | PrintTotalR | PrintFullColorR | PrintSingleColorR | CopyTotalR | CopyFullColorR | CopySinglecolorR | ScanTotalR | ScanOtherR | FAXTransmissionR | FAXTransmissionPortR |

(Modello tabella LibreOffice Calc)


- Bisogna partire da una tabella di Libreoffice Calc, corrispondente al
  modello che vedete sopra. La tabella dovrà contenere gli stessi 13
  campi, con i rispettivi nomi, inseriti nella stessa posizione. Il file
  ``*.csv`` deve essere prodotto scegliendo il formato DOS, con i
  separatori , .  (punto e virgola).
  
Configurazione per poter stampare dai client con job accounting locale impostato
---------------------------------------------------------------------------------------------------------------------------

Per permettere di stampare da PC con il ``Job Accounting locale impostato su On`` è necessario "rimappare" l’**utente sconosciuto** su un **utente locale**.
La procedura è la seguente (per confermare le modifiche cliccare su ``Invia``):

1) Si accede, anche da remoto, con l'utente amministratore, in genere ``Admin``

2) Nella finestra di scorrimento a sinistra si selezione ``Impostazioni di gestione``

3) Innanzitutto si crea l’utente locale da associare a quello di rete sconosciuto,ad esempio con ``ID 9999``. Per semplicità gli possiamo attribuire lo stesso Nome utente: 9999

.. image:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/09-Creazione_utente_sconosciuto.png
   :width: 600px
   :align: center
    
|

4) Si clicca su ``Impostazioni``

5) Si clicca su ``Impostazioni autenticazione``

.. image:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/10-Impostazioni_autenticazione.png
   :width: 400px
   :align: center

|
   

6) Si spunta la voce ``Autorizza`` e poi si clicca su ``Impostazioni utente sconosciuto``

.. image:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/11-Autorizza.png
   :width: 400px
   :align: center
    
|

7) Si clicca su ``Elenco account`` , si scorre fino a trovare l’utente 9999 e lo si spunta

.. image:: images/JOB_ACCOUNTING_OLIVETTI_KYOCERA/12-Proprieta_utente_sconosciuto.png
   :width: 400px
   :align: center
    
|

A questo punto sarà possibile agli utenti stampare dai client, ma le stampe non verranno (per il momento) conteggiate, mentre verranno conteggiate le copie fatte direttamente sulla fotocopiatrice.

