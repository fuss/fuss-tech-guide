Lettore per la tessera sanitaria o CNS/CPS
==========================================

L'attuale distribuzione FUSS incorpora già i pacchetti necessari per il
funzionamento del lettore della tessera sanitaria o CPS/CNS (Carta
Nazionale/Provinciale dei Servizi) fornito dalla Provincia Autonoma di
Bolzano. Tali pacchetti, a titolo informativo, sono::

   pcscd libccid opensc pcsc-tools

Per autenticarsi nei siti Web per mezzo della tessera sanitaria bisogna
impostare (una sola volta) Firefox per l'utilizzo della libreria OpenSC
secondo la seguente procedura.

1. Aprire Firefox e andare in `Menu -> Preferenze -> Privacy e
   Sicurezza`; in fondo alla pagina, sotto la voce `Certificati`
   cliccare il bottone `Dispositivi di sicurezza`;
2. Cliccare il pulsante `Carica`;
3. Fornire nel campo `Nome Modulo` una descrizione, ad esempio "Carta
   Nazionale dei Servizi";
4. Per finire impostare il `Nome file modulo` cliccando su `Sfoglia`,
   cercando e selezionando la libreria ``opensc-pkcs11.so`` nei seguenti
   percorsi:

   * per le distribuzioni a 64 bit:
     ``/usr/lib/x86_64-linux-gnu/opensc-pkcs11.so``;
   * per le distribuzioni a 32 bit:
     ``/usr/lib/i386-linux-gnu/opensc-pkcs11.so``.
