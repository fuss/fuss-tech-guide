PC con requisiti particolari
============================

.. _`audio` :

PC HP ProOne 440 G6 24 All-in-One
------------------------------------------

Per il funzionamento dell’ ``audio`` delle macchine HP ProOne 440 G6 24
All-in-One è necessario installare il pacchetto ``firmware-sof-signed``::

    apt update
    apt install firmware-sof-signed


A questo punto l’audio della webcam incorporata ancora non funziona e si
deve seguire la seguente procedura.

1) Il comando ``arecord -l`` consente di individuare ``card`` e
   ``dispositivo`` del microfono (nell` esempio qui sotto ``0,7``)::

    root@aulamuse:~# arecord -l
    **** List of CAPTURE Hardware Devices ****
    card 0: sofhdadsp [sof-hda-dsp], device 0: HDA Analog (*) []
      Subdevices: 1/1
      Subdevice #0: subdevice #0
    card 0: sofhdadsp [sof-hda-dsp], device 1: HDA Digital (*) []
      Subdevices: 1/1
      Subdevice #0: subdevice #0
    card 0: sofhdadsp [sof-hda-dsp], device 6: DMIC (*) []
      Subdevices: 1/1
      Subdevice #0: subdevice #0
    card 0: sofhdadsp [sof-hda-dsp], device 7: DMIC16kHz (*) []
      Subdevices: 1/1
      Subdevice #0: subdevice #0

2) In ``/etc/xdg/autostart`` si crei un file ``alsamodule.desktop`` che
   contenga::

    [Desktop Entry]
    Version=1.0
    Type=Application
    Name=alsamodule
    Comment=Carica modulo per usare audio della webcam incorporata; il device potrebbe variare e si desume dal comando  “arecord -l”
    Exec=/bin/sh -c 'sleep 1;/usr/bin/pacmd "load-module module-alsa-source device=hw:0,7"'
    Terminal=false
    StartupNotify=true

3) Dopo aver riavviato, cliccando sull’icona di Pulseaudio si troverà
   una voce in più, ``Entrata``,  che consente di selezionare il
   dispositivo incorporato.
