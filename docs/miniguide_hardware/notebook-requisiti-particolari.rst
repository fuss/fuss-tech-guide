Notebook con requisiti particolari
==================================

Notebook Acer Extensa 215-21
----------------------------

Per questo modello di notebook si rende necessario impostare il
parametro del kernel ``nvme_core.default_ps_max_latency_us=0``
altrimenti risulta impossibile accedere all'unità di archiviazione. Il
parametro ``nvme_core.default_ps_max_latency_us=0`` è legato ai
dispositivi di archiviazione **NVMe** (Non-Volatile Memory Express),
in particolare alle unità a stato solido (SSD).

``nvme_core`` è un modulo del kernel di Linux che fornisce il supporto per
i dispositivi NVMe. Il parametro ``default_ps_max_latency_us`` è
un'opzione del modulo che controlla la latenza massima, in
microsecondi, per lo stato di alimentazione (PS) di un dispositivo
NVMe.

Quando un dispositivo NVMe è inattivo, può passare a uno stato di
minore potenza per risparmiare energia. Tuttavia, questo può
introdurre una latenza quando il dispositivo deve risvegliarsi e
rispondere alle richieste in arrivo. Il parametro
``default_ps_max_latency_us`` imposta la latenza massima consentita per
questa transizione di stato energetico.

Impostando ``nvme_core.default_ps_max_latency_us=0``, si disabilita di
fatto il limite di latenza della transizione di stato di
alimentazione. Ciò significa che il dispositivo NVMe non entrerà in
uno stato di minore potenza, anche quando è inattivo, per minimizzare
la latenza al costo di un maggiore consumo energetico. Sarà però
sempre accessibile.

È stata predisposta un'immagine clonezilla di FUSS 12 specifica,
scaricabile al link
https://iso.fuss.bz.it/fuss12/client/fuss-12-amd64-client-gpt-nvme-bugfix-20240705-img.tar

È necessario aggiungere questo parametro anche a Clonezilla sul server
FUSS altrimenti Clonezilla stesso non riesce ad individuare l'unità
NVMe. È sufficiente farlo solo nel file
``/srv/tftp/efi/pxelinux.cfg/default`` in quanto si tratta di una
macchina con solo boot UEFI e non BIOS/Legacy, aggiungendo
``nvme_core.default_ps_max_latency_us=0`` al termine delle righe che
iniziano con ``APPEND`` per le modalità `Automatica` e `Manuale` ::

  label Clonezilla-unattended
  MENU LABEL Clonezilla Automatico (Ramdisk)
  # Comment the following line to enable direct booting of clonezilla with no
  # intervention on the machines.
  MENU PASSWD fuss
  KERNEL ../live/vmlinuz
  APPEND initrd=../live/initrd.img boot=live username=clonezilla union=overlay config components quiet noswap edd=on nomodeset nodmraid locales= keyboard-layouts=NONE ocs_live_batch=no net.ifnames=0 nosplash noprompt keyboard-layouts=it locales=it_IT.UTF-8 ocs_prerun1="sshfs clonezilla@proxy:/srv/clonezilla/ /home/partimag -o IdentityFile=/home/clonezilla/.ssh/id_rsa -o StrictHostKeyChecking=no" ocs_prerun2="screen -S XY '/home/partimag/script' " fetch=tftp://192.168.0.1/live/filesystem.squashfs nvme_core.default_ps_max_latency_us=0

  label Clonezilla-Manuale
  MENU LABEL Clonezilla Manuale (Ramdisk)
  # Comment the following line to enable direct booting of clonezilla with no
  # intervention on the machines.
  MENU PASSWD fuss
  KERNEL ../live/vmlinuz
  APPEND initrd=../live/initrd.img boot=live username=clonezilla union=overlay config components quiet noswap edd=on nomodeset nodmraid locales= keyboard-layouts=NONE ocs_live_run="ocs-live-general" ocs_live_extra_param="" ocs_live_batch=no net.ifnames=0 nosplash noprompt keyboard-layouts=it locales=it_IT.UTF-8 ocs_repository="ssh://clonezilla@proxy/srv/clonezilla/" fetch=tftp://192.168.0.1/live/filesystem.squashfs nvme_core.default_ps_max_latency_us=0


Notebook Acer TravelMate TMP-214
--------------------------------

Installazione immagine mediante Clonezilla
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Prima di installare un'immagine di FUSS mediante Clonezilla (via PXE
boot o con chiavetta USB) è necessario accedere al menu di setup del
notebook con `F2`. Nella scheda `Security` impostare come prima cosa la
password di Supervisor premendo `Enter` su `Set Supervisor Password`.
Disabilitare poi la voce `Secure Boot` nella scheda `Boot` come mostrato
nella seguente immagine:

.. figure:: images/acer-tmp214-01.jpg

Dopo aver clonato l'immagine di FUSS e riavviato il notebook, si entri
nuovamente in setup con `F2` riabilitando il `Secure Boot`.

.. figure:: images/acer-tmp214-02.jpg

Selezionare la voce `Select an UEFI file as trusted for executing`
percorrendo le cartelle del disco HDD1: ``<EFI> -> <debian>``.

.. figure:: images/acer-tmp214-03.jpg
.. figure:: images/acer-tmp214-04.jpg
.. figure:: images/acer-tmp214-05.jpg
.. figure:: images/acer-tmp214-06.jpg

Si scelga il file ``shimx64.efi`` premendo `Enter` ed inserendo nel
pop-up la descrizione di questa opzione di boot, p.es. ``fuss10``
confermando con `Yes`.

.. figure:: images/acer-tmp214-07.jpg
.. figure:: images/acer-tmp214-08.jpg

Al termine salvare le modifiche premendo `Exit Saving Changes` nella
scheda `Exit` o semplicemente premendo `F10` confermando al termine.

.. figure:: images/acer-tmp214-09.jpg


Notebook HP 250 G8
------------------

Per cause ancora sconosciute, sporadicamente questo modello di
notebook all'accensione non fa il boot e lo schermo rimane
semplicemente retroilluminato senza visualizzare nulla. Un semplice
riavvio non sortisce alcun effetto. In questi casi HP suggerisce di
seguire la procedura di **Reset del CMOS**
(v. https://support.hp.com/us-en/document/ish_3932413-2337994-16) che
riassumiamo nei seguenti passi:

* accendere il notebook;
* premere il tasto di accensione per **25 secondi**;
* premere il tasto `WINDOWS` + `v` e poi il tasto di accensione per
  **MAX 2 secondi** rilasciando poi il tasto di accensione ma tenendo
  premuti `WINDOWS` + `v`;
* pazientare per un minuto circa; il led sul lato sinistro del
  notebook si accende e spegne alcune volte; appare poi la schermata
  di avvenuto reset del CMOS;
* premere infine `ENTER` per riavviare.

	    
Notebook HP ProBook 430 G5
--------------------------

Wifi
^^^^
Per il funzionamento del wifi si veda:

`wireless-HP ProBook`_



Notebook HP ProBook 450 G5
--------------------------

.. _`wireless-HP ProBook` :

Wireless
^^^^^^^^

Per il funzionamento del wifi è necessario installare il pacchetto
``firmware-realtek`` se non già presente.

Eseguire::

   apt update
   apt install -t stretch-backports linux-image-amd64 firmware-realtek


Video
^^^^^

I portatili (HP ProBook 450 G5) presentano come unica risoluzione il
valore ``1920x1080``.

Il seguente codice aggiunge le risoluzioni del proiettore (o di un altro
dispositivo di output video) al portatile (interfaccia ``eDP-1`` ) e
rende quindi possibile la **specchiatura degli schermi** .  ::

    for i in $(xrandr | awk '{print $1}' | grep "x")
    do
    if [ "$i" != "1920x1080" ]; then
    xrandr --addmode eDP-1 $i
    fi
    done

Il codice va aggiunto allo script
``/etc/fuss-client/display-setup-script/setDisplayResolution`` che
diventerà pertanto::

    #!/bin/sh

    for i in $(xrandr | awk '{print $1}' | grep "x")
    do
    if [ "$i" != "1920x1080" ]; then
    xrandr --addmode eDP-1 $i
    fi
    done

    HOSTNAME=$(/usr/bin/hostname)
    autorandr --load $HOSTNAME || autorandr common || true

Una volta aggiunte le risoluzioni del proiettore, la seconda parte dello
script consente la specchiatura automatica dei due dispositivi già nella
finestra di login.
Gli scripts inseriti in ``/etc/fuss-client/display-setup-script``
vengono infatti lanciati all'avvio di lightdm.

.. _`hp-uefi-boot`:

HP UEFI boot - no bootable device
---------------------------------

Introduzione
^^^^^^^^^^^^

In alcuni modelli HP recenti, bootabili solo con UEFI, dopo
l’installazione dell’immagine Fuss10 il boot non va a buon fine ed
all’avvio appare un messaggio del tipo::

    NO BOOTABLE DEVICE

Il problema è dovuto al fatto che UEFI non riesce a trovare il file EFI
automaticamente. Una soluzione può essere la seguente:

1) Riavviare la macchina e premere il tasto per il boot manuale (in genere F9)


2) Scegliere ``Boot from file`` e poi:

        * > PciRoot … > EFI > debian > grubx64.efi  	# se il Secure
          Boot è disabilitato
        * > PciRoot … > EFI > debian > shimx64.efi	# se il Secure
          Boot è abilitato


3) Dopo l’avvio del sistema aprire una sessione terminale e lanciare::

      efibootmgr

   Il comando precedente elenca le opzioni di avvio disponibili e
   l’ordine di avvio, come nell'esempio seguente ::

      BootCurrent: 0001
      Timeout: 0 seconds
      BootOrder:
      Boot0000* Fuss10    HD(2,c8800,82000,a0d91f49-899b-46ac-8863-35f2d16774c4)File(\EFI\debian\grubx64.efi)
      Boot0001* Fuss10    HD(2,c8800,82000,a0d91f49-899b-46ac-8863-35f2d16774c4)File(\EFI\debian\shimx64.efi)
      Boot2001* USB Drive (UEFI)  RC
      Boot2002* Internal CD/DVD ROM Drive (UEFI)  RC


4) A questo punto si deve individuare la voce del menu con la quale
siete entrati, ad es. ``Boot0001``


5) Settare o cambiare il boot order col comando ::

      efibootmgr -o 0001,0000

   dove ``0001`` sono le 4 cifre che seguono “Boot”; non è necessario
   inserire tutte le voci, è sufficiente la prima.

6) Nel caso si voglia visualizzare il menu all’avvio, è sufficiente
   allungare il ``Timeout`` a 5 secondi o più con::

      efibootmgr -t 5

7) Riavviare la macchina e verificare che il sistema si avvii

Notebook Lenovo IdeaPad 120s
------------------------------------------

Introduzione
^^^^^^^^^^^^^

Tale notebook non è dotato di presa RJ45, pertanto per la connessione
alla rete è necessario dotarsi di un adattatore USB-RJ45. Tale
adattatore non viene riconosciuto dal BIOS per permettere un *network
boot* e consentire l'avvio di un'immagine di Clonezilla da un server
FUSS. E' necessario dotarsi di una chiavetta USB sulla quale va copiata
preventivamente un'immagine di Clonezilla.

Modifiche al BIOS
^^^^^^^^^^^^^^^^^

Innanzitutto si rende necessario modificare le impostazioni del BIOS.
Dopo aver acceso il notebook, premere il tasto `F2` per accedere al BIOS
(Phoenix SecureCore Technology Setup). Sotto la voce `Security`
accertarsi che `Secure Boot` sia `Disabled`. Sotto la voce `Boot`
cambiare `Boot Mode` in `Legacy Support`. Uscire salvando le modifiche
con `F10`. Il notebook verrà riavviato.

Avvio di Clonezilla
^^^^^^^^^^^^^^^^^^^

Dopo aver inserito una chiavetta USB avviabile con un'immagine di
`Clonezilla <https://clonezilla.org/>`_ (la versione usata per questa
guida è la 2.5.6-22), per la scelta del dispositivo di boot, premere
`F12` e selezionare la voce `USB HDD`.

Dopo l'avvio, Clonezilla vi chiederà di selezionare la lingua
dell'interfaccia ed eventualmente di cambiare il layout della tastiera
da quello di default. Dopo questa prima impostazione verrà mostrata la
finestra `Start Clonezilla` nella quale va selezionata la voce omonima.

A seguire l'opzione `device-image`, `ssh_server` e `dhcp`. Il notebook
riceverà un indirizzo IP dal server FUSS e subito dopo si dovrà inserire
l'indirizzo IP del server (p.es. ``192.168.0.1``), la porta (tipicamente
``22``), l'account dell'utente (``clonezilla``), la cartella sul server
ove Clonezilla dovrà leggere l'immagine (tipicamente
``/srv/clonezilla``) e la password dell'utente ``clonezilla``.

Dopo aver inserito la password, Clonezilla chiederà la modalità di
esecuzione: scegliere `Beginner` e poi `restoredisk`; verranno mostrate
le immagini disponibili sul server. Scegliere un'immagine di FUSS 9 a
64bit (di dimensione inferiore a 64 GB) che avrete preventivamente
caricato sul server FUSS nella cartella ``/srv/clonezilla``. Confermare
il disco di destinazione sul notebook (``mmcblk1``) premendo `Invio`.
Scegliere se fare o meno un check dell'immagine prima del restore ed
infine scegliere l'azione da eseguire al termine (`choose`, `reboot` o
`poweroff`). Premere `Invio` e confermare con `y` due volte. Clonezilla
provvederà a copiare l'immagine indicando il tempo necessario per
concludere l'operazione.



`wireless-HP ProBook`_


Lenovo ThinkPad P15 G2
----------------------

Per la configurazione audio (output e input) si veda: :ref:`audio`


Microtech e-tab Pro (2019)
--------------------------

Questo dispositivo è un tablet 10.1" con tastiera. È dotato di
processore Intel Quad Core N4000 e 4GB RAM. Ha due porte USB, pertanto
è possibile fare il boot via USB. La iso di FUSS 12 non presenta alcun
problema di installazione. Ciononostante è necessario fare alcuni
aggiustamenti all'orientamento dello schermo ed alle impostazioni del
touchscreen.

La console necessita di una rotazione di 90° in senso orario. Si deve
modificare il file ``/etc/default/grub`` aggiungendo il parametro
``fbcon=rotate:1`` alla linea ``GRUB_CMDLINE_LINUX_DEFAULT``.
Eseguire poi il comando ``update-grub``.

Riavviare la macchina al fine di poter lavorare più agevolmente da
console per eseguire le successive configurazioni.

Anche in X11 è necessario ruotare di 90° lo schermo ed abilitare il
cursore software. Si crei a tale scopo il file
``/usr/share/X11/xorg.conf.d/10-monitor.conf`` con il seguente
contenuto::

  Section "Monitor"
        Identifier "DSI-1"
        Option "Rotate" "right"
  EndSection

  Section "Device"
        Identifier "Intel GeminiLake"
        Option "SWCursor" "true"
  EndSection

Restano da configurare il touchscreen e la penna attiva "Goodix" che
possono essere elencati assieme agli altri dispositivi di input con il
comando ``xinput``. Sono, rispettivamente, "Goodix Capacitive
TouchScreen" e "Goodix Active Pen". Per il corretto riconoscimento è
necessario installare il pacchetto Debian ``xserver-xorg-input-evdev``
promuovendo poi la posizione del file di configurazione all'interno
della cartella ``/usr/share/X11/xorg.conf.d/``::

  apt update
  apt install xserver-xorg-input-evdev
  
  cd /usr/share/X11/xorg.conf.d/
  mv 10-evdev.conf 90-evdev.conf

Si crei il file di configurazione
``/usr/share/X11/xorg.conf.d/91-touch.conf`` con il seguente
contenuto::

  Section "InputClass"
        Identifier "Goodix Capacitive TouchScreen"
        Driver "evdev"
        MatchProduct "Goodix Capacitive TouchScreen"
        Option "InvertX" "1"
	Option "InvertY" "1"
	Option "SwapAxes" "1"
  EndSection

  Section "InputClass"
        Identifier "Goodix Active Pen"
        Driver "evdev"
        MatchProduct "Goodix Active Pen"
        Option "InvertX" "1"
        Option "InvertY" "1"
	Option "SwapAxes" "1"
  EndSection

Riavviare infine il display manager per rendere effettive le modifiche
con il comando ``systemctl restart lightdm``.

Per comodità, su https://iso.fuss.bz.it/fuss12/client/ viene messa a
disposizione l'immagine Clonezilla di FUSS 12 per questo dispositivo.
