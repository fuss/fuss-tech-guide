******************
Miniguide hardware
******************

Questa sezione comprende guide su argomenti vari di utilità nelle
scuole relativi ad installazioni per hardware particolari.

.. toctree::
   :maxdepth: 2

   pc-requisiti-particolari
   notebook-requisiti-particolari
   logitech-c920e
   stampanti-scanner
   lettore-tessera-sanitaria
   lettore-cie
   modificare-tipo-disco
   ups
   lim-videoproiettori
