************************
Gestione dei FUSS client
************************

Configurazione di un *FUSS Client*
==================================

Per configurare una macchina come *FUSS Client* è disponibile il comando
``fuss-client``, installato col pacchetto omonimo, che cura tutta la
configurazione della macchina, e l'eventuale collegamento della stessa al
*Fuss Server*, installando il software necessario ed effettuando le relative
configurazioni.

Installazione ordinaria
-----------------------

Il comando principale per la configurazione del client è ``fuss-client -a``
che esegue il collegamento ad un *Fuss Server* rilevato automaticamente nella
rete in cui è stata inserita la macchina.

Se si dispone di una chiavetta con una chiave di autenticazione per il
*Fuss Server* questa deve essere inserita sulla macchina, e la procedura
sarà completamente automatizzata (per i dettagli vedi `Accesso con
chiave al server per fuss-client`_), altrimenti una volta lanciato il
comando dovrà essere immessa per tre volte la password di root del *Fuss
Server* per consentire l'importazione delle credenziali necessarie.

Se non vi sono cluster definiti sul *Fuss Server* **è necessario**
definirne preventivamente uno, se ne è definito soltanto uno non verrà
chiesto nient'altro e l'installazione proseguirà direttamente fino alla
fine, ottenendo sul terminale un risultato del tipo::

  PLAY RECAP ****************************************************************
  localhost                  : ok=129  changed=71   unreachable=0    failed=0

(dove si deve avere ``failed=0``)

Se invece sul *Fuss Server* sono presenti più cluster all'inizio verrà chiesto
di scegliere in quale essere inseriti con una schermata del tipo::

  This server has several workstation groups

  Please choose the one desired for this machine:
  0  -  aula1
  1  -  aula2

  Your choice? (enter the server number)
  0
  ...

Impostazione dell'hostname
--------------------------

Il comando ``fuss-client`` supporta la installazione ed il collegamento
al server di una macchina con la contestuale impostazione del nome della
stessa. Questo risulta utile quando un pc viene installato utilizzando
una immagine creata con clonezilla, che ha ovviamente impostato
l'hostname del client da cui la si è creata.

Inoltre, il comando ``fuss-client`` effettua una normalizzazione dei
nomi delle macchine, infatti in alcuni casi veniva usato come hostname
della macchina l'hostname completo (FQDN) della stessa, cosa che crea
poi problemi nella risoluzione dei nomi ed inseriva gli stessi nel file
``/etc/clusters`` del server. Questo cosa poi aveva portato ad usare
come hostname completo (impostato in ``/etc/hosts``) un nome semplice
senza dominio (cosa che potrebbe causare problemi con eventuali servizi
installati successivamente).

Quando si esegue l'installazione ed il collegamento al server di un client, il
comando che permette l'impostazione contestuale dell'hostname della macchina
usando l'opzione ``-H`` nella forma::

  fuss-server -a -H clientname

dove ``clientname`` deve essere un nome a dominio alfanumerico che **non** deve
contenere nessun "``.``", ed essere indicato solo con lettere minuscole.

In tal caso il client, ottenuto il nome del dominio dal server, effettuerà la
corretta impostazione dei file ``/etc/hostname``, inserendovi semplicemente il
nome indicato con un contenuto come::

  clientname

mentre in ``/etc/hosts`` verrà inserito il corretto valore per la risoluzione
completa con un contenuto come::

  127.0.1.1	clientname.institute.lan clientname

e si otterrà correttamente che::

  root@testclient:~# hostname
  clientname
  root@testclient:~# hostname -f
  clientname.institute.lan

ed in questo modo nel file ``/etc/clusters`` del server verrà usato il nome
della macchina.

.. _Gestione dei client FUSS:

Veyon
-----

A partire da FUSS Client 10.0.40, la configurazione di Veyon viene fatta
in maniera completamente automatica estraendo i dati sui client da
``/etc/clusters`` sul server FUSS.

Potranno utilizzare ``Veyon master`` , da qualsiasi postazione di uno
stesso cluster, solo gli utenti appartenenti al ``gruppo veyon-master``
(che andranno pertanto opportunamente scelti ed associati al gruppo).

Per ragioni di sicurezza sono stati implementati dei controlli che
facciano sì che non sia controllabile da qualcun altro attraverso Veyon
nessun utente membro di almeno uno dei seguenti gruppi  ::

    - docenti
    - insegnanti
    - veyon-master
    - tecnici

Qualora si volesse inibire ulteriori gruppi, sarà necessario creare il
file di testo ``/var/www/html/veyon/excluded_groups`` con,  uno per
riga, i nomi dei gruppi.

Attenzione: questo file permette di fare l'override anche dei gruppi
pre-impostati, che andranno pertanto inseriti nel file
``excluded_groups`` per evitare, ad esempio, il controllo sul gruppo
docenti.

Diverse funzionalità di Veyon tipo accensione e spegnimento dei PC,
condivisione dello schermo del docente o aggiornamento dei PC non sono
al momento fruibili e verranno nascoste dall'interfaccia con un prossimo
aggiornamento.

Aggiornamenti
=============

Aggiornamenti minori di fuss-client
-----------------------------------

Nel caso ci siano aggiornamenti di minor version di fuss-client (10.0.x →
10.0.(x+1)) li si può applicare aggiornando il pacchetto::

   # apt update
   # apt install fuss-client

e quindi lanciando::

   # fuss-client -U

per applicare le nuove configurazioni.

Le informazioni presenti in ``/usr/share/doc/fuss-client/changelog.gz``
(dopo l'installazione del pacchetto) possono essere utili per scoprire
se le modifiche sono utili per installazioni esistenti o se riguardano
solo le nuove installazioni.

Accesso con chiave al server per fuss-client
============================================

Per collegare i client ad un fuss-server è necessario dare accesso ssh
al server a chi effettua il collegamento; è possibile limitare questo
accesso usando una chiave ssh apposita che da accesso come root al
server, limitato però ai soli comandi necessari per ``fuss-client``.

Coi comandi ``fuss-server create`` o ``fuss-server upgrade`` viene
creata una coppia di chiavi in
``/etc/fuss-server/client_keys/client-rsa(.pub)`` e la stessa viene
abilitata per l'accesso al server da parte di ``fuss-client``.

Sui client già configurati con ``fuss-client`` è disponibile uno script,
``/usr/local/sbin/copy_server_key``, che prepara una chiavetta USB
configurata per essere riconosciuta da ``fuss-client``, il quale
provvede a montarla, usarla per l'identificazione e poi smontarla quando
non più necessaria, in modo da poter essere spostata sugli altri client.

In alternativa, è possibile trasferire la chiave privata sui client in
altro modo a piacere ed usare l'opzione ``-k
</path/completo/della/chiave>`` per identificarsi senza password.  In
questo caso è ovviamente cura dell'utente provvedere a mount e umount di
eventuali dispositivi rimovibili.

.. note:: ssh impone che il file della chiave abbia permessi tali da
    impedirne la lettura ad altri utenti; se tali file sono su
    filesystem ext è opportuno assegnare i permessi ``0600`` al file
    della chiave privata.

    Se si usa invece un filesystem FAT, è necessario usare opzioni di
    mount opportune per evitare che i file si presentino con permessi
    ``0755``, come avviene di default.

In entrambi i casi, i file presenti sulla chiavetta vengono usati
durante la fase iniziale di fuss-client; quando non sono più in uso
viene emesso un suono diverso a seconda se la chiavetta sia stata
smontata e la si possa rimuovere tranquillamente o ci siano stati
problemi nello smontaggio e quindi la si può smontare a mano e
rimuovere.

Nel caso in cui non si senta il suono, questo avviene poco prima del
momento in cui iniziano le scritte colorate di ansible.

Gestione delle chiavi
---------------------

Aggiunta di nuove chiavi sul server
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

È possibile abilitare nuove chiavi mettendo la coppia di chiavi
pubbliche e private in ``/etc/fuss-server/client_keys/`` e lanciando
``fuss-server upgrade`` perché vengano abilitate all'accesso.

``/etc/fuss-server/client_keys/client-rsa(.pub)`` deve esistere
(altrimenti ne viene creata una nuova) ed è la chiave di default usata
ad esempio dallo script ``copy_server_key``, altre chiavi vanno gestite
manualmente.

Disabilitazione di chiavi
^^^^^^^^^^^^^^^^^^^^^^^^^

Per disabilitare una chiave dal server è necessario:

* rimuovere la riga corrispondente da ``/root/.ssh/authorized_keys``
* rimuovere le chiavi pubbliche e private da ``/etc/fuss-server/client_keys/``

Creazione di una chiavetta USB con le chiavi ssh
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sui client già configurati, a partire dalla versione 9.0.16 di
fuss-client, è presente uno script, ``/usr/local/sbin/copy_server_key``,
per creare chiavette USB contenenti le chiavi ssh da usare per
l'identificazione.
Lo script non dipende da altre componenti del fuss-client e può essere
copiato su altre macchine; in tal caso per usarlo è necessario aver
installato il pacchetto ``python3-pyudev`` (dipendenza di ``fuss-client``).

Per usarlo, collegare una chiavetta USB vuota al computer e lanciare il
comando::

   # copy_server_key /dev/sdXn

dove ``/dev/sdXn`` è il device corrispondente alla partizione della
chiavetta dove si desiderano salvare le chiavi.

.. warning:: La partizione specificata verrà riformattata, perdendo
   tutti i dati eventualmente presenti sulla chiavetta.

Di default viene copiata la chiave presente sul server raggiungibile
all'indirizzo ``proxy``; in alternativa si può specificare l'indirizzo
del server tramite l'opzione ``-s <indirizzo.del.server>``.

.. warning:: Nella versione di mkfs.ext4 presente in Debian Buster, e
   quindi sui vecchi fuss-client, è presente un bug nella localizzazione
   italiana, `#907034
   <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=907034>`_, che
   stampa le richieste di conferma in inglese, ma si aspetta risposta in
   italiano.

   Se viene stampata una richiesta tipo::

      mke2fs 1.43.4 (31-Jan-2017)
      /dev/sdb1 contiene un file system ext4 con etichetta "portachiavi"
          last mounted on /mnt/portachiavi on Mon Aug 13 09:40:52 2018
      Proceed anyway? (y,N)

   è necessario premere ``s`` per continuare; ``y`` non viene
   riconosciuto e viene trattato come il default, ``n``, che quindi
   causa l'uscita immediata dal programma.

Specifiche
----------

Chiavi USB per la distribuzione delle chiavi ssh
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Per poter lavorare in modo sicuro, ``fuss-client`` senza opzione ``-k``
richiede che le chiavi ssh siano salvate su una chiavetta USB
configurata in modo ben preciso, come generata dallo script
``copy_server_key``.

La chiavetta USB deve contenere una partizione formattata ext con
etichetta ``portachiave``, all'interno della quale è presente una
directory ``server_key`` contenente il file ``client-rsa`` con permessi
rispettivamente ``0700`` e ``0600``, entrambi appartenenti all'utente
``root``.

Risoluzione di problemi
=======================

Diagnostica degli errori di fuss-client
---------------------------------------

Per poter risolvere eventuali problemi di installazione di un client durante
l'esecuzione di ``fuss-client`` è essenziale poter disporre del log completo
delle operazioni effettuate da *ansible* durante l'esecuzione, riportare solo
le righe finali del risultato non è sufficiente.

Per questo nel caso si presentino problemi è opportuno rilanciare il comando
seguendo le indicazioni per registrare la sessione illustrate in
:ref:`Bug-reporting`.

Procedure alternative
=====================

Installazione su Debian 12 (bookworm)
-------------------------------------

Fuss Client può essere installato su una Debian 12 (bookworm) standard; in
tal caso è necessario effettuare alcune configurazioni, sempre lavorando
come root.

* Installare Debian 12 (bookworm) recuperando una delle seguenti ISO (a
  seconda dell'occorrenza):

  .. list-table::
     :header-rows: 1

     * - Architettura
       - ISO xfce-desktop UFFICIALE
       - ISO xfce-desktop NON UFFICIALE (con firmware proprietari)
     * - amd64
       - `debian-live-<versione>-amd64-xfce.iso
         <https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/>`_
       - `debian-live-<versione>-amd64-xfce+nonfree.iso
         <https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/10.3.0-live+nonfree/amd64/iso-hybrid/>`_
     * - i386
       - `debian-live-<versione>-i386-xfce.iso
         <https://cdimage.debian.org/debian-cd/current-live/i386/iso-hybrid/>`_
       - `debian-live-<versione>-i386-xfce+nonfree.iso
         <https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/10.3.0-live+nonfree/i386/iso-hybrid/>`_

* Abilitare i repository fuss in
  ``/etc/apt/source.list.d/archive_fuss_bz_it.list``::

     deb [signed-by=/usr/share/keyrings/fuss-keyring.gpg] http://archive.fuss.bz.it/ bookworm main

* aggiungere la chiave di firma del repository e aggiornare apt::

     # wget -qO /usr/share/keyrings/fuss-keyring.gpg  https://archive.fuss.bz.it/apt.gpg
     # apt update

* ed infine, lanciare il programma fuss-client::

     # fuss-client -a

Installazione standalone
------------------------

fuss-client supporta anche una modalità di installazione standalone
che non effettua il collegamento ad un fuss-server, ma si limita a
configurare un'installazione di debian con i programmi e le
personalizzazione del sistema fuss.

Per usarla, a partire da un'installazione pulita di Debian, è
sufficiente aggiungere i repository fuss in
``/etc/apt/source.list.d/archive_fuss_bz_it.list``::

     deb [signed-by=/usr/share/keyrings/fuss-keyring.gpg] http://archive.fuss.bz.it/ bookworm main

* aggiungere la chiave di firma del repository e aggiornare apt::

     # wget -qO - https://archive.fuss.bz.it/apt.key | gpg --dearmour > /usr/share/keyrings/fuss-keyring.gpg
     # apt update

* ed infine, lanciare il programma fuss-client::

     # fuss-client --standalone

Configurazione di chroot per la generazione di iso
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Nel caso in cui si stia configurando con ``--standalone`` non una
macchina reale ma una chroot, si può aggiungere l'opzione ``--iso`` per
evitare che siano configurati grub e i locale, per evitare fallimenti.

Installazione leggera
---------------------

L'opzione ``--light`` permette di fare installazioni più leggere
evitando di installare gli inter metapacchetti ``fuss-kids``,
``fuss-children`` e ``fuss-education``. Ovviamente una selezione di
pacchetti educational più limitata può essere installata manualmente in
un secondo tempo.

Supporto per firmware proprietari
---------------------------------

Per il funzionamento di determinato hardware è necessario l'uso di
firmware proprietari, disponibili nella sezione ``non-free-firmware``
dei repository.

L'opzione ``--unofficial`` di ``fuss-client`` aggiunge l'installazione
di tali firmware; per usarla è necessario abilitare le sezioni
``contrib``, ``non-free-firmware`` e ``non-free`` per i repository
debian in ``/etc/apt/sources.list``.

.. warning:: Come dice il nome, questa opzione è da considerarsi non
   ufficiale, ed in particolare l'installazione di software da
   ``non-free`` diverso dai firmware necessari per il supporto hardware
   non è supportata.

Cambiamento dell'hostname
--------------------------------------------

Per cambiare l'hostname di un fuss-client si può lanciare il comando::

   fuss-client -H <nuovo_hostname>

questo aggiorna anche la chiave kerberos con il nuovo hostname.

L'opzione è pensata per essere usata su macchine appena ripristinate da
un'immagine pulita per assegnare un hostname unico alla stessa subito
prima di configurarla con fuss-client.

L'uso di questa opzione su macchine già configurate con fuss-client è
adeguato nel caso in cui si sia fatto un errore nell'assegnazione
dell'hostname, ma è sconsigliato farne uso sistematico, e in particolare
nel caso di cambiamento del server è consigliabile effettuare un restore
da immagine pulita.

Nel caso si usi questa opzione su un client che è già stato collegato ad
un fuss-server è inoltre opportuno usare, sul server, il comando::

   /usr/share/fuss-server/scripts/forget_client <vecchio_hostname>

per rimuovere le tracce della registrazione con l'hostname precedente.

Rimozione completa della configurazione dei client
--------------------------------------------------------------------------------

**Premessa**

Fino a ``fuss 11`` fuss-client permetteva di togliere parte della configurazione di un FUSS Client in modo da poterlo spostare da un server in dismissione ad un nuovo server senza reinstallarlo.
Si otteneva lanciando il comando: ::

  # fuss-client -r -p

quindi spostando il client sulla rete del nuovo server e lanciando fuss-client regolarmente come indicato sopra.

**Configurazione attuale**

A partire da ``fuss 12``, una volta che una macchina è stato configurata non è possibile rimuovere in modo affidabile la sua configurazione; 
per cambiare totalmente caso d'uso di un PC (ad esempio da client a standalone) è necessario ripristinare la sua installazione da un'immagine pulita 
tramite fuss-fucc (o effettuarne una nuova installazione, a seconda di cosa risulti più comodo o veloce).

Mantenendo lo stesso caso d'uso (ridenominazione di un client nella stessa rete o spostamento di un client da una rete ad un'altra), generalmente è sufficiente una riconfigurazione con l'opzione ``-H`` documentata sopra: ::

    fuss-client -H client_name

Per rimuovere il client ``lato server`` è necessario invece lanciare sul server il comando: ::

    /usr/share/fuss-server/scripts/forget_client client_name
    
Nel caso vi sia la necessità di rimuovere un certo numero di client dal server,
è vantaggioso creare ad esempio nella cartella  /root del server una lista di hostname da rimuovere 
chiamandola ``client-forget.csv`` e uno script ``forget-clients`` come il seguente: ::

    #!/bin/sh

    systemctl stop octofussd
    for HOSTNAME in $(cat client-forget.csv)
    do
            echo $HOSTNAME
            DOMAINNAME=$(dnsdomainname)
            FQDN="$HOSTNAME.$DOMAINNAME" 
            sed -r -i "s/ +$HOSTNAME( +|$)/\1/g" /etc/clusters
            kadmin.local ktremove nfs/$FQDN all
            kadmin.local delprinc nfs/$FQDN
    done
    systemctl start octofussd

Lanciando questo script, tutti i client elencati vengono ``rimossi dal join`` e cancellati da ``/etc/clusters``.


..  LocalWords:  FUSS client Fuss fuss root RECAP localhost changed failed
..  LocalWords:  unreachable This has several groups Please choose the one
..  LocalWords:  desired for this machine Your choice number version apt log
..  LocalWords:  update install ansible nslcd stretch list table header rows
..  LocalWords:  xfce amd nonfree iso repository deb main wget key add kids
..  LocalWords:  metapacchetti children education graphics language support
..  LocalWords:  various
