******************************
Guida rapida all'installazione
******************************

Questo capitolo è dedicato a chi vuole vedere velocemente come
installare un server FUSS ed un client.

Topologia di una tipica rete didattica
======================================

Prima di mostrare come installare server e client, vediamo nella
seguente figura la tipica topologia di rete dove FUSS può trovare
applicazione.

.. figure:: images/fuss-network-architecture-cur.png
    :alt: Topologia di una rete FUSS

    Tipica topologia di una rete FUSS

Un server FUSS deve avere almeno 2 interfacce di rete. La prima serve
per la connessione alla WAN (Wide Area Network) mentre la seconda è
collegata alla rete locale (LAN-1) della scuola. La presenza di una
terza porta Ethernet sul server e di una seconda LAN nella scuola alla
quale sono connessi degli access point WiFi sono i presupposti per poter
installare sul server FUSS un captive-portal che offre la possibilità a
dispositivi satellite di accedere ad internet previa autenticazione.


Installazione di FUSS server dal template cloud-init ready
==========================================================

A partire dalla versione 10 è disponibile una modalità di installazione più
veloce del FUSS server partendo da una immagine di macchina virtuale
preinstallata con il supporto per la tecnologia di autoconfigurazione
cloud-init.

Questa modalità può essere utilizzata solo con un sistema di virtualizzazione,
qui verrà illustrato come farlo con la piattaforma di virtualizzazione
Proxmox, adottata dal progetto, che supporta la tecnologia indicata
consentendo di gestire tutte le caratteristiche della macchina virtuale
direttamente dalla sua interfaccia web.

In particolare diventa possibile:

* gestire indirizzi di rete, gateway e DNS dall'interfaccia web
* gestire hostname e dominio dell'interfaccia web
* allargare automaticamente il disco radice una volta ridimensionato
  sull'interfaccia web


Installazione della macchina virtuale
-------------------------------------

Il primo passo è scaricare l'immagine predisposta dal progetto per
inserirla fra i dump disponibili per il ripristino. Questa è disponibile
all'indirizzo
http://iso.fuss.bz.it/cloud-init/vzdump-qemu-fuss-server-12-latest.vma.zst
e la cosa più semplice è scaricarla direttamente sul server Proxmox su
cui sarà utilizzata collegandosi con SSH ed eseguendo::

  # cd /var/lib/vz/dump/
  # wget http://iso.fuss.bz.it/cloud-init/vzdump-qemu-fuss-server-12-latest.vma.zst

.. note:: se si è configurato in Proxmox uno storage diverso da ``local`` per
          le immagini dei dump ci si ponga nella rispettiva directory invece
          che sotto ``/var/lib/vz/dump/``.

.. note:: le immagini più recenti sono compresse con il formato ``zstd`` ed il
          file ha estenzione ``.zst``, alcune delle prime immagini (il cui uso
          è comunque sconsigliato) sono in formato ``lzo`` ed hanno estensione
          ``.lzo``.

Una volta completato il download l'immagine comparirà fra in contenuti
dello storage (dall'interfaccia web) e si potrà iniziare il ripristino
anche direttamente dall'interfaccia selezionandola ed usando il pulsante
"Restore" che creerà una nuova macchina virtuale sul primo VMID libero,
l'interfaccia comunque consente di indicarne uno specifico diverso. Si
abbia cura di indicare, sempre nella finestra di ripristino, l'uso di
``local-lvm`` come storage di ripristino e mettere la spunta sulla
casella "Unique".

Si tenga presente che perché questo funzioni regolarmente occorre avere
configurato Proxmox per l'uso di FUSS, la macchina virtuale infatti assume la
presenza di almeno due diverse interfacce di rete, una sulla rete esterna
(``vmbr0``) ed una sulla rete interna (``vmbr1``); qualora non fosse così si
potranno comunque cambiare le assegnazioni delle interfacce una volta
ripristinata la macchina virtuale.

Alternativamente si può eseguire il ripristino dalla riga di comando (si
assume che si sia configurato lo storage su LVM con il nome utilizzato nella
installazione di default di Proxmox) con::

  qmrestore /var/lib/vz/dump/vzdump-qemu-000-0000_00_00-00_00_00.vma.zst \
     106 -storage local-lvm -unique

dove al posto di ``106`` si può usare un qualunque VMID libero. Lo storage
deve essere local-lvm (come l'installazione diretta) e l'uso di ``-unique``
consente di generare un MAC address della nuova VM che non confligga con altri
eventualmente presenti (è poco probabile, a meno che non si reistallino più
machine dalla stessa immagine di partenza, ma è buona norma farlo).

Una volta ripristinata l'immagine gli si deve associare l'immagine del disco
di configurazione con::

  qm set 106 --ide2 local-lvm:cloudinit

.. note:: se si ottiene un errore del tipo::

             lvcreate 'pve/vm-106-cloudinit' error:   Logical Volume "vm-106-cloudinit" already exists in volume group "pve"

          si rimuova la precedente istanza del volume logico con::

             lvremove pve/vm-106-cloudinit

          e si ripeta il comando.

A questo punto prima di avviare la macchina per la prima volta si potrà
configurare la rete dall'interfaccia web, nella sezione *Cloud-Init*,
impostando: gli IP sulle interfacce di rete, il default gateway per
l'interfaccia esterna, una chiave SSH di accesso a root, il dominio ed il
server DNS. Quest'ultimo deve essere sempre ``127.0.0.1``, ed il nome a
dominio dovrà essere quello che verrà poi utilizzato nella configurazione
finale eseguita dal comando ``fuss-server``.

.. warning:: si deve **sempre** configurare il server DNS come ``127.0.0.1`` e
   il nome a dominio uguale a quello che verrà usato poi con il comando
   ``fuss-server`` altrimenti si rischia che in una successiva esecuzione di
   ``fuss-server``, o nella riesecuzione dell'inizializzazione di cloud-init,
   ci siano interferenze e sovrascritture reciproche

Tutte queste operazioni si possono fare anche a riga di comando; per inserire
i suddetti parametri nelle configurazioni di ``cloud-init`` (si adattino gli
indirizzi di rete alle proprie esigenze) si esegua::

  qm set 106 --ipconfig0 ip=10.0.101.79/24,gw=10.0.101.1
  qm set 106 --ipconfig1 ip=192.168.0.1/24
  qm set 106 --searchdomain fusslab.blz
  qm set 106 --nameserver 127.0.0.1
  qm set 106 --sshkey ~/.ssh/id_rsa.pub

l'ultimo comando, se si ha un elenco di chiavi, può essere sostituito da::

  qm set 106 --sshkey elencochiavissh.txt

dove ``elencochiavissh.txt`` è un file contenente l'elenco di chiavi pubbliche
(una per riga) che verranno abilitate per la macchina in questione; lo si può
generare da un elenco di file di chiavi con qualcosa tipo ``cat *.pub >
elencochiavissh.txt``.

Oltre alla configurazione della rete è opportuno impostare dall'interfaccia
web anche l'hostname della macchina. Questa corrisponde di default al nome
usato da Proxmox per la relativa VM (quello mostrato insieme al VMID
nell'interfaccia). Nell'immagine distribuita si è usato come nome di default
``fuss-server-base-image`` che si potrà modificare nella sezione *Options*
dell'interfaccia web. Anche in questo caso la modifica si può fare a riga di
comando con::

  qm set 106 --name serverhostname

.. warning:: benché sia possibile impostare l'hostname della macchina in un
             secondo tempo all'interno della stessa con ``hostnamectl``, dato
             che la configurazione iniziale della rete viene gestita comunque
             da cloud-init, è opportuno configurare l'hostname direttamente in
             questa sezione e verrà correttamente propagato anche nelle varie
             configurazioni.

.. warning:: si tenga presente che se si cambia uno qualunque di questi
             parametri in un secondo tempo, **tutte** le configurazioni da lui
             gestite verranno rigenerate da cloud-init al successivo
             riavvio. Questo comprende anche le chiavi SSH del server, con la
             conseguenza che le precedenti non saranno più valide; per cui se
             ci si è già collegati alla macchina si otterrà il solito avviso
             che le fingerprint delle chiavi del server non corrispondono, e
             sarà necessario rimuoverle e riaccettarle da capo.

Si dovranno inoltre modificare i parametri hardware della macchina virtuale
(dalla omonima sezione *Hardware* nell'interfaccia web), per aumentare la
memoria ed allargare il disco quanto necessario, ed eventualmente aggiungere
le interfacce di rete mancanti (ad esempio quella per il captive portal). Si
dovrà anche abilitare l'accensione automatica della macchina virtuale
all'avvio, dalla sezione *Options*.  Anche per modificare queste
caratteristiche si può continuare ad operare direttamente a riga di comando,
con qualcosa del tipo::

  qm set 106 --memory 4096
  qm set 106 --onboot 1
  qm resize 106 scsi0 500G

dove si aumenta la RAM assegnata alla macchina virtuale a 4G, si richiede il
lancio automatico al riavvio di Proxmox, e si allarga il disco a 500G. Se però
si decide di dedicare un disco separato per le home quest'ultima operazione
non deve essere eseguita, a meno che i 32G assegnati nell'immagine di default
al disco della radice risultino insufficienti, nel qual caso comunque la si
esegua con una dimensione opportuna.

Le immagini fornite hanno già attivato il discard sul disco di installazione
(quest'ultima opzione ha senso solo se, come nell'esempio, si ha un disco che
è stato estratto da uno storage che supporta il discard, come LVM-thin),
qualora fosse assente o non necessario lo si può attivare/disattivare
rispettivamente con::

  qm set 106 --scsi0 local-lvm:vm-106-disk-0,discard=on
  qm set 106 --scsi0 local-lvm:vm-106-disk-0,discard=off

Si tenga presente che le opzioni indicate verranno applicate al successivo
riavvio, anche per la parte di allargamento del disco, che verrà eseguita
automaticamente da ``cloud-init`` (con l'avvertenza però che questo è
possibile solo grazie allo specifico partizionamento usato dall'immagine
fornita dal progetto).

Alla fine del primo avvio della macchina virtuale vengono mostrate nella
console (accessibile dall'interfaccia web di Proxmox) le fingerprint delle
chiavi generate per il server SSH, che è possibile usare per verificarne la
correttezza alla prima connessione. Le si possono trovare in un secondo tempo
nel file ``/var/log/cloud-init-output.log``.


Configurazioni post-installazione
---------------------------------

Una volta finita l'installazione non è in genere necessario eseguire nessuna
altra configurazione, a meno di non avere necessità di mantere le home su un
disco separato, che è una buona pratica qualora serva mantenere le quote
disco, e che consentirà, in futuro, di eseguire un aggiornamento solo per la
parte di sistema, senza dover reinstallare i file nella home.

Si perde però in questo caso la capacità di avere un ridimensionamento
automatico del disco come avviene per il filesystem di root, in quanto
cloud-init gestisce questa funzionalità solo per quest'ultimo. Essendoci pro e
contro si lascia la valutazione dell'uso delle home separate a chi esegue
l'installazione, tratteremo comunque qui le modalità per configurare le home
su disco separato.

Creazione di un disco aggiuntivo per le home
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Per installare le home su un disco separato si provveda ad aggiungere un disco
di dimensione opportuna alla macchina virtuale dall'interfaccia web (sezione
"Hardware", "Add->Hard Disk"). Si può eseguire la stessa operazione dalla riga
di comando con qualcosa del tipo::

  pvesm alloc local-lvm 106 '' 100G
  qm set 106 --scsi1 local-lvm:vm-106-disk-1,discard=on

(di nuovo l'opzione ``discard=on`` ha senso solo se si usa uno storage come
``local-lvm``).

A questo punto una volta avviato il server disco verrà visto all'avvio come
``/dev/sdb``.  Ci si colleghi come root e si verifichi che il disco sia
effettivamente riconosciuto come ``/dev/sdb``; a questo punto lo si dovrà
partizionare e creare il filesystem per le home, questo si può fare con::

  echo "start=2048, type=83" | sfdisk /dev/sdb
  mkfs.ext4 /dev/sdb1

si recuperi l'UUID del disco e lo aggiunga a ``/etc/fstab``, questo si può
fare con::

  echo -e "# /home was on /dev/sdb1 during installation" >> /etc/fstab
  echo -e "$(blkid -o export /dev/sdb1|grep ^UUID=) /home  ext4  defaults,grpquota,usrquota  0  2" >> /etc/fstab

Si sostituisca ``/dev/sdb1`` con l'opportuno file di dispositivo se se ne è
usato un altro. A parte l'aggiunta di un commento esplicativo il comando
estrae l'UUID del filesystem appena creato e crea una voce corretta per le
home con le opzioni per avere le quote e il giusto numero di sequenza nella
scansione iniziale del filesystem check.

Una volta verificato che nella installazione di cloud-init non ci siano file o
directory sotto ``/home`` (potrebbe restare la home dell'utente ausiliario di
installazione ``debian``, che può essere rimosso con ``userdel -r debian``) si
potrà montare il disco home con ``mount -a``.

Configurazione iniziale per le quote
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sia che si sia usato un disco aggiuntivo per ``/home``, sia che si siano
attivate successivamente le quote sulla radice (aggiungendo
``grpquota,usrquota`` alla sua voce in ``/etc/fstab``) è necessario
inizializzare le quote con::

  quotacheck -a -f
  quotacheck -ag -f

dove l'opzione ``- f`` è necessaria qualora (come avviene se si aggiungono le
opzioni di mount per le quote sulla radice) per forzare la scrittura dei file
delle quote a sistema attivo. Si possono ignorare gli avvertimenti che i dati
potrebbero essere imprecisi, verranno comunque corretti al primo riavvio.

Una volta attive le quote si potranno usare i comandi ``repquota`` e
``repquota -g`` per verificare la effettiva presenza delle quote. Se detti
comandi non sono presenti si possono installare con ``apt install quotatool``
(sono stati comunque inclusi nell'immagine di cloud-init).

Se non si riavvia la macchina dopo aver eseguito i comandi precedenti ed
attivato le quote nelle opzioni di montaggio, o se si aggiungono le opzioni
``grpquota,usrquota`` in un secondo tempo rimontando il filesystem, e si forza
il calcolo delle quote a filesystem montato usando anche l'opzione ``-m`` con
i due comandi precedenti, occorrerà anche attivare le quote esplicitamente
con::

    quotaon -a


.. _installazione-fuss-server:


Installazione di FUSS server tradizionale
=========================================

Per installare FUSS Server su una macchina fisica, è necessario partire
da un'installazione di Debian Bookworm, su cui è basato il FUSS server.
Le immagini si possono ottenere dall'indirizzo

https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/

prendendo il file ``debian-<versione>-amd64-netinst.iso``.

Per le :ref:`installazioni nelle scuole di Bolzano
<installazioni-scuole-bolzano>` quest'immagine andrà :ref:`caricata sul
server proxmox <caricare-iso-su-proxmox>` precedentemente configurato,
proseguendo quindi con la creazione della macchina virtuale fino al boot
della stessa. Ma in questo caso è preferibile utilizzare la procedura
illustrata nel paragrafo precedente.

Per installare invece direttamente sul server è necessaria una
chiavetta USB della capacità minima di 1 GB sulla quale va copiata
l'immagine ISO scaricata. In GNU/Linux si può usare il comando ``dd``.
Dopo aver inserito la chiavetta nel PC ove è disponibile l'immagine,
verificare con il comando ``lsscsi`` quale dispositivo è stato assegnato alla
chiavetta. Nell'esempio usiamo ``/dev/sdX`` dove X può essere una delle
lettere ``a``, ``b``, ``c`` ecc.  Come root, dare il comando::

   # dd if=/PERCORSO_IMMAGINE/fuss-server-8.0-amd64-201708221233.iso \
     of=/dev/sdX bs=4M status=progress

Preparata la chiavetta USB, inserirla nel server e dopo averlo avviato
premurarsi di scegliere come dispositivo di boot la chiavetta stessa.

Installazione di Debian
-----------------------

Configurazioni iniziali
^^^^^^^^^^^^^^^^^^^^^^^

Se si è correttamente configurato l'avvio della macchina virtuale dalla ISO
del Netinstall si otterrà la seguente schermata iniziale:

.. figure:: /installazioni_specializzate/images/netinstall-boot.png

si scelga una installazione testuale, verranno chieste nell'ordine la lingua:

.. figure:: /installazioni_specializzate/images/netinstall-langsel.png

e si scelga l'italiano; la posizione (per il fuso orario):

.. figure:: /installazioni_specializzate/images/netinstall-posizione.png

e si scelga Italia; la tastiera:

.. figure:: /installazioni_specializzate/images/netinstall-keyboard.png

e si scelga quella italiana.

Per la rete si usi come interfaccia per l'installazione quella corrispondente
alla WAN del server (quella che si affaccia su Internet):

.. figure:: /installazioni_specializzate/images/netinstall-ifselect.png

l'installer tenterà la configurazione automatica della rete, che deve essere
interrotta (si prema invio durante l'acquisizione per cancellarla, o si torni
indietro qualora sia avvenuta). In questo modo si potrà selezionare
esplicitamente una configurazione manuale per l'IP "esterno" del fuss-server:

.. figure:: /installazioni_specializzate/images/netinstall-manualnet.png

e si effettuino le impostazioni standard della rete (indirizzo IP, netmask e
default gateway e DNS):

.. figure:: /installazioni_specializzate/images/netinstall-configip.png
.. figure:: /installazioni_specializzate/images/netinstall-configgw.png
.. figure:: /installazioni_specializzate/images/netinstall-configdns.png

Verrà poi chiesto il nome della macchina, si inserisca subito quello
definitivo:

.. figure:: /installazioni_specializzate/images/netinstall-sethostname.png

si prosegua poi impostando il dominio:

.. figure:: /installazioni_specializzate/images/netinstall-setdomain.png

verranno poi chieste la password di root e l'utente iniziale, da impostare a
piacere.

Si dovrà poi effettuare il partizionamento dei dischi per l'installazione del
sistema.

La scelta più sicura, per evitare problemi di riempimento della radice,
è usare filesystem separati per ``/home``, ``/var``, ``/tmp``. Questo
però con il partizionamento diretto rende meno flessibile la eventuale
riallocazione dello spazio disco.

Si tenga presente infatti che anche avendo disponibile spazio disco per
poter allargare le partizioni, l'allargamento avverrebbe sul "fondo"
pertanto sarebbe facile ridimensionare soltanto l'ultima partizione (nel
caso la ``/home``, che pur essendo quella più probabile, non è detto sia
davvero quella che ha bisogno dello spazio disco aggiuntivo).

Per questo si suggerisce, per avere maggiore flessibilità, al costo di
una leggera perdita di prestazioni in I/O, di installare usando LVM,
selezionando "guidato - usa l'intero disco e imposta LVM":

.. figure:: /installazioni_specializzate/images/fuss-server_scelta-guidato.png

quindi selezionare il disco da partizionare:

.. figure:: /installazioni_specializzate/images/fuss-server_selezione-disco.png

l'uso dei filesystem separati:

.. figure:: /installazioni_specializzate/images/fuss-server_selezione-partizioni.png

e confermare la configurazione di LVM:

.. figure:: /installazioni_specializzate/images/fuss-server_conferma-scelta.png

e l'uso di tutto lo spazio disponibile per il gruppo di volumi:

.. figure:: /installazioni_specializzate/images/fuss-server_gruppo-volumi.png

e poi la formattazione finale:

.. figure:: /installazioni_specializzate/images/fuss-server_scelta-finale.png

Una volta completato il partizionamento ed esaurita l'installazione del
sistema base verrà chiesto se aggiungere ulteriori CD o DVD, rispondere
di No:

.. figure:: /installazioni_specializzate/images/netinstall-ulteriori-cd.png

quindi alla richiesta di configurare i repository dei pacchetti, si
utilizzi il mirror più vicino, non sarà necessario, essendo sulla WAN,
utilizzare un proxy.

.. figure:: /installazioni_specializzate/images/netinstall-pacchetti.png
.. figure:: /installazioni_specializzate/images/netinstall-mirror.png
.. figure:: /installazioni_specializzate/images/netinstall-proxy.png

Si risponda come si preferisce alla richiesta di partecipare o meno alla
indagine del popularity contest, e nella selezione del software si scelgano
soltanto le voci "server SSH" e "utilità di sistema standard":

.. figure:: /installazioni_specializzate/images/netinstall-tasksel.png

e si completi l'installazione con GRUB installato sul Master Boot Record del
disco:

.. figure:: /installazioni_specializzate/images/netinstall-grubinstall.png
.. figure:: /installazioni_specializzate/images/netinstall-selectdisk.png

Completata l'installazione si riavvi il server, eventualmente rimuovendo
il CD di installazione e ripristinando l'ordine di avvio al boot.

Configurazioni post-installazione
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Completata l'installazione di Debian occorre finalizzare le configurazioni
iniziali della macchina prima di poter lanciare ``fuss-server create``. Il
primo passo è configurare la seconda interfaccia di rete per la LAN, si dovrà
modificare ``/etc/network/interfaces`` per aggiungere la relativa
configurazione con qualcosa del tipo::

  # lan
  allow-hotplug enp2s0
  iface enp2s0 inet static
        address 192.168.0.1
        netmask 255.255.255.0
        network 192.168.0.0

ed attivare l'interfaccia con ``ifup enp2s0``.

Occorrerà poi configurare le sorgenti software per i pacchetti,
aggiungendo in ``/etc/apt/sources.list`` le righe per il repository di
backports e per quello di FUSS::

  deb http://deb.debian.org/debian/ bookworm-backports main
  deb [signed-by=/usr/share/keyrings/fuss-keyring.gpg] http://archive.fuss.bz.it/ bookworm main

infine si dovrà importare la chiave GPG del repository di FUSS con::

  # apt install gnupg
  # wget -qO /usr/share/keyrings/fuss-keyring.gpg https://archive.fuss.bz.it/apt.gpg
  # apt update

a questo punto si potrà installare il pacchetto del fuss-server::

  # apt install fuss-server

Una volta completata la configurazione iniziale della macchina, si potrà
proseguire con la configurazione del fuss-server come già illustrato nella
sezione :ref:`configurazione-fuss-server`.

.. _installazione-fuss-client:

Installazione tradizionale di FUSS Client
=========================================

Si passa ora all'installazione del primo client.

Preparazione chiavetta USB
--------------------------

Come prima cosa è necessario scaricare l'ultima versione dell'immagine
ISO live Xfce di Debian 12 "bookworm" prendendo dall'indirizzo
https://cdimage.debian.org/mirror/cdimage/release/current-live/amd64/iso-hybrid/
il file ``debian-live-<versione>amd64-xfce.iso``

È necessaria una chiavetta USB con taglia  minima di almeno 4 GB sulla
quale va copiata l'immagine ISO scaricata. Come detto anche sopra, in
GNU/Linux si può usare il comando ``dd``. Dopo aver inserito la
chiavetta nel PC ove è disponibile l'immagine, verificare con il comando
``lsscsi`` quale dispositivo è stato assegnato alla chiavetta.
Nell'esempio usiamo ``/dev/sdX`` dove X può essere una delle lettere
``a``, ``b``, ``c`` ecc.
Nell'ipotesi che la ISO scaricata si per architettura amd64, come root,
dare il comando

.. code-block:: console

   dd if=/PERCORSO_IMMAGINE/debian-live-<versione>-amd64-xfce.iso of=/dev/sdX bs=4M status=progress

Preparata la chiavetta USB, inserirla nel PC/notebook e dopo averlo
avviato premurarsi di scegliere come dispositivo di boot la chiavetta
stessa.

Procedura di installazione guidata
----------------------------------

Per immagini viene mostrata di seguito la procedura di installazione del
primo client. Si è scelto l'installer da console (Debian Installer). In
alternativa si può optare per l'installer grafico (Graphical Debian
Installer).

.. note:: Se si vuole utilizzare FUSS in modalità `LIVE`, si scelga la
   prima opzione. Le credenziali dell'utente di default sono ``user`` -
   ``live``.

.. figure:: images/fuss-client_01.png

Scelta di lingua e tastiera.

.. figure:: images/fuss-client_02.png

.. figure:: images/fuss-client_03.png

.. figure:: images/fuss-client_04.png

Inserire il nome del host.

.. figure:: images/fuss-client_05.png

Il dominio interno nel quale si colloca il host, come definito durante
l'installazione del server.

.. figure:: images/fuss-client_06.png

Impostazione della password di root.

.. figure:: images/fuss-client_07.png

.. figure:: images/fuss-client_09.png

Creazione di un utente locale.

.. figure:: images/fuss-client_10.png

.. figure:: images/fuss-client_11.png

.. figure:: images/fuss-client_12.png

.. figure:: images/fuss-client_13.png

Partizionamento dei dischi. Si scelga il partizionamento manuale
impostando una partizione di swap ed una per la radice (/ o root) del
filesystem.

.. figure:: images/fuss-client_14.png

.. figure:: images/fuss-client_15.png

.. figure:: images/fuss-client_16.png

.. figure:: images/fuss-client_17.png

.. figure:: images/fuss-client_18.png

.. figure:: images/fuss-client_19.png

.. figure:: images/fuss-client_20.png

.. figure:: images/fuss-client_21.png

.. figure:: images/fuss-client_22.png

.. figure:: images/fuss-client_23.png

.. figure:: images/fuss-client_24.png

.. figure:: images/fuss-client_25.png

.. figure:: images/fuss-client_26.png

Al termine scrivere le modifiche sul disco.

.. figure:: images/fuss-client_27.png

Inizia l'installazione del sistema.

.. figure:: images/fuss-client_28.png

L'installer cerca i pacchetti nel CD-ROM di installazione che non
esiste. Semplicemente ignorare l'errore e proseguire premendo
:guilabel:`Continua`.

.. figure:: images/fuss-client_29.png

Scegliere un mirror di rete.

.. figure:: images/fuss-client_30.png

.. figure:: images/fuss-client_31.png

.. figure:: images/fuss-client_32.png

Impostare il proxy a ``http://proxy:8080`` dove ``proxy`` risponde al
FUSS Server.

.. figure:: images/fuss-client_33.png

.. figure:: images/fuss-client_34.png

Installare il boot loader GRUB nel master boot record del disco sul
quale si sta installando il sistema.

.. figure:: images/fuss-client_35.png

.. figure:: images/fuss-client_36.png

.. figure:: images/fuss-client_37.png

.. figure:: images/fuss-client_38.png

Al termine la macchina va riavviata.

Configurazione FUSS Client
--------------------------

Dopo il riavvio si acceda come root. La password preimpostata è ``fuss``
e si consiglia di cambiarla con il comando ``passwd``.

È necessario configurare i repository FUSS. Abilitare pertanto sia i
repository FUSS che `bookworm-backports` in ``/etc/apt/sources.list``:

.. code-block:: html

    deb [signed-by=/usr/share/keyrings/fuss-keyring.gpg] http://archive.fuss.bz.it/ bookworm main
    deb http://httpredir.debian.org/debian bookworm-backports main

La sources.list dovrebbe pertanto risultare ad esempio:

.. code-block:: html

    # See https://wiki.debian.org/SourcesList for more information.
    deb http://deb.debian.org/debian bookworm main
    deb-src http://deb.debian.org/debian bookworm main

    deb http://deb.debian.org/debian bookworm-updates main
    deb-src http://deb.debian.org/debian bookworm-updates main

    deb http://security.debian.org/debian-security/ bookworm-security main
    deb-src http://security.debian.org/debian-security/ bookworm-security main

    # bookworm-backports
    deb http://httpredir.debian.org/debian bookworm-backports main

    deb [signed-by=/usr/share/keyrings/fuss-keyring.gpg] http://archive.fuss.bz.it/ bookworm main


Se invece si ha la necessità di scaricare anche i pacchetti non-free si
aggiungano a "main" anche "contrib", "non-free" e "non-free-firmware"::

    # See https://wiki.debian.org/SourcesList for more information.
    deb http://deb.debian.org/debian bookworm main contrib non-free-firmware non-free 
    deb-src http://deb.debian.org/debian bookworm main contrib non-free-firmware non-free

    deb http://deb.debian.org/debian bookworm-updates main contrib non-free-firmware non-free
    deb-src http://deb.debian.org/debian bookworm-updates main contrib non-free-firmware non-free

    deb http://security.debian.org/debian-security/ bookworm-security main contrib non-free-firmware non-free
    deb-src http://security.debian.org/debian-security/ bookworm-security main contrib non-free-firmware non-free

    # bookworm-backports
    deb http://httpredir.debian.org/debian bookworm-backports main contrib non-free-firmware non-free

    deb [signed-by=/usr/share/keyrings/fuss-keyring.gpg]  http://archive.fuss.bz.it/ bookworm main contrib non-free


.. note:: Se si è dietro un FUSS server, perché sia possibile scaricare
   la chiave di firma di APT, occorre prima definire ``export
   http_proxy=http://proxy:8080``  ed ``export
   https_proxy=http://proxy:8080``

Installare, se non già presente, il pacchetto ``wget``:

.. code-block:: console

    apt update
    apt install wget

Aggiungere la chiave di firma del repository ``archive.fuss.bz.it`` e
aggiornare con apt i pacchetti.

.. code-block:: console

    wget -qO - https://archive.fuss.bz.it/apt.key | gpg --dearmour > /usr/share/keyrings/fuss-keyring.gpg
    apt update
    apt dist-upgrade

All'occorrenza aggiungere i pacchetti Debian necessari a seconda del
contesto in cui viene installato il FUSS Client.

Creazione di un'immagine del client con Clonezilla
--------------------------------------------------

Al fine di velocizzare l'installazione del FUSS Client sui PC/notebook
rimanenti, si consiglia di creare con Clonezilla un'immagine del primo
FUSS Client. Il FUSS Server monta un'istanza di Clonezilla, eseguibile
da qualsiasi macchina presente nella LAN via PXE Boot (network boot).
Pertanto, riavviando il FUSS CLient appena creato e scegliendo l'opzione
di boot "PXE Boot", verrà caricato Clonezilla dal server e sarà
possibile creare un'immagine del primo client che verrà salvata nella
cartella ``/var/clonezilla`` sul server. Clonezilla chiederà la password
dell'utente clonezilla, che è memorizzata sul server nel file
``/root/clonezilla_cred.txt``.

Al termine della procedura di salvataggio del clone sul server, sarà
possibile installare agevolmente nuovi client lanciando parimenti
Clonezilla via network boot e scegliendo di fare il restore di
un'immagine.

Ad ogni client va attribuito un nome di host diverso. E` necessario
intervenire, pertanto, sui file ``/etc/hostname`` ed ``/etc/host``
riavviando al termine il client.

Join del client al server
-------------------------

Infine va effettuato il join del client al server lanciando da terminale
il comando ``fuss-client`` come segue:

.. code-block:: console

    fuss-client -a

Come unica interazione viene chiesto, qualora configurato, a quale
cluster associare il host (es: `aula-01`, `aula-insegnanti`, ecc.).
Inoltre va inserita per tre volte la password di root del server.

Installazione con FUSS-FUCC
===========================

FUCC è l'acronimo di Fully Unattended Clonezilla Cloning

Compilazione della lista dei computer
-------------------------------------------------

Nella cartella ``/srv/clonezilla`` (normalmente cartella standard di
clonezilla) o su altra cartella sul server FUSS che contiene le immagini
da clonare, si trova il file ``computerList.txt`` in cui bisogna
elencare i **nomi** che si vogliono assegnare ai computer specificando
il **mac-address** e **l'immagine di clonezilla** che si vuole
installare sul computer.  
Sono possibili le seguenti opzioni (e parametri, ove richiesti): ::

    [ join CLUSTER [ wifi ] || standalone ] [ light ] [ locale LOCALE ] [ keyboard KBD ]

- ``join`` Seguito dal parametro **CLUSTER**, permette di agganciare il computer al dominio e di includerlo nel gruppo **CLUSTER**. 

- ``wifi`` Consente di configurare i client per permettergli di contattare automaticamente il server attraverso la WLAN. I parametri **wifi-ssid** e **wifi-pass** vengono letti nel file **/srv/clonezilla/clientScripts/wireless_conf**, se esiste e i parametri non sono vuoti, altrimenti ci viene chiesto di inserirli interattivamente. Esempio di file wireless_conf::

    wifissid: FUSS_LAN
    wifipass: MYSUPERSECRETPASSWORD


- ``standalone`` Alternativo a join, permette di installare la macchina standalone.

- ``light`` Nel caso si parta da un'immagine clonezilla **light**  permette di installare la macchina (joinata o standalone) senza i pacchetti fuss-kids, fuss-children e fuss-education.

- ``locale`` Seguito dal parametro **LOCALE**, permette di modificare la lingua di sistema (default it_IT.UTF-8). L'elenco dei possibili locale si può trovare nel file **/srv/clonezilla/clientScripts/locale.txt**

- ``keyboard`` Seguito dal parametro **KBD**, permette di modificare il layout della tastiera (default it). L'elenco dei possibili layout si può trovare nel file **/srv/clonezilla/clientScripts/keyboard_layout.txt**


Esempi: ::

    dm-info1-03 00:0b:fe:d7:4a:cc cloneImage-img join aula_info1
    arc-aula37 7a:2d:b4:59:d6:f3 cloneImage-img join piano-primo locale de_IT.UTF-8 keyboard de 
    aula-04 6c:4b:90:26:4d:72 cloneImage-img standalone light locale es_ES.UTF-8 keyboard es
    lab-102 3c:7b:30:46:1e:8a cloneImage-img join piano-terzo wifi keyboard us locale en_GB.UTF-8 light

.. note:: 
  Si noti che l'ordine non ha importanza, purchè si rispettino la sintassi e la corrispondenza tra opzione e parametro. 



La creazione del file ``/srv/clonezilla/computerList.txt`` può essere
effettuata anche automaticamente lanciando lo script:

.. code:: bash

    fuss-fucc octolist NOME-IMMAGINE-CLONEZILLA

Viene creato il file **computerList.txt.octo-new** che può essere
copiato al posto di ``/srv/clonezilla/computerList.txt``. Verificare che
la lista contenga tutti i pc che si intende aggiornare.

In questo modo, se si reinstalla in modalità automatica, ai client
vengono assegnati gli stessi hostname e cluster di prima.

.. note:: 
   Le opzioni standalone, light, locale, keyboard, wifi dovranno 
   eventualmente essere aggiunte a mano.


Modifiche recenti
---------------------------

Tra la fine del 2024 e l'inizio del 2025 sono state introdotte due modifiche allo script clientScript 
al fine di evitare che tutti i pc clonati con clonezilla ereditassero dall'immagine lo stesso machine-id
e le stesse chiavi ssh che le rendevano indistinguibili nel file **/root/.ssh/known_hosts** .
Lo stesso ssh ci avvisa quando ci colleghiamo per la prima volta ad una macchina che la stessa chiave è presente
in diversi altri client.

Per "sanare" la situazione nei client già installati si possono utilizzare gli stessi comandi 
riportati nel seguente script bash:

.. code:: bash

    #! /bin/bash
    # Cancella le vecchie chiavi    
    rm /etc/ssh/ssh_host_*
    # Rigenera le nuove chiavi 
    dpkg-reconfigure openssh-server
    
    # Rimuovi i vecchi machine-id
    rm -f /etc/machine-id /var/lib/dbus/machine-id 
    #Rigenera i nuovi machine-id
    dbus-uuidgen --ensure=/etc/machine-id && dbus-uuidgen --ensure
    
    # Riavviare la macchina per evitare conflitti tra vecchio e nuovo machine-id
    reboot

.. note:: 
    Prevedendo lo script un riavvio, si raccomanda di lanciare lo stesso, o la serie di comandi
    che contiene, solo quando non vi sono utenti loggati.



Installazione del client
--------------------------------

Una volta eseguito quanto sopra indicato si avviino in ``network
boot(PXE)`` i PC da installare (in genere si preme il tasto ``F12`` ma
potrebbe variare a seconda del computer).

Il menu presenta due possibili scelte, **automatica** o **manuale** ,
come indicato nello screenshot seguente. La **modalità automatica è il
default** ma richiede ovviamente che il file ``computerList.txt`` sia
compilato correttamente.

In modalità automatica non occorre praticamente fare nulla, l'immagine
viene copiata, il client viene rinominato e joinato o meno alla rete ed 
eseguite le altre opzioni come indicato in computerList.txt. Nel caso la 
computerList contenga **errori o incongruenze** ci verranno poste delle 
domande in modo interattivo.
 
.. figure:: images/boot-menu.png
   :alt: clonezilla boot

   clonezilla boot

Sempre in modalità automatica, se il **mac-address** non compare
nella computerList ci verrà chiesto di inserire le varie opzioni interattivamente
anche attraverso finestre di scelta che possono semplificare parecchio il lavoro.

In caso qualcosa non vada a buon fine, si può optare per
l'installazione manuale. In tal caso il client carica clonezilla, ma per
il resto si installa il client quasi come se si usasse una chiavetta, la
partizione contenente le immagini viene montata previa autenticazione
con password (si scelga ``skip`` al momento di scegliere la sorgente).

Clonata l'immagine bisognerà lanciare::

    fuss-client -H <hostname>

per rinominare il client ed agganciarlo al server.

Configurazione del cambio automatico della password di root
---------------------------------------------------------------------------

FUCC è in grado di modificare automaticamente la password di root dei
client clonati con una criptata che gli viene passata. Per
**configurare** il ``cambio password`` eseguire sul server lo script:

.. code:: bash

    fuss-fucc rootpw

ed inserire due volte la password di root da dare ai client. Di norma
questo **script** dev'essere ovviamente eseguito **prima** di iniziare a
clonare le macchine.

Accesso all'interfaccia di amministrazione OctoNet
--------------------------------------------------

Aprendo il browser da un qualsiasi PC/notebook della LAN all'indirizzo
`<http://proxy:13402>`_, è possibile accedere all'interfaccia OctoNet di
configurazione della rete didattica e da questa, tra le altre funzioni, si
possono creare le utenze della rete didattica. L'amministratore è l'utente
`root` e la password è la Master Password impostata durante l'esecuzione di
fuss-server.

Questa modalità però comporta che tutto il traffico passi in chiaro, pertanto
è fortemente sconsigliata, si utilizzi un tunnel SSH come illustrato nel
paragrafo dedicato all'uso di OctoNet, che non espone al rischio di
intercettazione delle credenziali di accesso.

Rete WiFi WPA-Enterprise
========================

La configurazione della rete WiFi deve essere effettuata una volta che
si sia correttamente installato il *Fuss Server* (secondo le istruzioni
di :ref:`installazione-fuss-server`). In particolare si suppone che
siano già correttamente configurate le interfacce di rete per la rete
interna (quella rivolta verso le macchine dell’aula) ed esterna (quella
da cui si accede ad internet).

Per poter utilizzare il *WiFi* è necessario disporre di una
terza interfaccia di rete sulla quale sia già assegnato un IP statico da
destinare al server.
Questa interfaccia sarà quella che dovrà essere collegata fisicamente al
tratto di rete (fisicamente separato dalla rete interna
del server) al quale afferiranno gli Access Point (ad esempio vi si
potrà attaccare un access point senza autenticazione). Negli esempi
successivi assumeremo che si tratti di ``ens20``.

Per installare il necessario a gestire la rete Wi-Fi con la nuova
infrastruttura occorre eseguire il comando::

   fuss-server cp

che provvederà a richiedere, qualora non siano già definiti, i dati
necessari alla configurazione. Come per gli altri questi vengono
mantenuti nel file ``/etc/fuss-server/fuss-server.yaml`` In particolare
saranno richiesti:

-  interfaccia di rete su cui attestare la rete WiFi (nell'
   esempio ``ens20``)
-  indicazione della rete IP e relativa maschera (ad esempio
   ``10.10.0.0/22``)

Un esempio di sessione di installazione è il seguente::

   root@fuss-server-iso:~# fuss-server cp
   ################################################################################
   Please insert Hot Spot Interface

   The Hotspot interface of the server, ex. 'eth3'
   Your choice? ens20
   ################################################################################
   Please insert Hot Spot Network (CIDR)

   The Hotspot network of the server, ex. '10.1.0.0/24'
   Your choice? 10.10.0.0/22
   ...

Il server FUSS, una volta ultimata la configurazione, esporrà la porta
1812 per l'autenticazione RADIUS da parte del controller / degli 
access point e permetterà (ma registrerà) il traffico verso la WAN.
Solo le porte 80 e 443 sono permesse.


.. warning::
   Se si sta aggiornando un'infrastruttura esistente, si tenga presente
   che, una volta operativo, il traffico verso internet sarà sempre
   consentito. Valutare pertanto di scollegare gli Access Point
   o impostare preventivamente una password alla rete WiFi per
   evitare accessi imprevisti prima che WPA Enterprise sia operativo.


.. note::
   l’installazione della rete WiFi crea il gruppo ``wifi`` (si
   vedano a tal proposito i due file ``/etc/group`` ed
   ``/etc/octofuss/octofuss.conf``). Di default gli utenti di una rete
   scolastica non appartengono al gruppo ``wifi`` e pertanto non hanno
   l’autorizzazione per accedere al captive portal; devono essere
   esplicitamente autorizzati in OctoNet.



Abilitazione di un IP ad eseguire query verso FreeRadius
--------------------------------------------------------

La policy di default configurata da FUSS Server prevede di rigettare
tutte le query che pervengono a FreeRadius.

Si rende pertanto necessario inserire il controller ed altri eventuali
dispositivi che dovranno contattare FreeRadius nel file
`/etc/freeradius/3.0/clients.conf`::


   # FUSS ARUBA AP
   client 10.1.1.250/32 {
   	secret = testing123
   	nas_type = other
   }
   ...

Sostituire il parametro `secret`, qui valorizzato con `testing123`,
con un'opportuna password generata randomicamente.



Autenticazione via RADIUS su controller Aruba
---------------------------------------------

Come prima cosa è necessario collegarsi via browser all'interfaccia
web di management del controller ed eseguire l'accesso.

Selezionare quindi "Configurazione" > "Reti".

I parametri da modificare sono i medesimi sia se si crea una nuova
rete sia se se ne modifica una esistente.

Una volta aperta la procedura di creazione/modifica della rete WiFi,
selezionare "Mostra opzioni avanzate" in basso a sinistra.

Giungere allo step 3 (Sicurezza).

Una volta selezionato "Enterprise" come "Livello di sicurezza" e
"WPA2-Enterprise" come "Gestione chiavi", la voce
"Server di autentic. 1" diventa configurabile.
Crearne uno nuovo (o selezionare quello già creato se più reti WiFi
sono presenti) come in figura.


.. figure:: images/ap-controller/radius-server-1.png


.. figure:: images/ap-controller/radius-server-2.png


La configurazione della rete risulterà quindi come di seguito:


.. figure:: images/ap-controller/wifi-sec.png


È possibile salvare la configurazione della rete e constatare
che questa funzioni correttamente in modalità WPA2-Enterprise.



Postazioni prive di collegamento cablato alla LAN FUSS
======================================================

In un’ottica di costante miglioramento dal punto di vista tecnologico e
dell’esperienza utente, a partire dal FUSS Server 12 la rete WiFi non
viene più gestita tramite Captive Portal. Il server espone la porta del
servizio FreeRadius e demanda agli AccessPoint, attraverso il protocollo
WPA Enterprise, l’autenticazione degli utenti.

Questo aggiornamento complica ulteriormente la gestione ibrida fatta fino
ad ora dei portatili condivisi e collegati tramite WiFi, utilizzati
con l’utenza Guest. Da questo il cambio di paradigma nella gestione dei
portatili discussa nel presente documento.


.. note::
   Apparato condiviso: dispositivo utilizzato esclusivamente nella
   sede scolastica, condiviso da più utenti e senza una connessione
   cablata alla LAN.

   Ad esempio: PC portatile utilizzato come PC d’aula.

Schema di funzionamento
-----------------------

Attraverso il multi-SSID e l’utilizzo delle VLAN, gli apparati
condivisi privi di connessione cablata vengono equiparati a
quelli che invece ne sono dotati. La LAN viene trasportata
sull’infrastruttura WiFi e consegnata sull’apparato condiviso.

Divenendo, di fatto, come qualsiasi altro PC presente nella
rete, una volta configurato il suo funzionamento al di fuori
della portata della rete WiFi sarà impossibile.

L’esperienza utente sarà del tutto equiparata ai PC fissi,
permettendo di accedere ai file memorizzati nella home,
le cartelle condivise, ecc.


Interconnessione della LAN all’infrastruttura WiFi
--------------------------------------------------

Si rende necessario far sì che gli AccessPoint dispongano di
un collegamento verso la rete LAN perché venga trasmessa
attraverso un SSID dedicato. La soluzione più rapida è quella
di trasportare la LAN sulla rete del WiFi attraverso una VLAN.

Di seguito sono descritti i due possibili approcci alla
creazione di una seconda VLAN su rete WiFi.


Cross-connection degli switch
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


.. figure:: images/wifi-cross-connection.png


Consiste nel realizzare l’interconnessione con una patch direttamente
tra lo switch LAN e quello WiFi. Richiede che lo switch lato rete WiFi
sia di tipo managed. In questo caso è sufficiente configurare la porta
di prelievo della LAN sullo switch WiFi come “port to VLAN” ovvero
prelievo in untagged e trasporto tagged.

In alternativa, qualora fosse supportato, è possibile usare il
re-mapping della VLAN 1 verso una tagged sulla porta di consegna verso il WiFi.


Pro:

* da un punto di vista della topologia di rete, il WiFi è in cascata come qualsiasi altro switch;
* evita traffico inutile attraverso le schede di rete e la CPU del server.

Contro:

* un reset imprevisto dello switch o lo spostamento della patch su porte diverse
  rispetto a quelle configurate causerebbe un collasso delle due reti.


Consegna del bridge su interfacce multiple lato PVE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Consiste nel riconfigurare il virtual bridge dedicato alla LAN per consegnare
su più di una porta, più specificamente sia sulla scheda di rete dedicata
alla LAN sia su una VLAN sulla scheda di rete del WiFi.

Pro:

* realizzabile anche con soli switch unmanaged;
* resiliente ad eventuali modifiche fisiche.

Contro:

* il traffico broadcast e inter-client (LAN trasportata su WiFi verso LAN fisica)
  deve attraversare entrambe le schede di rete del server e la CPU.


Esempio di configurazione
"""""""""""""""""""""""""

Si prenda come esempio la seguente configurazione iniziale:


.. figure:: images/wifi-pve-bridge.png


In questo esempio i bridge sono configurati come di seguito:

* vmbr0, dedicato alla WAN, ha come interfaccia genitore eth0;
* vmbr1, dedicato alla LAN, ha come interfaccia genitore eth1;
* vmbr3, dedicato al WiFi, ha come interfaccia genitore ens5f1.


Sarà sufficiente modificare la configurazione del vmbr1 aggiungendo la
stessa interfaccia del WiFi e la VLAN di consegna separate da un punto.


.. figure:: images/wifi-pve-bridge-setup.png


Configurazione degli AP Aruba
-----------------------------

Collegarsi al controller Aruba Instant On quindi, dal menu laterale,
scegliere “Configurazione” → “Reti”. Premere sul pulsante di aggiunta “+”.

Al primo passo, inserire il nome della rete e lasciare le altre
impostazioni come predefinite.

.. figure:: images/wifi-new-net-step-1.png

Al secondo passo, selezionare “Statica” come “Assegnazione VLAN client”
ed inserire l’ID VLAN scelto in precedenza.


.. figure:: images/wifi-new-net-step-2.png


Al terzo passo, impostare la passphrase per l’accesso tramite “WPA2-Personal”.


.. figure:: images/wifi-new-net-step-3.png


Terminare l’ultimo passo confermando le impostazioni predefinite.

Per evitare confusione da parte degli utenti, è possibile modificare la rete appena creata,
selezionare “Mostra opzioni avanzate in basso a sinistra” e, sotto “Varie”,
selezionare “Nascondi” per SSID per rendere la rete nascosta.


Provisioning degli apparati condivisi
-------------------------------------

È consigliabile eseguire la prima configurazione del client utilizzando la rete cablata.
Può essere eseguito il provisioning classico utilizzando FUSS FUCC.

Una volta effettuato il join del client alla rete sarà sufficiente eseguire
``fuss-client`` passando come argomenti l’SSID della rete WiFi estensione della LAN e la password.
Verrà automaticamente riattivato Network Manager ed impostata la connessione alla rete all’avvio.::

    fuss-client -U --wifi-ssid 'FUSS_LAN' --wifi-pass 'PASSWORD'

Fatto questo la connessione cablata non sarà più richiesta all’avvio ed
il client contatterà automaticamente il server attraverso la WLAN.
    
Prima di riavviare il client, verificare che il wifi non sia disabilitato::

    nmcli radio wifi

Qualora risulti ``disabled``, attivarlo con::

    nmcli radio wifi on
    

..  LocalWords:  FUSS client images WAN Wide ethernet access point WiFi fuss
..  LocalWords:  portal Buster ref bolzano proxmox iso USB GB dd lsscsi root
..  LocalWords:  if of bs progress Netinstall l'installer netmask gateway DNS
..  LocalWords:  filesystem home var tmp riallocazione LVM repository mirror
..  LocalWords:  proxy popularity contest SSH GRUB lan allow hotplug enp inet
..  LocalWords:  iface static address ifup backports deb buster main GPG wget
..  LocalWords:  qO apt key add update install amd block notebook Installer
..  LocalWords:  Graphical LIVE live host swap guilabel loader passwd stretch
..  LocalWords:  html https dist upgrade Clonezilla PXE CLient clonezilla eth
..  LocalWords:  restore Join join OctoNet Coova Chilli tun manual cp Please
..  LocalWords:  insert Hot Interface The Hotspot interface the Your choice
..  LocalWords:  CIDR LDAP conf firewall wifi dell'access MAC cloud init dump
..  LocalWords:  storage local VMID vmbr qmrestore lvm unique qm ide image ip
..  LocalWords:  cloudinit already exists warning Options ipconfig gw fusslab
..  LocalWords:  searchdomain blz nameserver sshkey elencochiavissh cat scsi
..  LocalWords:  memory resize onboot
