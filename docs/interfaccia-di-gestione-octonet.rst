************************************
L'interfaccia di gestione di OctoNet
************************************

Architettura
============

Servizi necessari al funzionamento
----------------------------------

Per poter utilizzare l'interfaccia di gestione di *OctoNet*, occorre che
sia attiva tutta l'infrastruttura di gestione *Octofuss*) fornita del
*Fuss Server*, che prevede la presenza dei seguenti tre servizi (gestiti
con *systemd* e lanciati all'avvio del server):

* **octofussd**: demone di gestione dell'infrastruttura di gestione
  *Octofuss* (verificabile con ``systemctl status octofussd``);
* **octonet**: servizio che fornisce l'interfaccia web di *OctoNet*
  (verificabile con ``systemctl status octonet``);
* **octofuss-client**: demone di controllo della macchina locale, da
  installare anche sul server (verificabile con ``systemctl status
  octofuss-client``).

Accesso all'interfaccia web
---------------------------

Il *Fuss Server* fornisce l'interfaccia di gestione via web *OctoNet*, cui si
accede tramite browser.

Sul server stesso si può accedere usando l'indirizzo
`<http://localhost:13402>`_; dato che l'accesso è fornito senza
cifratura non è sicuro collegarvisi direttamente da altre macchine, ma
si può usare un tunnel SSH: su un terminale da un qualunque client della
rete interna eseguire::

   $ ssh sshuser@proxy -L 13402:localhost:13402

dove ``sshuser`` è un qualunque utente con accesso SSH sul server (cioè
facente parte del gruppo ``sshaccess``).

Per abilitare l'accesso anche alla nuova piattaforma :doc:`fuss-manager`
occorre collegare anche la porta ``1232``::

  $ ssh sshuser@proxy -L 13402:localhost:13402 -L 1232:localhost:1232

Una volta collegati in console, si potrà accedere all'interfaccia web
usando un browser sul client puntandolo all'indirizzo
`<http://localhost:13402>`_, dove si otterrà la seguente schermata di
accesso:

.. figure:: images/browser-to-octonet-login.png

Con l'installazione del *Fuss Server* viene creato un accesso di default
con username ``root`` e password uguale alla master password impostata
durante l'esecuzione del comando ``fuss-server create`` (l'utenza creata
è interna al servizio ``octofussd`` e non ha nulla a che fare con
l'utenza ``root`` di sistema). Una volta effettuato l'accesso si otterrà
una pagina come la seguente:

.. figure:: images/browser-to-octonet-justlogged.png

Nella pagina è disponibile, nella colonna sulla sinistra, il menu delle
diverse funzionalità di gestione fornite da OctoNet. Si tenga presente che il
menu è dinamico e mostra le diverse funzionalità solo quando queste sono
disponibili (se ad esempio non sono state abilitate le quote disco, la
relativa voce non sarà presente).

La barra in alto riporta a destra un menù utente che consente di scollegarsi,
ed alla sua sinistra un menu di scelta della lingua (sono disponibili inglese,
italiano e tedesco). Nel seguito faremo riferimento alla versione in italiano.

L'interfaccia a riga di comando octofussctl
-------------------------------------------

L'infrastruttura di *Octofuss* mette a disposizione un tool a riga di comando,
``octofussctl``, che consente di effettuare le operazioni di gestione in forma
scriptabile.

Il comando richiede le stesse credenziali di accesso utilizzate per *OctoNet*,
e fornisce sia una linea di comando interattiva, che la possibilità di
eseguire operazioni in batch (scrivendo i relativi comandi all'interno di un
file di testo da passare al comando con l'opzione ``-b``).

Per l'accesso occorre indicare la URL che identifica il server, nella forma::

    octofussctl http://localhost:13400/conf

si può indicare l'utente con l'opzione ``-u`` (o con la variabile di ambiente
``OCTOFUSS_USER``) mentre la password, se non indicata nella variabile di
ambiente ``OCTOFUSS_PASSWORD``, viene richiesta sul terminale. Le credenziali
sono le stesse che si usano per *OctoNet* (e si applica quanto detto in
precedenza).

I comandi disponibili all'interno della shell di ``octofussctl`` sono elencati
dal comando ``help``, ed il relativo funzionamento con ``help comando``.

Con ``octofussctl`` diventa possibile automatizzare alcune operazioni che
diventerebbero molto macchinose da eseguire attraverso l'interfaccia web; ad
esempio qualora si volessero cancellare tutti gli utenti di un gruppo
(l'interfaccia web consente di rimuoverli da un gruppo, ma non di
cancellarli), si potrebbe usare il comando ``delete`` in un ciclo come::

  export OCTOFUSS_PASSWORD=password
  export OCTOFUSS_USER=root
  for i in $(members gruppo); do
    octofussctl http://localhost:13400/conf delete /users/users/$i
  done


Gestione Utenti e Gruppi
========================

Si può accedere alla gestione utenti di OctoNet attraverso la voce *Utenti e
Gruppi* che abilita sulla barra superiore (sulla sinistra) un menu omonimo da
cui accedere alle altre pagine.

.. figure:: images/browser-to-octonet-user-group-menu.png


Visualizzazione utenti
----------------------

Una volta selezionata la gestione utenti si viene portati sulla pagina di
gestione degli utenti (corrispondente alla voce *Tutti gli utenti* del
suddetto menu).

.. figure:: images/browser-to-octonet-user-list.png

La pagina visualizza una tabella con tutti gli utenti creati sul server (solo
quelli gestiti in maniera centralizzata attraverso il servizio LDAP), due di
questi, ``admin`` e ``nobody`` sono utenti di servizio sempre presenti che non
devono essere modificati (sono disabilitati, e per questo contraddistinti da
una crocetta rossa).

Si può cambiare il numero di utenti visualizzati selezionando una delle
possibilità nel menu a tendina *Visualizza* sulla sinistra o eseguire una
ricerca sul nome utente inserendo una stringa nel campo *Cerca*.

Cliccando sul link con il nome utente in seconda colonna o sul pulsante
modifica sulla riga dello stesso, è possibile accedere alla pagina di gestione
dell'utente stesso:

.. figure:: images/browser-to-octonet-user-admin-edit.png


Gestione gruppi
---------------

Si tenga conto che prima di creare un utente, se si vuole che questo sia
assegnato ad un qualche gruppo specifico, occorre che quest'ultimo esista.
L'elenco dei gruppi di ottiene dal menu *Utenti e Gruppi -> Tutti i gruppi*,
alcuni di questi sono automaticamente definiti (ad uso di Samba)
all'installazione del *Fuss Server*, e sono quelli mostrati nella pagina
seguente:

.. figure:: images/browser-to-octonet-groups-initial.png

Da questa pagina si può aggiungere un nuovo gruppo con il pulsante *Crea nuovo
gruppo* o selezionando la stessa funzionalità dal menu *Utenti e Gruppi*, si
otterrà la pagina di creazione:

.. figure:: images/browser-to-octonet-group-create.png

in cui occorre inserire il nome del gruppo (che deve essere indicato usando
solo lettere minuscole e numeri, senza spazi o altri caratteri di
interpunzione).  Una volta creato il gruppo si verrà riportati nella pagina
dello stesso, che ne elenca i membri (all'inizio nessuno):

.. figure:: images/browser-to-octonet-group-initiallist.png

É opportuno creare un gruppo per ogni classe che si vuole definire, ed
eventuali altri gruppo per tipologia di utenti (tecnici, segreteria, docenti,
ecc.). Ma si tenga conto che il gruppo docenti (o qualunque gruppo il cui nome
inizi per ``docent``) ha un ruolo speciale per la gestione della scadenza
delle password secondo le normative della privacy, ed è riservato alle utenze
degli insegnanti.

Una volta che si sia creato un gruppo questo potrà essere usato nelle pagine
di gestione o creazione degli utenti, e questi vi potranno essere inseriti o
tolti. Inoltre premendo sul pulsante *Modifica* in alto a destra si passerà
alla pagina di gestione del gruppo:

.. figure:: images/browser-to-octonet-group-edit.png


I membri presenti sono presenti sono illustrati nella casella di testo
*Membri* come pulsanti, e possono essere tolti dal gruppo cliccando sulla
crocetta nel pulsante, cliccando invece nell'area vuota verrà presentato un
menu a tendina con gli utenti disponibile con cui è possibile aggiungere nuovi
membri al gruppo. Una volta completate le modifiche si dovranno salvare con il
pulsante *Salva*.

Ulteriori operazioni sono possibili con i pulsanti a destra, in tal caso
l'applicazione è immediata ed una volta effettuata l'operazione si viene
riportati nella pagina del gruppo; le operazioni sono:

* con *Aggiungi gruppo* è possibile aggiungere tutti gli utenti del gruppo
  corrente ad un altro gruppo, selezionato dal menu a tendina sovrastante;
* è possibile rimuovere in un colpo solo tutti gli utenti dal gruppo con il
  pulsante *Rimuovi tutti gli utenti*;
* è possibile aggiungere in blocco dei Permessi a tutti gli utenti del gruppo
  con il pulsante *Aggiungi permesso*, selezionando quale permesso dal menu a
  tendina soprastante;
* analogamente con il pulsante *Aggiungi permesso* si potrà rimuovere un
  permesso, sempre da selezionare dal menu a tendina soprastante, a tutti gli
  utenti del gruppo.


Creazione e gestione utente singolo
-----------------------------------

Una volta che si sono creati i gruppi necessari per creare un utente occorre
selezionare la voce *Utenti e Gruppi -> Crea utente* dal menu di gestione
*Utenti e Gruppi* che porta nella pagina di creazione illustrata di seguito:

.. figure:: images/browser-to-octonet-user-create-empty.png

In questo caso occorrerà specificare anzitutto un nome utente, di nuovo
occorre indicare lo stesso utilizzando solo lettere minuscole e numeri, senza
spazi o altri caratteri di interpunzione, nello scriverlo verrà avvalorato
automaticamente il campo della *Directory Home* con il default (che è sempre
``/home/nomeutente``). Se l'utente è uno studente è opportuno indicare come
*Gruppo Primario* quello corrispondente alla sua classe, ed indicarne il nome
completo (qui si possono usare maiuscole e spazi) nel campo omonimo.

I campi *Shell predefinita*, *Gruppo Controllore* e *Directory Home* si
possono lasciare al default; eventualmente si può modificare quest'ultima se
si è deciso di suddividere le home degli studenti per classe, usando un
percorso del tipo ``/home/studenti/nomeclasse/nomestudente``.

La password deve esser immessa due volte (uguale nei campi *Password* e
*Conferma*). Eventuali ulteriori gruppi di cui si vuole l'utente faccia parte
vanno indicati nel campo *Gruppi* (cliccando all'interno vengono mostrati in
una tendina quelli che corrispondono al testo immesso).

Il campo *Permessi* serve a controllare i permessi che verranno assegnati
all'utente perché questo possa utilizzare le relative funzionalità quando si
collega su un client. Questi corrispondono ad altrettanti gruppi locali dei
client nel quale l'utente verrà automaticamente inserito (si tenga conto però
che questo non è immediato, occorre che il client sia acceso e si sincronizzi
con il server, per cui possono, nella peggiore delle ipotesi, passare anche 5
minuti perché questo avvenga).

Inoltre, perché venga abilitata la navigazione su internet è necessario
che venga rinnovata la cache di ``nscd``: per forzare un aggiornamento
si può usare la voce *Propaga i permessi di accesso alla rete* nel menù
*Utenti e Gruppi*.

.. figure:: images/browser-to-octonet-user-create-filled.png

Il default propone i permessi per l'accesso alla scheda sonora, ai dispositivi
rimuovibili, allo scanner, ad internet ed al CDROM, secondo l'elenco in
tabella.

+-----------+--------------------------------------------------------------+
| Permesso  | Significato                                                  |
+===========+==============================================================+
| plugdev   | Può utilizzare dispositivi rimuovibili (chiavette USB, ecc.) |
+-----------+--------------------------------------------------------------+
| cdrom     | Ha accesso a CD e DVD                                        |
+-----------+--------------------------------------------------------------+
| audio     | Può utilizzare i dispositivi audio (scheda sonora)           |
+-----------+--------------------------------------------------------------+
| video     | Può usare dispositivi di registrazione video (ex. webcam)    |
+-----------+--------------------------------------------------------------+
| internet  | Può navigare il web attraverso il proxy                      |
+-----------+--------------------------------------------------------------+
| scanner   | Può utilizzare uno scanner                                   |
+-----------+--------------------------------------------------------------+
| lpadmin   | Può amministrare le stampanti                                |
+-----------+--------------------------------------------------------------+
| bluetooth | Può comunicare con il servizio bluetooth attraverso dbus     |
+-----------+--------------------------------------------------------------+
| netdev    | Può amministrare le interfacce di rete via Network Manager   |
+-----------+--------------------------------------------------------------+

Cliccando sulla crocetta essi possono essere rimossi, cliccando nel campo
viene mostrato un menu a tendina che mostra quelli disponibili, con quelli
presenti in grigio, ed è possibile aggiungerne altri. Una volta premuto il
pulsante *Salva* l'utente verrà creato e si verrà portati sulla relativa
pagina di gestione:

.. figure:: images/browser-to-octonet-user-student1-created.png

Si noti come l'utente, non essendo stato classificato come docente, riporti
nella riga *Unità* il valore *studente*. Come accennato questo parametro viene
gestito in modalità automatica sulla base del gruppo principale dell'utente, e
determina le modalità con cui viene gestita la scadenza delle password, che
non c'è per uno studente (che non tratta dati personali) mentre viene
applicata per i docenti secondo i requisiti della normativa sulla privacy.

Nel caso si voglia creare un utente per un docente occorrerà allora utilizzare
come gruppo principale (**attenzione**, gruppo principale, nella voce *Gruppo
primario* e non in uno dei gruppi ausiliari indicati nella voce *Gruppi*) un
gruppo il cui nome inizi con la stringa ``docent`` (è così possibile creare
più gruppi per i docenti, per differenziare eventuali diritti di accesso, ad
esempio per il *Gruppo Controllore*).

Un esempio è nella pagina seguente, dove oltre ad usare il gruppo ``docenti``
si assegna all'utente anche il privilegio ``lpadmin`` che consente di
effettuare la gestione delle stampanti.

.. figure:: images/browser-to-octonet-user-docente1-create.png

con l'utilizzo del gruppo  ``docenti`` come gruppo principale il nuovo utente
verrà classificato nella riga *Unità* come docente:


.. figure:: images/browser-to-octonet-user-docente1-created.png

Una volta creato l'utente si potrà modificarne le impostazioni, disabilitarlo
o eliminarlo cliccando sul pulsante *Modifica* della sua pagina di gestione:

.. figure:: images/browser-to-octonet-user-student1-edit.png

Da questa pagina, oltre a cambiare le proprietà inserite in fase di creazione
(tutte tranne il nome utente, che non può essere cambiato via *OctoNet*, se lo
si è sbagliato si deve cancellare e creare da capo), sono possibili alcune
ulteriori operazioni:

#. si possono (quando sono state configurate ed abilitate) inserire le quote
   disco (nella sezione *Modifica Quote*) dell'utente

#. si può cancellare l'utente con il pulsante *Elimina utente*; l'utente verrà
   cancellato e per non perdere eventuali suoi dati verrà creato un archivio
   ``username.tar.gz`` con il contenuto della sua home directory nella
   directory sotto cui questa si trovava

#. si può disabilitare l'utente con il pulsante *Disabilita utente*, nel qual
   caso l'accesso dell'utente verrà disabilitato, l'utente verrà marcato come
   disabilitato nella lista degli utenti e nella sua pagina, ma il suo account
   resterà attivo e potrà essere riabilitato

#. si può riabilitare un utente precedentemente disabilitato con il pulsante
   *Abilita utente* che compare al posto di quello *Disabilita utente* nella
   pagina di modifica quando l'utente è disabilitato.

.. figure:: images/browser-to-octonet-user-student1-reenable.png


.. warning::

    ATTENZIONE!
    Nel caso in cui il pulsante di ``Modifica`` ci porti alla pagina di errore, è possibile che non siano state configurate correttamente le **quote** (vedi `<https://fuss-tech-guide.readthedocs.io/it/latest/quick-install.html#configurazione-iniziale-per-le-quote>`_     ).
    Ad ogni modo, per indagare la causa del malfunzionamento di octonet, si possono usare i comandi::

        systemctl status octonet.service

    oppure::

        journalctl -u octonet.service


Gestione manuale di utenti e gruppi
-----------------------------------

Il modo migliore per gestire utenti è usare l'apposita interfaccia di octofuss
che automatizza l'intera procedura minimizzando il rischio di errori, inoltre
è l'unico che consente la gestione dei permessi degli utenti. Questi appunti
sono utili nel caso sia invece necessario intervenire direttamente tramite gli
``smbldap-tools``. Si da per assunto che siano invocati come root sul server.

È importante notare che per il corretto funzionamento della home sul
fuss-server è necessario non solo gestire un utente LDAP/Samba, ma anche il
relativo principal Kerberos, con la stessa password.

La creazione di un utente può essere realizzata con il comando::

  smbldap-useradd -a -m nomeutente

a cui deve seguire l'impostazione di una password con il comando::

  smbldap-passwd nomeutente

È quindi necessario impostare un principal Kerberos con la stessa password,
tramite il comando @kadmin.local@::

  kadmin.local << EOF
  addprinc user@DOMINIO.LAN
  pwd
  pwd
  EOF

Si tenga presente che con questi comandi non viene impostato il valore
dell'attributo ``ou`` dell'utente che indica se questi è uno studente o un
docente. Lo si può impostare manualmente usando il comando::

  ldapvi '(uid=username)'

che apre un editor con il contenuto in formato LDIF dei dati dell'utente;
aggiungendo la riga::

  ou: studenti

agli attributi mostrati nell'editor si classificherà l'utente come studente.

Per cambiare la password dell'utente si può usare il comando visto sopra::

  smbldap-passwd nomeutente

e successivamente aggiornare anche la password del principal Kerberos con
``kadmin.local``::

  kadmin.local << EOF
  cpw user@DOMINIO.LAN
  newpw
  newpw
  EOF

Per la creazione di un gruppo di utenti si può utilizzare il comando::

  smbldap-groupadd -a nomegruppo

per aggiungere un utente ad un gruppo si può utilizzare il comando::

  smbldap-groupmod  -m nomeutente nomegruppo

mentre per toglierlo::

  smbldap-groupmod  -x nomeutente nomegruppo

Per disabilitare temporaneamente un utente si può usare il comando::

  smbldap-usermod -I nomeutente

e per riabilitarlo::

  smbldap-usermod -J nomeutente


Il gruppo controllore
---------------------

Nella creazione di un utente la sua home sarà creata con permessi ``0700``
(cioè accessibile soltanto a lui) assegnando all'utente la proprietà della
stessa, mentre per il gruppo proprietario verrà usato il gruppo preimpostato
``Domain Users``, si avrà cioè nel caso dell'esempio precedente::

  root@fussserver:~# ls -ld /home/studente1
  drwx------ 2 studente1 Domain Users 4096 lug 23 18:22 /home/studente1

è possibile però fornire accesso al contenuto della home agli utenti facenti
parte di un gruppo (detto per questo *Gruppo controllore*) da specificare nel
campo omonimo della pagina di gestione dell'utente (ad esempio il gruppo
``docenti``) cliccando sul quale si ottiene un menu a tendina sui gruppi
disponibile (sui quali viene effettuata automaticamente una ricerca su quanto
si scrive nel campo) da cui scegliere:

.. figure:: images/browser-to-octonet-user-student1-controlgroupset.png

ed una volta impostato un gruppo controllore questo comparirà nella pagina
dell'utente:

.. figure:: images/browser-to-octonet-user-student1-controlled.png

ed i permessi della sua home verranno cambiati in ``2770``::

  root@fussserver:~# ls -ld /home/studente1
  drwxrws--- 2 studente1 docenti 4096 lug 23 18:22 /home/studente1

in questo modo il gruppo ``docenti`` avrà accesso in lettura e scrittura dei
contenuti, ed i nuovi file e le directory saranno assegnati al gruppo stesso
come gruppo proprietario.


Creazione utenti in massa
-------------------------

La funzionalità, prevalentemente fornita a scopo di test e per creare blocchi
di utenti temporanei (ad esempio per accessi da fornire a esterni in una prova
di esame) è accessibile dalla voce *Creazione in massa* del menu *Utenti e
gruppi* e richiede di immettere un prefisso ed un numero di utenti da creare:


.. figure:: images/browser-to-octonet-user-masscreate.png

gli utenti vengono creati ed automaticamente viene fatto scaricare un file
(``mass-created-users-AAAA-MM-GG.csv``) in cui viene fornito l'elenco degli
stessi e delle rispettive password che sono assegnata automaticamente.

Non esiste una funzionalità di cancellazione in massa, ma questa può essere
realizzata in maniera relativamente veloce usando ``octofussctl`` da riga di
comando con::

   export OCTOFUSS_PASSWORD=password
   echo ls users/users/test* \
     | octofussctl http://localhost:13400/conf -u root \
     | tail -n +2 | tr -d " />" \
     | sed -r 's|([a-z0-9]+)|delete users/users/\1|' \
     | octofussctl http://localhost:13400/conf -u root

che a differenza della cancellazione manuale con altri tool di gestione
utenti, effettua la cancellazione come se la si fosse fatta da OctoNet,
creando gli archivi con i file degli utenti.

Creazione utenti da file CSV
----------------------------

Una seconda funzionalità per creare in massa liste di utenti è quella
dell'importazione da file CSV, che consente di preparare una lista in un file
``.csv``.

Formato del file
^^^^^^^^^^^^^^^^

Il formato CSV ha moltissime varianti, delle quali viene supportato un
limitato sottoinsieme, per questo alcune precauzioni nella creazione del file
CSV da usare per l'importazione, che deve avere queste caratteristiche:

#. **Encoding**: l'encoding del file deve essere ASCII o UTF-8 senza BOM. Il
   BOM (`Byte Order Mark <https://en.wikipedia.org/wiki/Byte_order_mark>`_)
   abbiamo visto creare problemi. Si può verificare che l'encoding sia
   corretto eseguendo il comando::

     $ file nomedelfile.csv

   e se il risultato è::

     nomedelfile.csv: UTF-8 Unicode (with BOM) text, with CRLF line
     terminators

   bisognerà convertirlo in questo modo::

     $ uconv nomedelfile.csv -t ASCII > nuovofile.csv

   e si potrà poi verificare che ``nuovofile.csv`` avrà il corretto encoding::

     nuovofile.csv: ASCII text, with CRLF line terminators

#. **Campi testuali**: i campi devono essere in formato alfanumerico evitando
   caratteri di controllo come virgolette o apici, questo è necessario per
   nomi di utenti e gruppi, ma si applica anche per i nomi e per le password,
   per cui occorre limitare i caratteri di punteggiatura ed interpunzione.

#. **Coerenza del numero di colonne**:  Nel file tutte le righe dovranno avere
   lo stesso numero di campi. Ad esempio qui si vede la prima riga che
   contiene 4 campi, e la seconda 5::

     Mario,Rossi,mariorossi,mariorossipassword
     Lucia,Bianchi,luciabianchi,luciabianchipassword,campoaggiuntivo

   e anche questo non andrà bene; si tenga conto anche che una riga vuota,
   anche se messa in coda al file, viene considerata come con 0 campi (l'a
   capo sull'ultima riga non conta, ma non ve ne devono essere altri) per cui
   un file che ne contenga una non andrà bene per questo motivo).

#. **Formato del file**: Il file *non* dovrebbe avere la prima riga di
   intestazione, ma tutte le righe dovrebbero essere relative agli utenti. Ad
   esempio un file che inizia così::

     Name,Surname,Username,Password
     Mario,Rossi,mariorossi,mariorossipassword
     Lucia,Bianchi,luciabianchi,luciabianchipassword

   non va bene, e bisognerà cancellare la prima riga in modo che il file inizi
   direttamente con le righe relative agli utenti. In questo caso il requisito
   non è stringente, in quanto l'interfaccia di importazione consente di
   escludere una eventuale riga di intestazione.

Dato che se il file da importare è molto grande esaminare la correttezza (in
particolare per il terzo requisito) non è banale, dal menu *Utenti e gruppi ->
Verifica file CSV* si può effettuare un controllo preventivo:

.. figure:: images/browser-to-octonet-user-csv-verify.png

Dove viene richiesto di scegliere il file da controllare (si aprirà il
selettore di file del desktop) e la riga in cui si è inserito l'username
(verrà verificato che non vi siano doppioni), se ad esempio partiamo da un
con una riga vuota file come::

  utente,tre,utente3,pippo,classe2
  docente,due,docente2,pippo,docenti

  docente,tre,docente3,pippo,docenti

occorrerà indicare la colonna con l'username con il numero 3, ma contenendo
questo una riga vuota otterremo:

.. figure:: images/browser-to-octonet-user-csv-verify-numcolerr.png

invece avendo un file con un nome utente indicato due volte come::

  utente,tre,utente3,pippo,classe2
  docente,due,docente2,pippo,docenti
  docente,tre,docente3,pippo,docenti
  utente,quattro,utente4,pippo,classe1
  studente,tre,studente3,pippo,classe1
  studente,quattro,studente4,pippo,classe1
  studente,cinque,studente5,pippo,classe1
  studente,cinque,studente5,pippo,classe1

otterremo:

.. figure:: images/browser-to-octonet-user-csv-verify-dupuser.png

mentre se il file è corretto (mettendo ``studente6`` nell'ultima riga)
otterremo:

.. figure:: images/browser-to-octonet-user-csv-verify-ok.png

venendo rediretti automaticamente nella pagina di importazione dei file CSV.

Questo controllo è importante perché l'importazione inserisce un utente alla
volta e si ferma in caso di errore, dopo di che non è immediato ricominciare
da dove ci si era fermati.


Prerequisiti
^^^^^^^^^^^^

Il controllo su *Utenti e gruppi -> Verifica file CSV* effettua solo un
controllo di base di coerenza del contenuto del file, perché l'importazione
finale abbia successo sono necessari una serie di ulteriori requisiti:

* Eventuali gruppi primari o gruppi controllore indicati nel file dovranno già
  essere presenti nel sistema (bisognerà crearli prima sempre tramite
  *OctoNet*). In caso ne mancassero, il sistema darà un messaggio di errore
  senza importare nulla, per evitare import parziali. Questo non vale per i
  gruppi secondari, che invece vengono creati nell'importazione.
* Nessun utente elencato nel file CSV dovrà esistere sul sistema.
* Nessuna directory home di quelle che si dovrebbero creare durante
  l'importazione dovrà essere presente.

Si ricorda che per i professori, il gruppo primario a cui aggiungerli è
``docenti``, non ``insegnanti`` o ``professori`` o altro. Quindi questo dovrà
essere il gruppo primario indicato, per i professori, nel file CSV. Il nome è
fondamentale perché è sulla base del nome che il sistema capisce che si sta
trattando un docente e non uno studente.

Il file può contenere i seguenti campi (il nome campo fa riferimento al nome
usato nella interfaccia di importazione):

+-------------------+--------------------------------------------------------+
|Nome campo         | Formato                                                |
+===================+========================================================+
|Nome               | Nome (una parola)                                      |
+-------------------+--------------------------------------------------------+
|Cognome            | Cognome (una parola)                                   |
+-------------------+--------------------------------------------------------+
|Nome completo      | Nome e Cognome                                         |
+-------------------+--------------------------------------------------------+
|Password           | password (evitare virgole e caratteri CSV)             |
+-------------------+--------------------------------------------------------+
|Nome utente        | username (solo minuscole, eventuali numeri in coda)    |
+-------------------+--------------------------------------------------------+
|Gruppo primario    | nome di un gruppo (deve esistere)                      |
+-------------------+--------------------------------------------------------+
|Gruppo controllore | nome di un gruppo (deve esistere)                      |
+-------------------+--------------------------------------------------------+
|Gruppi secondari   | nomi dei gruppi separati da spazi                      |
+-------------------+--------------------------------------------------------+
|Prefisso Home      | pathname della directory dove andranno le home         |
+-------------------+--------------------------------------------------------+

Procedura di importazione
^^^^^^^^^^^^^^^^^^^^^^^^^

Per l'importazione degli utenti da un file CSV si deve usare la pagina *Utenti
e gruppi -> Importa da CSV*, che consente di selezionare un file dalla propria
macchina col pulsante *Scegli file*, si otterrà una visualizzazione della
tabella come la seguente:

.. figure:: images/browser-to-octonet-user-csv-import-loaded.png

A questo punto bisognerà selezionare le colonne che si desidera importare
semplicemente trascinando le celle di intestazione nella colonna desiderata
(dalle altre colonne o dalla lista di quelle non assegnate in fondo alla
tabella); se una colonna non si vuole o non si deve importare, si trascini la
relativa intestazione fuori dalla tabella e l'intestazione resterà
deselezionata.

L'interfaccia consente anche l'eliminazione delle prime righe del file
importato (solo a partire dall'inizio del file). Questo può risultare utile se
il CSV ha una riga di intestazione (come quello che si può ottenere dalla
conversione di un file ``.ldif``). Per farlo occorre cliccare sulla riga
corrispondente che verrà barrata, un esempio è riportato nella seguente
immagine:

.. figure:: images/browser-to-octonet-user-csv-import-exclude.png

cliccando sulla riga successiva potrà essere esclusa anche quella, ricliccando
si rimuoverà l'esclusione (di nuovo funziona solo sull'ultima delle righe
escluse, la funzionalità di esclusione si applica solo all'inizio del file).

Il campo *Nome completo* non si può importare se viene usato in coincidenza
con *Nome* o *Cognome*, se si usano questi due campi viene creato
automaticamente unendoli.

È sempre obbligatorio selezionare la colonna *Nome utente*, se non lo si fa
l'importazione riporta un errore e non va avanti.

Se la colonna password non viene impostata gli utenti verranno creati senza
password (e non potranno accedere fintanto che non gli se ne imposta una). Una
volta finito si dovrà avere una situazione del tipo:

.. figure:: images/browser-to-octonet-user-csv-import-reordered.png

Una volta impostate le colonne si potrà premere *Carica file* e se tutto è
inizialmente corretto si dovrebbe vedere un popup che dice di non chiudere la
finestra e di attendere la fine del processo. Questo potrebbe durare diversi
minuti, o se si tratta di migliaia di utenti, anche ore (è un limite
intrinseco di LDAP).

Se tutto è stato importato correttamente, alla fine comparirà un bottone per
redirigere alla lista utenti, in cui si potrà verificare l'avvenuta
importazione.

.. figure:: images/browser-to-octonet-user-csv-import-imported.png

Importazione da un file LDIF
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Qualora si disponga dell'elenco utenti in formato LDIF (che può essere
ottenuto da un backup degli stessi, disponibile sotto ``/var/backups/slapd``
se si è installato e configurato ``fuss-backup``) è possibile eseguire una
conversione dello stesso usando il pulsante *Converti file LDIF* in alto a
destra. Si può comunque ottenere un file LDIF dall'istanza corrente con il
comando ``slapcat``, con::

   slapcat -v -l nome_del_file.ldif

.. figure:: images/browser-to-octonet-ldif-conversion-choose.png

In questo caso si dovrà scegliere un file ``.ldif`` con il pulsante *Scegli
file* e poi fargli generare il CSV premendo sul pulsante *Genera file CSV*,
questo verrà generato e fatto scaricare al browser, e si potrà andare sulla
pagina di importazione cliccando sul nuovo pulsante *Importa il file
generato*.

Si tenga presente che il file così generato conterrà delle password casuali
create da OctoNet, ed ha una riga di intestazione che deve essere rimossa
prima dell'importazione.

Gestione quote disco
--------------------

La gestione delle quote disco può essere eseguita per il singolo utente dalla
sua pagina di gestione, o utilizzando direttamente l'interfaccia di gestione
che compare nel menu alla voce *Quote disco* sulla colonna di destra di
OctoNet. Cliccando sulla stessa si verrà portati su una pagina che elenca i
filesystem disponibili:

.. figure:: images/browser-to-octonet-quota-list-filesystem.png

Si tenga conto che la funzionalità è presente soltanto se le quote disco sono
attive e configurate. Nella installazione ordinaria del *Fuss Server* questo
viene fatto per la directory ``/home`` solo se questa è montata su un
filesystem separato.

Le quote disco infatti sono applicabili solo a livello di filesystem e non per
le singole directory. Questo significa anche, se i dati sono distribuiti su
più filesystem, che non possono essere applicate in forma "globale" per un
totale generico ma possono essere impostate solo per ciascuno filesystem in
maniera del tutto indipendente.

La pagina prima elenca i filesystem disponibili indicando rispettivi *mount
point* e la relativa occupazione di spazio disco, e ripete in fondo quelli per
i quali sono abilitate le quote disco. La scelta di *Fuss Server* è abilitarle
**solo** per le home, dato che è solo li che han senso, applicandosi ai dati
degli utenti. Si tenga presente che se si è installato il  *Fuss Server*
usando solo il filesystem radice, esse non saranno attive.

Benché sia possibile cliccare su tutti i filesystem si otterrà una lista delle
quote solo per quelli su cui queste sono abilitate, nel caso precedente
``home``, per cui si otterrà:

.. figure:: images/browser-to-octonet-quota-home-user-quotes.png

Le quote disponibili sono di due tipi, le quote utente (che valgono per il
singolo utente) e le quote gruppo (applicate ai file di un gruppo), ed hanno
due limiti, *soft* ed *hard*. Il primo può essere superato per un periodo di
tempo limitato (il *grace time*, che di default vale una settimana) passato il
quale verrà applicato, il secondo viene applicato immediatamente. Le quote
utente e quelle gruppo sono indipendenti.

Inoltre le quote sono suddivise per spazio disco (*Quota disco*) e per numero
di file (*Quota file*), ed anche queste sono indipendenti l'una dall'altra, si
può cioè superare la quota sia perché si è finito lo spazio disco, o perché si
è esaurito il numero di file (per cui non si potranno creare nuovi file, ma si
potranno allargare quelli esistenti).

La pagina presenta di default le quote utente, si può passare a quelle gruppo
cliccando sulla linguetta corrispondente. Vengono visualizzati un numero fisso
di utenti o gruppi selezionabili con il menu a tendina *Visualizza* e si può
effettuare la ricerca di un utente/gruppo specifico con *Cerca*.

Per modificare uno dei valori delle quote si faccia un doppio click sulla
stessa, e comparirà una finestra di immissione, si dovrà specificare una
dimensione in kilobytes per le *Quota disco* ed un numero per le *Quota file*
e poi salvarlo; si tenga conto che occorre sempre indicare per le quote *soft*
un valore inferiore che per le *hard*.

.. figure:: images/browser-to-octonet-quota-setting.png

Come accennato si possono impostare anche le quote nella pagina di gestione
del singolo utente, nella sezione finale della pagina di modifica dello
stesso:

.. figure:: images/browser-to-octonet-user-quota-setting.png

Gestione hosts
==============

Il *Fuss Server* è in grado di gestire in maniera centralizzata i client
facenti parte di una rete scolastica, utilizzando le funzionalità presenti
nella sezione *Hosts* del menu sulla colonna di destra.


Gestione del DHCP
-----------------

Si può accedere alle pagine di gestione del DHCP dalla voce *Leases DHCP* che
porta automaticamente sulla pagina che elenca lo stato attuale dei *leases*
(vale a dire delle assegnazioni MAC Address/Indirizzo IP) presenti sul server,
e fa comparire nella barra in alto il menu *Leases DHCP* sulla sinistra.


.. figure:: images/browser-to-octonet-dhcp-leases.png

alla stessa pagina si può tornare da una qualunque delle altre della sezione
usando il link *Leases DHCP serviti* dal menu *Leases DHCP*.

Si ricordi che in fase di installazione del *Fuss Server* una delle richieste
effettuate da ``fuss-server create`` è quella dell'indicazione dell'intervallo
di indirizzi IP da assegnare dinamicamente, che viene memorizzato nel file di
configurazione ``fuss-server.yaml``, nella variabile ``dhcp_range``; lo si
potrà pertanto ottenere con il comando::

    grep dhcp_range /etc/fuss-server/fuss-server.yaml

La pagina illustrata è solo informativa e ci dice quali assegnazioni dinamiche
sono state effettuate; è possibile però impostare anche delle assegnazioni
statiche (le cosiddette "reservation") usando dal menù la voce *Crea
assegnazione statica*, che porta sulla relativa pagina di immissione:

.. figure:: images/browser-to-octonet-dhcp-reservation-create.png

ed in questo modo si possono controllare gli indirizzi assegnati alle singole
macchine attraverso il DHCP (la cosa può essere utile ad esempio per assegnare
IP fissi ad apparati di rete come le stampanti in modo da poterne gestire
l'accesso in maniera centralizzata). In genere non è opportuno utilizzare
questa assegnazione per i client, che vengono gestiti direttamente dal server,
ma solo per macchine ed apparati esterni.

La pagina richieda che si immetta un nome che identifichi l'apparato, il MAC
address della sua scheda di rete e l'indirizzo IP che si vuole gli venga
assegnato.  É opportuno che quest'ultimo sia fuori dall'intervallo di IP
dinamici identificato in precedenza. L'impostazione verrà salvata nel file
``/etc/dhcp/dhcp-reservations``.

Si tenga conto che se l'apparato è impostato per ricevere l'indirizzo in DHCP,
questo verrà assegnato dinamicamente una volta accesso, ed anche se poi si
effettua una assegnazione statica, questa non verrà effettuata fintanto che il
precedente lease non scade. Per questo per rendere effettiva l'assegnazione
statica occorre in genere riavviare l'apparato (o forzare la nuova richiesta
di un indirizzo IP, se esiste una funzionalità per farlo).

Eseguita l'assegnazione si verrà portati nella pagina che elenca le
assegnazioni effettuate, a cui si può accedere anche direttamente dal menu
*Leases DHCP* con la voce *Assegnazione statica DHCP*:

.. figure:: images/browser-to-octonet-dhcp-reservation-list.png

da questa pagina si potranno gestire anche le assegnazione presenti
cancellandole col pulsante *Elimina* (verrà chiesto conferma in una finestra di
pop-up) o modificandole con il pulsante *Modifica*, nel qual caso si verrà
riportati nella pagine di impostazione dell'assegnazione statica, potendo però
modificare solo i campi relativi al MAC address ed all'indirizzo IP (per la
modifica del nome è necessario fare una cancellazione ed una creazione da
capo).


Gestione cluster
----------------

Il *Fuss Server* consente una gestione più dettagliata di tutti i client che
vengono registrati sul server con una *join*, quando su di essi si esegue
*fuss-client -a*. In quella fase si può chiedere di inserire il client in un
*cluster*, che consente di raggruppare gruppi di client.

L'elenco dei computer gestiti dal *Fuss Server* si ottiene selezionando la
voce *Computer gestiti* dal menu della colonna di destra, che porta sulla
pagina degli host gestiti, e rende inoltre disponibile il menu *Computer
gestiti* nella barra superiore.

.. figure:: images/browser-to-octonet-host-managed-list.png

La pagina mostra l'elenco dei client registrati sul *Fuss Server* (ed è
inizialmente vuota) in una tabella dove viene mostrato un eventuale utente
collegato sulla macchina e lo stato della stessa (se accesa o spenta) e dello
schermo (se bloccato o sbloccato).

Si tenga presente che l'elenco fa riferimento a tutti i computer che sono
stati registrati, anche se questi non sono più presenti. Si potrà tornare
sulla lista selezionato dal menu *Computer gestiti* la voce *Tutti gli host*.

É anche possibile ottenere una lista di computer che sono stati osservati
sulla rete (grazie al servizio *Avahi*) usando la voce del menu *Nuovi
computer* ma si tenga conto che questa pagine contiene una lista di tutti i
computer osservati, non solo quelli presenti sulla rete interna su cui sono
posizionati i client.

.. figure:: images/browser-to-octonet-host-newhosts-list.png

Come accennato quando si esegue ``fuss-client -a`` su un client questo viene
agganciato al server, se sul server è presente un cluster, questo verrà
automaticamente selezionato, e se ne sono presenti più di uno verrà richiesta
la scelta di quale cluster utilizzare. Se non ne è presente nessuno verrà
usato come default ``None`` ed un cluster con questo nome verrà
automaticamente creato.

Pertanto è in genere opportuno creare i cluster sul server prima di eseguire
la *join* dei client, in modo che questo possa selezionare in quale essere
incluso all'esecuzione di ``fuss-client -a``, per farlo si deve selezionare
dal menu *Computer gestiti* nella barra superiore la voce *Crea nuovo cluster*
che porterà nella pagina di creazione:

.. figure:: images/browser-to-octonet-host-cluster-create.png

dove si potrà creare un cluster immettendo il nome nella casella *Nome
cluster*, si tenga conto che il nome non deve contenere spazi ed per
semplicità utilizzare solo lettere minuscole e numeri. Una volta creato si
verrà rediretti automaticamente nella pagina di gestione del cluster:

.. figure:: images/browser-to-octonet-host-cluster-created.png

e lo stesso apparirà nel menu *Computer gestiti* nella barra superiore, e lo
si potrà selezionare per arrivare alla relativa pagina:

.. figure:: images/browser-to-octonet-host-cluster-menu.png

ottenendo:

.. figure:: images/browser-to-octonet-host-cluster-hosts.png

Premendo sul pulsante *Dettagli cluster* si può poi passare alla pagina di
gestione delle macchine del cluster, da cui si possono effettuare diverse
operazioni sulle macchine che ne fanno parte.

.. figure:: images/browser-to-octonet-host-cluster-operations.png

Da questa con il pulsante *Shutdown now* si possono spegnere tutte le macchine
del cluster, con il pulsante *Disable internet* si blocca l'accesso ad
Internet delle stesse (viene creata una opportuna regola di firewall) e con il
pulsante *Lock screen* si blocca lo schermo degli utenti collegati. È inoltre
possibile inviare un messaggio agli utenti collegati (da inserire nel campo
*Message to send*) con il pulsante *Send a message* e indicare un pacchetto da
installare (da inserire nel campo *Package to install*) con il pulsante
*Install software*.

Infine si può inserire nel cluster un nuovo client usando il pulsante *Create
new computer*, scrivendone il nome nel campo *Name of the new computer*.


Gestione manuale del cluster
----------------------------

I dati dei cluster sono mantenuti sul *Fuss Server* nel file ``/etc/clusters``
il cui formato è nella forma di una riga per cluster di macchine, con campi
separati da spaziature, in cui il primo campo, quello ad inizio riga, indica
il nome del cluster, ed i successivi i nomi dei client che ne fanno parte.

Si tenga presente che nel collegamento ordinario al server i client vengono
identificati solo per hostname (con il nome singolo, non l'FQDN completo), ma
questo avviene con le versioni più recenti del client, che hanno introdotto il
supporto per la normalizzazione dei nomi e la capacità di cambiare al volo
l'hostname di una macchina prima di effettuare il collegamento.

Se si deve intervenire manualmente su questo file occorre avere alcune
accortezze, infatti il suo contenuto viene letto da ``octofussd`` all'avvio,
ed aggiornato coerentemente fintanto che si usa *OctoNet* o si aggiungono i
client con ``fuss-client``, ma se si opera manualmente su ``/etc/clusters``
occorre forzare la rilettura con ``service octofussd restart``.

Qualora si voglia creare manualmente il file si possono utilizzare i dati
raccolti grazie al servizio DHCP, per questo però è necessario che tutti i pc
che devono essere inseriti nel cluster siano accesi. In tal caso è possibile
visualizzare a schermo tutte le macchine accese con il comando::

  host -l “nome dominio.local”

e da questa lista si può creare il file ``/etc/clusters`` eseguendo (sempre
dalla console del server) il comando::

   host -l “nome dominio.local” \
   | cut -d “.” -f 1 \
   | head -n -1 \
   | sort \
   | tr '\n' ' ' >> /etc/clusters

In alternativa possiamo estrarre, piuttosto che i nomi dei client, gli IP
consegnati dal server DHCP (utile quando sulle postazioni clienti non è ancora
installato ``fuss-client`` che configura ``dhclient`` per far si che i nomi
dei clienti siano presenti sul DNS) in questo modo::

  grep lease /var/lib/dhcp/dhcpd.leases  \
  | head -n -8 \
  | cut -d " " -f 2 \
  | sort | uniq \
  | tr '\n' ' ' >> /etc/clusters

Si possono utilizzare i dati di definizione del cluster direttamente dalla
console del server con il comando::

  cssh nomecluster

oppure da una postazione client, mi devo però prima collegare al server con::

  ssh -X root@server

oppure::

  ssh -X server -l root

e avvio cssh::

  cssh nomecluster

Gestione degli host
-------------------

Oltre alla gestione generale dei client che fanno parte di un cluster, si
possono pianificare operazioni per le singole macchine. Si può accedere a
queste operazioni cliccando sul link di una macchina dalla pagine *Tutti gli
host* (o da quella di un cluster di cui la macchina fa parte), che porta sulla
pagina dell'host richiesto:

.. figure:: images/browser-to-octonet-host-singlehost-operations.png

Nella pagina vengono riportate le *Azioni* possibili sulla parte destra;
alcune di queste (*Shutdown now*, *Disable internet*, *Lock desktop*, *Send a
message*, *Install software*) sono le stesse già viste per le macchine di un
cluster in `Gestione cluster`_; a queste si aggiungono la possibilità di
inserire la macchina in un cluster (da selezionare dal menu a tendina *Add to
cluster*) con il pulsante *Add to cluster* e quella di rimuoverla da un
cluster con (da selezionare dal menu a tendina *Remove from cluster*) con il
pulsante *Remove from cluster*.

Infine è possibile aggiungere la macchina ad un aggiornamento pianificato (che
deve essere creato in precedenza, vedi `Gestione aggiornamenti`_) selezionando
lo stesso dal dal menu a tendina *Add to upgrade* con il pulsante *Add to
upgrade*, o pianificare l'esecuzione di uno degli script di gestione inseriti
sulla piattaforma (anche questo deve essere creato in precedenza, vedi
`Gestione script`_) di nuovo da selezionare dal tendina *Add to script run*
con il pulsante *Add to script run*.


Gestione aggiornamenti
----------------------

Per la gestione degli aggiornamenti delle macchine collegate al *Fuss Server*
si possono impostare dei job di aggiornamento usando la voce *Gestione
Aggiornamenti* dal menu delle operazioni di destra.

.. figure:: images/browser-to-octonet-host-upgrade-page.png

Questo porta nella pagina che elenca i lavori impostati, da cui è possibile
impostare un nuovo aggiornamento nella sezione *Crea nuovo aggiornamento*,
indicandone un nome nella casella di testo e cliccando sull'adiacente pulsante
*Crea*, che ci porterà nella pagina di impostazione dello stesso.

.. figure:: images/browser-to-octonet-host-upgrade-manage.png

Dalla pagina si potrà selezionare il tipo di aggiornamento dalla casella *Tipo
aggiornamento* (con le due possibilità *Aggiornamento pacchetti* e
*Aggiornamento distribuzione* che corrispondono all'esecuzione di ``apt-get
upgrade`` e ``apt-get dist-upgrade``) e selezionare le macchine da inserire
nell'aggiornamento o singolarmente dalla casella *Aggiungi host* o per cluster
dalla casella *Aggiungi gruppo*.

Finanto che non si inserisce almeno una macchina nell'aggiornamento non
compare il pulsante verde *Programma* che consente di programmare
l'operazione, che invece può essere cancellata in qualunque momento con il
pulsante rosso *Elimina*. Una volta inserita una macchina nell'aggiornamento
questa apparirà nella lista sulla destra, affiancata da un pulsante *Rimuovi*
che consente di toglierla dall'aggiornamento.

Quando si è finito di inserire le macchine volute nell'aggiornamento lo si
potrà programmare con il pulsante *Programma*, da quel momento non sarà più
modificabile.


Gestione script
---------------

Dalla pagina principale di **Octonet** si clicchi in basso a destra
sulla voce ``Gestore Script`` .
Ci si trova nella pagina con la ``Lista degli scripts`` .

.. figure:: images/octonet/lista_degli_script.png

Si clicchi sul pulsante ``Programma`` posto a destra dello script prescelto.

.. figure:: images/octonet/esecuzione.png

Dopo aver assegnato un ``Nome`` si clicchi sul pulsante ``Crea una nuova
esecuzione`` .

Si clicchi sul nome dell'Esecuzione appena creata.

.. figure:: images/octonet/esecuzione2.png

A questo punto si devono aggiungere gli hosts o i gruppi (cluster)
inserendoli negli appositi campi e cliccando volta per volta sul
pulsante ``Conferma`` .

Infine si lanci lo script cliccando sul pulsante nero ``Programma`` . Si
tenga presente che che lo script non viene lanciato istantaneamente ma
in genere passano alcuni minuti.

.. figure:: images/octonet/confermaeprogramma.png

Terminata l'esecuzione, essa può essere eliminata cliccando sul tasto
rosso ``Elimina``.

Gestione filtri web
===================

Tramite la voce *Rete* → *Filtro Web* nella barra laterale si accede
alla pagina di gestione dei filtri sui contenuti per la navigazione,
abilitandone il relativo menù nella barra superiore.

.. figure:: images/octonet/wf01-filtro_web.png

Le voci di menù presenti corrispondono ai file di configurazione del
programma che gestisce le regole di navigazione, documentati in
dettaglio nell'apposita sezione :ref:`file-configurazione-dansguardian`
della guida.

Le varie pagine contengono un elenco di voci, per ciascuna delle quali è
disponibile il campo *Descrizione* tramite il quale fornire dei commenti
sulla voce e sul motivo per cui è stata inserita.

La prima voce del menù, *allowed_sites* permette di gestire l'elenco di
domini ai quali è sempre consentito accedere, indipendentemente da divieti
imposti.

Il file corrispondente è ``/etc/fuss-server/content-filter-allowed-sites``.

.. figure:: images/octonet/wf02-siti_permessi.png

*banned_extensions* è un elenco di estensioni di file ai quali è
proibito l'accesso.

Il file corrispondente è ``/etc/dansguardian/lists/bannedextensionlist``.

.. figure:: images/octonet/wf03-estensioni_proibite.png

banned_sites è un'elenco di domini ai quali l'accesso è sempre proibito.

Il file corrispondente è ``/etc/dansguardian/lists/bannedsitelist``.

.. figure:: images/octonet/wf04-siti_proibiti.png

Ed infine, *config* permette di modificare la configurazione di
dansguardian stesso; in questo caso il campo *Descrizione* contiene il
valore del parametro di configurazione corrispondente.

Il file corrispondente è ``/etc/dansguardian/dansguardianf1.conf``.

.. figure:: images/octonet/wf05-config.png

Modifiche ai filtri web
-----------------------

.. figure:: images/octonet/wf06-edit.png

I campi delle pagine appena viste sono modificabili, oppure svuotabili
premendo il tasto x rosso corrispondente; nuove voci possono essere
aggiunte nelle cinque righe vuote presenti in fondo alla pagina.

Perché le modifiche vengano scritte sui file di configurazione è poi
necessario premere il tasto *Salva*, e alla fine riavviare il servizio
dalla pagina principale di questa sezione perché diventino attive.

.. figure:: images/octonet/wf07-restart.png

Gestione firewall
=================

Tramite la voce *Rete* → *Firewall* nella barra laterale si accede alla
pagina di gestione del firewall, abilitandone il relativo menù nella
barra superiore.

.. figure:: images/octonet/fw01-firewall.png

Le voci di menù presenti corrispondono ai file di configurazione del
programma che gestisce le regole di navigazione, documentati in
dettaglio nell'apposita sezione :ref:`file-configurazione-firewall`
della guida.

Le varie pagine contengono un elenco di voci, per ciascuna delle quali è
disponibile il campo *Descrizione* tramite il quale fornire dei commenti
sulla voce e sul motivo per cui è stata inserita.

Host senza accesso corrisponde al file
``/etc/fuss-server/firewall-denied-lan-hosts``

.. figure:: images/octonet/fw07-host_senza_accesso.png

Host con accesso completo corrisponde al file
``/etc/fuss-server/firewall-allowed-lan-hosts``

.. figure:: images/octonet/fw04-host_accesso_completo.png

Destinazioni consentite corrisponde al file
``/etc/fuss-server/firewall-allowed-wan-hosts``

.. figure:: images/octonet/fw02-destinazioni_consentite.png

Servizi consentiti corrisponde al file
``/etc/fuss-server/firewall-allowed-wan-services``

.. figure:: images/octonet/fw06-servizi_consentiti.png

Servizi offerti all'esterno corrisponde al file
``/etc/fuss-server/firewall-external-services``

.. figure:: images/octonet/fw05-servizi_esterno.png

.. note::
   
   Eventuali regole iptables custom non possono essere configurate da
   OctoNet ma solo modificando sul server il file
   ``/etc/fuss-server/firewall-custom-rules`` (con le dovute
   attenzioni) riavviando subito dopo il firewall (si faccia
   riferimento alla sezione :ref:`file-configurazione-firewall`).
	    
Modifiche al firewall
---------------------

I campi delle pagine appena viste sono modificabili, oppure svuotabili
premendo il tasto x rosso corrispondente; nuove voci possono essere
aggiunte nelle due righe vuote presenti in fondo alla pagina.

Perché le modifiche vengano scritte sui file di configurazione è poi
necessario premere il tasto *Salva*, e alla fine riavviare il servizio
dalla pagina principale di questa sezione perché diventino attive.


La gestione di  Host/servizi consentiti
-----------------------------------------

Oltre alle configurazioni precedenti riguardanti accessi a singoli host o
porte è stata aggiunta al firewall anche la possibilità di indicare un accesso
esterno ad una singola macchina su una porta specifica, nella sezione 
Host/servizi consentiti, che corrisponde al file
``/etc/fuss-server/firewall-allowed-wan-host-services``

.. figure:: images/octonet/fw03-servizi_consentiti.png


In questo caso, pur mantenendo la richiesta di due campi, è necessaria una
sintassi speciale dovendo gestire la specificazione di un host e di una
porta. Questo comporta il dover indicare il primo campo, indicato in figura
come "Host di rete" nella forma ``nome.dominio.o.ip:porta-protocollo`` mentre
si può mettere una stringa qualunque nel campo "Descrizione".

In sostanza per abilitare l'accesso alla macchina 1.1.1.1 sulla porta 53 udp
occorrerà inserire nel primo campo qualcosa come::

  1.1.1.1:53-udp

Si tenga conto poi quanto inserito nel campo "Descrizione" verrà ignorato
mentre Octonet mostrerà nella lista dei contenuti l'indirizzo IP o il nome a
dominio come "Host di rete" e la porta indicata con la sintassi precedente
come "Descrizione". In sostanza inserendo quanto in esempio si otterrà una
voce "Host di rete" uguale a ``1.1.1.1`` ed una voce "Descrizione" uguale a
``53/udp``.

In questo caso per modificare un valore presente non si potranno modificare i
campi mostrati da Octonet, che invece andranno riscritti da zero con il formato
indicato.


.. _`stampanti-di-rete`:

Stampanti di rete
=================

La sezione *Rete* → *Stampanti di rete* permette di gestire le stampanti
condivise.

La pagina principale della sezione presenta l'elenco delle stampanti
disponibili (ovvero "code di stampa"), con link alla relativa pagina di
configurazione, ed un pulsante che permette di raggiungere la pagina di
configurazione di *CUPS*, il servizio di gestione delle stampanti.

.. figure:: images/octonet/pr01-stampanti_di_rete.png

L'accesso a CUPS è consentito a root, oppure agli utenti del gruppo
``lpadmin``.

.. warning::

   Se si sta accedendo ad OctoNet da un computer diverso dal server,
   tramite tunnel SSH, il link *Configura stampanti e code* punterà
   all'interfaccia CUPS *della macchina da cui si sta accedendo* (se
   installato) e non del server.

   Per accedere all'interfaccia CUPS del server si può creare un secondo
   tunnel ssh col comando::

      ssh sshuser@proxy -L 13631:localhost:631

   e dopo aver aperto *Configura stampanti e code* correggere
   manualmente l'indirizzo perché punti a http://localhost:13631/

   .. figure:: images/octonet/pr03-interfaccia_cups.png

Nella pagina di modifica di una coda di stampa si trova l'elenco degli
host della rete locale abilitati ad accedere, con la possibilità di
rimuovere host individuali dall'elenco, aggiungerne (*Aggiungi host alla
coda*) oppure aggiungere con un click solo tutti gli host facenti parte
di un cluster (*Aggiungi gruppo di host alla coda*).

.. figure:: images/octonet/pr02-modifica_stampante.png

Aggiunta di una stampante
-------------------------

Per aggiungere una nuova stampante è innanzitutto necessario
configurarla su CUPS: dall'interfaccia relativa selezionare *CUPS for
Administrators* → *Adding Printers and Classes*:

.. figure:: images/octonet/pr04-cups_admin.png

Quindi premere il tasto *Add Printer* nella sezione *Printers*,
autenticandosi con un utente che sia nel gruppo ``lpadmin`` sul server.

.. tip::

   Alcuni modelli di stampante richiedono passi aggiuntivi per la loro
   installazione in CUPS; per questi si veda la miniguida
   :ref:`miniguida-stampanti`.

Si otterrà una pagina con la scelta di che tipo di stampante
aggiungere; nel caso siano già state trovate stampanti locali o di rete
queste verranno presentate in *Local Printers* o *Discovered Network
Printers* rispettivamente:

.. figure:: images/octonet/pr05-cups_add_printer.png

Selezionando una stampante che sia stata già riconosciuta, verranno
richieste un nome (che non può contenere i caratteri ``/``, ``#`` né
spazio), una descrizione ed una posizione fisica da visualizzare agli
utenti; inoltre viene richiesto se condividere la stampante (*Share This
Printer*):

.. figure:: images/octonet/pr05-cups_add_printer_settings.png

Premendo su *Continue* si raggiunge la schermata successiva, in cui si
seleziona il driver da usare per la stampante indicando dapprima il
produttore:

.. figure:: images/octonet/pr05-cups_add_printer_make.png

e, dopo aver premuto *Continue* il modello:

.. figure:: images/octonet/pr05-cups_add_printer_model.png

Premendo *Add Printer* la stampante viene aggiunta e si viene portati ad
una pagina dove è possibile cambiare le sue impostazioni di default:

.. figure:: images/octonet/pr09-cups_default_options.png

A questo punto si può ricaricare la pagina delle stampanti di rete di
octonet, dove sarà apparsa la stampante appena aggiunta:

.. figure:: images/octonet/pr10-stampanti_rete_nuova.png

E tramite *Modifica Host* si possono abilitare host ad usarla,
completando così la configurazione della nuova stampante:

.. figure:: images/octonet/pr11-aggiungi_host.png

Share Samba
===========

Creazione di uno share
----------------------

Per creare uno share samba:

* predisporre la directory sul filesystem, ad esempio in una
  sottodirectory di ``/home/SAMBA``::

     # mkdir /home/SAMBA/<nome_share>

  ed assegnarle permessi opportuni perché gli utenti possano accedervi;
  ad esempio per creare una share scrivibile dai docenti::

     # chgrp docenti /home/SAMBA/<nome_share>
     # chmod 775 /home/SAMBA/<nome_share>

* via octonet, andare sulla creazione di un nuovo share:

  - Cartelle Condivise → Servizio Samba nel menù laterale;
  - Cartelle Condivise → Crea Cartella Condivisa nel menù in alto;

  (si aprirà la pagina all'indirizzo ``/samba/create``)

* indicare:

  - nome dello share
  - path assoluto
  - eventuali parametri di accesso (guest, gruppi di utenti con accesso
    in lettura, in scrittura etc)

.. warning::

   I nomi degli share samba devono essere composti solo da lettere
   minuscole per evitare errori nell'uso successivo.

   Le versioni più recenti di octonet impediscono l'inserimento di
   caratteri diversi nel nome dello share.

.. figure:: images/share-samba/samba_create.png

Test di uno share
-----------------

Usare un client samba qualsiasi per provare le shares.

Da riga di comando
^^^^^^^^^^^^^^^^^^

Ad esempio, con ``smbclient`` da riga di comando, installato su un
fuss-client, si possono elencare le share disponibili sul server (e
visibili all'utente in uso) con::

   $ smbclient -L proxy

e ci si può collegare con::

   $ smbclient \\\\proxy\\<nome della share>

usare ``ls`` per vedere i file presenti, ``quit`` per uscire, ``help``
per vedere gli altri comandi disponibili.

Con Thunar (Gestore dei File grafico)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il Gestore dei File dei fuss-client è capace di accedere alle share
samba; si può quindi usare per testarne la configurazione.

Nella colonna laterale selezionare “Esplora la rete”, quindi aprire
“Rete Windows” e le successive cartelle fino ad arrivare alla share
desiderata.

.. figure:: images/share-samba/thunar_01_esplora_la_rete.png

.. figure:: images/share-samba/thunar_02_rete_windows.png

.. figure:: images/share-samba/thunar_03_workgroup.png

.. figure:: images/share-samba/thunar_04_server.png

Inserire la password di un utente opportuno per verificare la corretta
visibilità della share.

.. figure:: images/share-samba/thunar_05_password.png

.. figure:: images/share-samba/thunar_06_share.png

..  LocalWords:  OctoNet Octofuss Fuss systemd octofussd systemctl octonet ls
..  LocalWords:  octofuss client SSH ssh sshuser sshaccess images width align
..  LocalWords:  center username root fuss octofussctl tool scriptabile URL
..  LocalWords:  LDAP admin nobody docent Home Shell home CDROM plugdev USB
..  LocalWords:  cdrom webcam proxy lpadmin Domain Users ld drwx lug drwxrws
..  LocalWords:  created users AAAA GG csv echo tail tr sed delete  pippo MAC
..  LocalWords:  l'encoding UTF BOM Order Mark nomedelfile unicode with text
..  LocalWords:  CRLF line terminators uconv nuovofile encoding mariorossi ou
..  LocalWords:  mariorossipassword luciabianchi luciabianchipassword l'a EOF
..  LocalWords:  campoaggiuntivo Name Surname  l'username pathname ldif popup
..  LocalWords:  filesystem mount point soft grace kilobytes hosts DHCP yaml
..  LocalWords:  leases dhcp grep reservation address lease Avahi smbldap pwd
..  LocalWords:  join host tools principal Kerberos useradd nomeutente passwd
..  LocalWords:  kadmin local addprinc ldapvi uid cpw newpw groupadd groupmod
..  LocalWords:  nomegruppo usermod slapcat hostname l'FQDN l'hostname cut to
..  LocalWords:  service restart head sort dhclient DNS uniq cssh nomecluster
..  LocalWords:  Shutdown now Disable firewall Lock screen Message send Send
..  LocalWords:  message Package install new of the pc dell'host Add Remove
..  LocalWords:  from upgrade run job
