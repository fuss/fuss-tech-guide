**************
 Architettura
**************

Servizi presenti su Fuss Server
===============================

.. list-table:: Servizi
   :header-rows: 1

   * - Servizio
     - Descrizione
   * - apache
     - Server web
   * - bind
     - DNS locale
   * - e2guardian
     - Filtro sui contenuti internet (se non disabilitato)
   * - isc-dhcp-server
     - DHCP per la rete locale
   * - iptables
     - Firewall
   * - fuss-fucc
     - Gestione dell'installazione automatizzata dei client
   * - fuss-manager
     - Gestore delle macchine su reti locali (nuova versione
       sperimentale)
   * - kerberos
     - Autenticazione e autorizzazione
   * - ldap
     - Autenticazione
   * - nfs-kernel-server
     - Home condivise
   * - Octonet
     - Gestione di macchine ed utenti (legacy)
   * - Samba
     - Condivisione file
   * - squid
     - Proxy web
