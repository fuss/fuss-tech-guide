==================================
Standard installazioni FUSS
==================================

Struttura directory /home
------------------------------------------

Premessa
^^^^^^^^^^^^^^^^^^^^
Nei paragrafi che seguono si fa riferimento alla nomenclatura comunemente usata per le classi
( ad es. b-2022 per una classe della sezione B che sia entrata nel 2022).
Questa pratica, che può risultare comoda per qualche amministratore, 
non lo è per i fruitori del servizio, cioè docenti e studenti.Si invita pertanto a seguire 
lo standard illustrato al seguente `link`_ , più comprensibile ed ordinato.


Di seguito viene descritta la struttura comune delle cartelle utente (studenti e docenti) 
nonché delle cartelle condivise che dovrebbe essere adottata in ciascuna scuola.

Rete scolastica per scuola singola
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Gruppi Ldap comuni ad ogni scuola:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* ``docenti``: gruppo primario dei docenti 
* ``studenti``: gruppo primario per gli studenti, gruppo secondario per i docenti    
* ``gruppo-classe``: (facoltativo) gruppo secondario degli studenti di quella classe, coincide col nome della cartella di classe, gruppo secondario per i docenti

***Esempio*** : La cartella classi/c-2021 (nell'anno scolastico 2021/22) è la cartella comune della classe 2C. La cartella home/classi/c-2021 è accessibile in lettura e scrittura solo al gruppo classe (quindi  e agli studenti della classe ed ai relativi docenti)

.. note:: ATTENZIONE: se si usano i gruppi classe, che vengono attribuiti anche ai docenti, bisogna fare molta attenzione quando si procederà a cancellare gli utenti appartenenti a quel gruppo-classe (cioè gli alunni in uscita): si rischia in tal modo di cancellare anche i docenti aggiunti a quel gruppo. Pertanto bisogna ricordare di cancellare gli utenti appartenenti a quel gruppo-classe escludendo quelli appartenenti al gruppo docenti.

Contenuto della cartella home
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Nella cartella home si creano le seguenti cartelle per gli utenti:

* ``docenti``, permessi : 750, proprietario: root , gruppo: docenti
* ``classi`` , permessi : 750, proprietario: root , gruppo: studenti
* ``ospiti`` , permessi : 750, proprietario: root , gruppo: ospiti

Contenuto della cartella docenti
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* una cartella comune-docenti accessibile solo ai docenti, permessi : 3770, proprietario: root , gruppo: docenti
* le cartelle personali dei docenti della scuola, permessi : 700

Contenuto della cartella classi
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* una cartella per ogni classe il nome della classe è della forma [a-z]-[2-99]:
* la lettera indica la sezione, il numero a quattro cifre l'anno di entrata nella scuola; permessi : 770, proprietario: root , gruppo: studenti (se presente il gruppo-classe inserire quello)

**Esempio** : nell'anno scolastico 2024/25 a-2024 indica la classe 1A (entrata nella scuola nell'anno 2024)

All'interno della cartella classi è presente:

* una cartella comune-classe, permessi : 3770, proprietario: root , gruppo: studenti (o gruppo-classe se esiste)
* le cartelle personali degli alunni della classe

Contenuto della cartella ospiti
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* le cartelle di utenti ospiti che, anche temporaneamente, devono utilizzare la rete didattica della scuola, permessi : 770, proprietario: root , gruppo: ospiti
* una cartella comune-ospiti, permessi : 3770, proprietario: root , gruppo: ospiti

Rete scolastica per Istituti Comprensivi o Pluricomprensivi
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Gruppi Ldap comuni ad ogni istituto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* docenti: gruppo primario dei docenti
* nome-scuola: gruppo secondario per docenti e studenti di quella scuola
* studenti: gruppo primario per gli studenti, gruppo secondario per i docenti
* gruppo-classe: (facoltativo) gruppo secondario degli studenti di quella classe, coincide col nome della cartella di classe, gruppo secondario per i docenti.

.. note:: ATTENZIONE: se si usano i gruppi classe, che vengono attribuiti anche ai docenti, bisogna fare molta attenzione quando si procederà a cancellare gli utenti appartenenti a quel gruppo-classe (cioè gli alunni in uscita): si rischia in tal modo di cancellare anche i docenti aggiunti a quel gruppo. Pertanto bisogna ricordare di cancellare gli utenti appartenenti a quel gruppo-classe escludendo quelli appartenenti al gruppo docenti.

Nella cartella/home vengono create due (o più cartelle):

* `nome-scuola-1`, permessi : 750, proprietario: root , gruppo: nome-scuola-1
* `nome-scuola-2`, permessi : 750, proprietario: root , gruppo: nome-scuola-2
* `nome-scuola-3`, permessi : 750, proprietario: root , gruppo: nome-scuola-3

In ciascuna cartella delle scuole si ripete la struttura indicata nel precedente paragrafo (Rete scolastica per scuola singola).

**Esempio**:

L'istituto Comprensivo ICBZ3 ha due scuole: scuola secondaria di I grado Leonardo da Vinci, scuola primaria don Milani

Avremo le seguenti cartelle:

* /home/donmilani oppure /home/primaria
* /home/davinci oppure /home/secondaria
* /home/ospiti

Quindi nella cartella donmilani avremo:

* docenti
* classi

Anche per i gruppi vale quanto già scritto nel precedente paragrafo (Rete scolastica per scuola singola). 

La procedura descritta è necessaria per mantenere in ordine la
disposizione delle classi come illustrata, ma può essere utile anche per
sanare alberi delle home poco ordinati o nomi poco chiari per alunni e
docenti, come **2019a** per indicare la **classe-4a**. Il tutto richiede
poco tempo e solo una certa attenzione, ribadiamo, nella predisposizione
dei file csv.

Nomi utenti
------------------------

I nomi degli utenti sono definiti dal **cognome più le tre lettere iniziali del nome**, tutto in **minuscolo** .
Nell'eventualità di doppio cognome si elimina lo spazio vuoto tra un cognome e l'altro o si tiene solo il primo cognome.
Tutti i caratteri speciali (accenti, umlaut, ecc) vengono eliminati con sostituzione delle lettere accentate con quelle semplici.
La umlaut viene sostituita con il dittongo corrispondente o con lettere semplici.

Hostname
---------------------------

Nella riunione dell'8 maggio 2018 si è deciso di adottare la seguente convenzione:

Client
^^^^^^^^^^^^^
I nomi dei client dovranno essere basati sullo schema: ::

    nome_aula-numero_progressivo

Server
^^^^^^^^^^^^^
Per i nomi dei server si decide di utilizzare lo schema seguente: ::

    nome_scuola-fuss    per il server FUSS
    nome_scuola-prox     per il server fisico



.. _`link` : 

Spostamento delle classi da un anno all'altro
--------------------------------------------------------------

Premessa
^^^^^^^^^^^^^^^^^^^^^^^

Per una gestione più chiara ed ordinata dell'albero ldap si consiglia di:

* attribuire al nome delle classi il nome reale (ad es. 1b, 3c, ecc), anzichè  il nome della sezione seguito dall'anno di iscrizione (b-2024, c-2002,..)
* attribuire lo stesso nome al gruppo classe 

Ad esempio la cartella che contiene le home degli alunni di una classe della primaria (poniamo 2c) sarà ::

    /home/primaria/alunni/classe-2c-ele

Gli alunni di questa classe apparterrebbero invece al gruppo **2c-ele**

Ricordiamo che la creazione di un gruppo specifico per la classe ha lo scopo di facilitare la gestione delle classi, 
come ad esempio eliminare agevolmente le classi in uscita. Naturalmente si possono scegliere denominazioni di classi e gruppi
diverse, purchè siano coerenti ed omogenee nello stesso plesso.
Visti alcuni precedenti si preferisce aggiungere ancora una volta la seguente Nota:

.. note:: ATTENZIONE: se si usano i gruppi classe, che vengono attribuiti anche ai docenti, bisogna fare molta attenzione quando si procederà a cancellare gli utenti appartenenti a quel gruppo-classe (cioè gli alunni in uscita): si rischia in tal modo di cancellare anche i docenti aggiunti a quel gruppo. Pertanto bisogna ricordare di cancellare gli utenti appartenenti a quel gruppo-classe escludendo quelli appartenenti al gruppo docenti.


Lo script
^^^^^^^^^^^^^^^^^^^^^^

L'anno successivo chiaramente le classi ed i gruppi cambieranno (ad
esempio la classe **classe-2c-ele** diventerà **classe-3c-ele** ed il
gruppo **2c-ele** diventerà **3c-ele**.

La procedura di spostamento viene affidata ad uno script e richiede solo
una particolare attenzione nel predisporre i due file csv letti dallo
script stesso. L'esecuzione dello script prevede che, come auspicabile, tutti gli
alunni di uno stesso plesso appartengano allo stesso **gruppo
primario**. Diversamente bisognerà adattarlo alla situazione specifica o
lanciarlo, ad esempio, per gruppi più piccoli.

Ovviamente si raccomanda di analizzare bene lo script per capire
esattamente quello che esegue. In caso di dubbi rivolgersi a
info@fuss.bz.it::

    #! /bin/bash
    # Creiamo innanzitutto una copia di ldap
    # LDAP save
    slapcat > ldap-dump.ldif

    ### LDAP restore in caso di problemi
    #systemctl stop slapd
    #echo "Restoring LDAP"
    #rm -rf /var/lib/ldap/*
    #slapadd < ldap-dump.ldif
    #chown openldap:openldap /var/lib/ldap/*
    #systemctl start slapd

    # Decommentare le seguenti due righe e si commenti la successiva se
    # si intende procedere in modo interattivo

    #echo "Qual è il gruppo primario degli utenti da spostare?"
    #read GRUPPO_PRIMARIO

    #Sostituire ad alunni-ele il gruppo primario degli alunni delle
    # classi da spostare
    GRUPPO_PRIMARIO=alunni-ele
    echo "Gruppo primario: $GRUPPO_PRIMARIO"
    MEMBRI=`members -p $GRUPPO_PRIMARIO`
    echo "Membri: $MEMBRI"

    # Nella seguente parte dello script viene letto il file csv con il
    # nome vecchio e quello nuovo della classe. Le classi in uscita
    # vengono spostate in classi inesistenti (ad esempio IV media, VI
    # elementare o superiore)
    # Viene effettuato un controllo per evitare che una classe
    # sovrascriva l'altra se il csv è stato elaborato male, ma si rischia
    # di spostare solo una parte delle classi.
    # In questo caso è preferibile ripristinare la situazione iniziale,
    # sistemare il file csv e riclanciare lo script.


    while IFS=, read -r OLDHOME NEWHOME
    do
        if [ ! -d "$NEWHOME" ]
        then
            echo $NEWHOME
            mv $OLDHOME $NEWHOME
        else
            echo "Controlla l'ordine delle classi nel csv!"
            exit
        fi
    done < classi_mapping.csv

    # Con il codice  seguente si effettua lo stesso spostamento delle
    # classi nell'albero ldap; in caso di errori si esegua il restore di
    # ldap commentato ad inizio script

    declare -A classi; while IFS=, read -r OLDHOME NEWHOME ; do classi[$OLDHOME]=$NEWHOME ; done < classi_mapping.csv

    for i in `members -p $GRUPPO_PRIMARIO`
    do
        home=`smbldap-usershow $i |grep homeDirectory|awk '{print $2}'`

        echo $home

        home_tr=${home/"/"$i/}

        echo $home_tr

        home_mod=${classi[$home_tr]}

        echo $home_mod

        smbldap-usermod -d $home_mod"/"$i $i

    done

    # Infine, se esistono gruppi_classe, col seguente comando è possibile
    # aggiornarli da un anno all'altro leggendo i dati da un csv.
    # !!! Attenzione!!! Per rinominare un gruppo è necessario che il
    # nuovo gruppo non esista già, per cui il file csv va eventualmente
    # preparato con numeri decrescenti dall'alto in basso!

    while IFS=, read -r OLDGROUP NEWGROUP ; do smbldap-groupmod -n $NEWGROUP $OLDGROUP; done < group_mapping.csv

I file csv
^^^^^^^^^^^^^^^^^^^^^^^^^^

Ecco un esempio di csv (**classi_mapping.csv**) per lo spostamento delle
classi.
Come si può vedere il csv è formato da due colonne separate da una virgola:

- la colonna di sinistra contiene le classi "vecchie" e quella di destra
  le classi "nuove";

- si noti che le classi sono in ordine decrescente dal basso all'alto;

- si presti particolare cura nella preparazione del csv::

    /home/primaria/alunni/classe-5b-ele,/home/primaria/alunni/classe-6b-ele
    /home/primaria/alunni/classe-5a-ele,/home/primaria/alunni/classe-6a-ele
    /home/primaria/alunni/classe-4b-ele,/home/primaria/alunni/classe-5b-ele
    /home/primaria/alunni/classe-4a-ele,/home/primaria/alunni/classe-5a-ele
    /home/primaria/alunni/classe-3b-ele,/home/primaria/alunni/classe-4b-ele
    /home/primaria/alunni/classe-3a-ele,/home/primaria/alunni/classe-4a-ele
    /home/primaria/alunni/classe-2b-ele,/home/primaria/alunni/classe-3b-ele
    /home/primaria/alunni/classe-2a-ele,/home/primaria/alunni/classe-3a-ele
    /home/primaria/alunni/classe-1b-ele,/home/primaria/alunni/classe-2b-ele
    /home/primaria/alunni/classe-1a-ele,/home/primaria/alunni/classe-2a-ele

Il seguente file (**group_mapping.csv**) è un esempio di csv per lo
spostamento degli alunni delle classi da un gruppo a quello successivo.
Viene letto nella stessa maniera di quello precedente. Se il gruppo
nuovo esiste già, lo spostamento non avviene, per cui si deve sempre
procedere in ordine decrescente dall'alto al basso, accertandosi che i
gruppi più alti (nel nostro caso **classe-6...**) non esistano, ed
eventualmente eliminarli. ::

    5b-ele,6b-ele
    5a-ele,6a-ele
    4b-ele,5b-ele
    4a-ele,5a-ele
    3b-ele,4b-ele
    3a-ele,4a-ele
    2b-ele,3b-ele
    2a-ele,3a-ele
    1b-ele,2b-ele
    1a-ele,2a-ele

Conclusioni
^^^^^^^^^^^^^^^^^^^^^^
La procedura descritta è necessaria per mantenere in ordine la
disposizione delle classi come illustrata, ma può essere utile anche per
sanare alberi delle home poco ordinati o nomi poco chiari per alunni e
docenti, come **2019a** per indicare la **classe-4a**. Il tutto richiede
poco tempo e solo una certa attenzione, ribadiamo, nella predisposizione
dei file csv.



