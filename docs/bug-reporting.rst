.. _bug-reporting:

*************
Bug reporting
*************

Raccolta di informazioni
========================

Quando qualcosa va storto, può essere necessario segnalare un bug
(*Segnalazione*) sul progetto opportuno di
`<https://work.fuss.bz.it/>`_; prima di farlo è importante raccogliere
tutte le informazioni rilevanti per aiutare la diagnosi del problema.

numeri di versione
------------------

Piuttosto che indicare "aggiornato alla data X", meglio indicare i
numeri di versione precisi del pacchetto (o pacchetti) che si sospetta
possano essere coinvolti nel problema, ottenendoli ad esempio con::

   dpkg -l <nomepacchetto>

``fuss-server`` e ``fuss-client``
---------------------------------

Per la diagnosi è importante registrare tutti i messaggi forniti da
ansible durante l'installazione, pertanto si riporti tutto quanto
stampato dal comando fin dall'inizio.

Se si è in console (fisica o di una macchina virtuale) si può salvare
tutto l'output del comando usando preventivamente il comando script, ad
esempio::

   script
   <comando>
   ...
   exit # da dare quando il comando finisce

dove ``<comando>`` sarà di volta in volta ``fuss-server create``,
``fuss-client -a`` o le loro varie varianti; questo salva tutto l'output
sul file ``typescript`` nella directory corrente.

