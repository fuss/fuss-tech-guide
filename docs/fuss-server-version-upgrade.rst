.. _fuss-server-version-upgrade:

*****************************
Aggiornamento del server FUSS
*****************************

Di seguito viene mostrata passo-passo la procedura di aggiornamento di un server FUSS dalla versione 10 alla 12.

- Riferimenti sulla FUSS Tech Guide:

   -  https://fuss-tech-guide.readthedocs.io/it/latest/quick-install.html#installazione-di-fuss-server-dal-template-cloud-init-ready
   -  https://fuss-tech-guide.readthedocs.io/it/latest/restore.html#operazioni-di-dump-sul-server-originario-fuss-10
   -  https://fuss-tech-guide.readthedocs.io/it/latest/gestione-dei-fuss-client.html#rimozione-completa-della-configurazione

- Aggiornare il server fuss e lanciare fuss-server upgrade:

   ::

      apt update
      apt dist-upgrade
      fuss-server upgrade

- Fare il dump della macchina Proxmox.

- Se la scuola è dotata di rete WiFi con Captive Portal, scollegare la
  scheda di rete o impostare una password al SSID dal controller. Dal
  momento che dalla versione 12 l'autenticazione è gestita dagli AP,
  se la rete viene lasciata aperta gli utenti possono navigare non
  autenticati durante la procedura di upgrade.

- Per fare il backup della configurazione di Proxmox, la comunità
  Proxmox consiglia gli script che si trovano al link
  https://github.com/DerDanilo/proxmox-stuff

-  Fare il fuss-backup del vecchio server FUSS;

   -  il fuss-backup include anche il backup fatto da ``fuss-dump.sh`` dove ci sono anche

      -  ``fuss-backup.conf`` con relativo cron file
      -  ``/opt``

-  Copiare il file ``/var/backups/fuss-server-dump-yyyy-mm-dd.tgz`` nella
   cartella root di Proxmox:

   ::

      scp /var/backups/fuss-server-dump-yyyy-mm-dd.tgz root@IP-PROXMOX-SERVER:.

      
   .. note::   
     Per ricavare il fingerprint RSA/ECDSA/ED25519 di un server si può usare il seguente comando:

     ::

        ssh-keygen -lf <(ssh-keyscan $HOSTNAME 2>/dev/null)

-  Seguire la guida
   https://fuss-tech-guide.readthedocs.io/it/latest/quick-install.html#installazione-di-fuss-server-dal-template-cloud-init-ready

-  Connettersi al server Proxmox:

   ::

      ssh root@IP-PROXMOX-SERVER

-  Copiare l’immagine cloudinit presa da http://iso.fuss.bz.it in
   ``/var/lib/vz/dump``:

   ::

      cd /var/lib/vz/dump
      wget http://iso.fuss.bz.it/cloud-init/vzdump-qemu-fuss-server-12-latest.vma.zst

-  Spegnere la vecchia VM del server FUSS.

-  Effettuare il restore dell'immagine cloud-init (da local) in Proxmox

   .. figure:: images/fuss-server-version-upgrade-fig01.png
      :alt: Restore cloud-init image

-  Creare disco cloud-init: ``Hardware --> Add``:

   .. figure:: images/fuss-server-version-upgrade-fig02.png
      :alt: cloud-init drive

-  Parametri cloud-init: fare copy-paste dei parametri dalla VM del vecchio server FUSS.

-  Avviare la nuova VM.

-  Collegarsi via ssh:

   ::

      ssh root@IP-FUSS-SERVER

   .. note::
      La password di root della nuova immagine cloudinit è ``fuss``; va cambiata subito.

-  Aggiornare i pacchetti:

   ::

      apt update && apt dist-upgrade

- Copiare da Proxmox il file ``fuss-server-dump-yyyy-mm-dd.tgz`` e
  scompattarlo in ``/root`` per poter accedere ai parametri di
  ``fuss-server.yaml`` da inserire dopo aver lanciato ``fuss-server
  create``:

   ::

      scp root@IP-PROXMOX-SERVER:fuss-server-dump-yyyy-mm-dd.tgz .
      tar xvzf fuss-server-dump-yyyy-mm-dd.tgz

   ::

      apt install fuss-server

-  Per avere i parametri fuss-server del vecchio server sotto mano:

   ::

      cat fuss-server.yaml.old

-  Lanciare la configurazione del fuss-server:

   ::

      fuss-server create

   -  Può darsi che ``/var/lib/dpkg/info/cups.postinst`` rallenti il task misc di ansible
      (Install package cups, foomatic-db-compressed-ppds by apt). In tal caso stoppare il server cups: 

      ::

         systemctl stop cups

-  Fare il restore della configurazione del vecchio server FUSS:

   ::

      /usr/share/fuss-server/scripts/fuss-restore.sh  fuss-server-dump-yyyy-mm-dd.tgz

-  Lanciare

   ::

      fuss-server upgrade

-  Copiare eventualmente anche la cartella ``/opt`` 

-  .. note::
     Ricordare di ingrandire il disco (se viene usata un'unica partizione per ``/`` e ``/home``) prima di riavviare la VM andando in
     ``Hardware –> Disk Action –> Resize``:

   .. figure:: images/fuss-server-version-upgrade-fig03.png
      :alt: Resize drive

-  Montare i backup FUSS sul NAS e copiare le home degli utenti:

   ::

      fuss-backup mount
      cd /mnt/recover
      cd PATH-ULTIMO-BACKUP/home
      cp -a * /home

- Se invece le ``/home`` sono su di un disco separato rispetto alla
  root ``/``, potete riassegnare quel disco alla nuova VM sempre
  usando ``Disk Actions`` e ricordandovi di andare a riprendere dal
  backup la riga per il mount della ``/home`` dal file ``/etc/fstab``.

- Riavviare server

- Se la scuola è dotata di rete WiFi, configurare la nuova
  infrastruttura basata su WPA Enterprise. Ricordarsi che con la nuova
  configurazione l’IP deve essere già assegnato alla scheda di rete
  quando si lancia ``fuss-server cp``. Si veda
  https://fuss-tech-guide.readthedocs.io/it/latest/quick-install.html#rete-wifi-wpa-enterprise
  
-  .. note::
     Non dimenticare di mantenere aggiornata la macchina Proxmox
     assicurandosi sempre di avere il dump delle VM. Portarla **almeno
     alla versione 7** di Proxmox VE. Seguite le semplici istruzioni sul
     wiki di Proxmox:

     -  da 5 a 6: https://pve.proxmox.com/wiki/Upgrade_from_5.x_to_6.0
     -  da 6 a 7: https://pve.proxmox.com/wiki/Upgrade_from_6.x_to_7.0
     -  da 7 a 8: https://pve.proxmox.com/wiki/Upgrade_from_7_to_8

- Dopo aver portato anche i client a FUSS 12, ricordare di resettare
  le impostazioni di Xfce4 agli utenti usando lo script
  ``/usr/share/fuss-server/scripts/home-cleanup``.  Editare lo script
  aggiungendo alla variabile ``CLEAN_LIST`` il path

  ::

     .config/xfce4/

  Eseguire lo script una volta e rimuovere poi il path aggiunto al
  file ``home-cleanup``.
