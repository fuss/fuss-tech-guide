.. _`installazioni-scuole-bolzano`:

Installazioni nelle scuole della Provincia Autonoma di Bolzano
==============================================================

La procedura standard di installazione nelle scuole della Provincia
Autonoma di Bolzano prevede
l':ref:`installazione-proxmox-debian`, seguita dalla
:ref:`installazione-fuss-server-proxmox` seguendo le accortezze
specifiche di questa sezione.

Installazione di Debian
-----------------------

Durante l'installazione ricordarsi di:

* annotarsi il nome delle schede di rete, specialmente WAN;
* dare un nome host alla macchina seguendo gli standard stabiliti (ad
  esempio ``galilei-prox``);
* seguire gli standard anche per scegliere il nome di dominio (ad
  esempio ``galilei.blz``);
* scegliere un nome utente locale (ad esempio ``local-fuss``, con
  password ``local-fuss``);
* inserire i server DNS 208.67.222.222 e 208.67.220.220;
* eliminare eventuali partizioni pre-esistenti.

Per il partizionamento dei dischi si possono presentare due casi: se il
RAID nativo del server non viene riconosciuto (come avviene ad esempio
con Server Fujitsu TX 1320 M1 o 2 e HP Proliant Microserver Gen8) è
necessario seguire la procedura per l':ref:`debian-raid-software`; in
caso contrario si può procedere direttamente con la guida
all':ref:`installazione-proxmox-debian`, usando il seguente schema di
partizionamento:

+----------+-------------+---------------------+---------------------+
| mount    | dimensioni  | tipo partizione     | filesystem          |
+==========+=============+=====================+=====================+
| /boot    | 1 GB        | Partizione *fisica* | ext4 con journaling |
+----------+-------------+---------------------+---------------------+
| / (root) | 50 GB (max) | Volume logico LVM   | ext4 con journaling |
+----------+-------------+---------------------+---------------------+
| swap     | 4 GB        | Volume logico LVM   | swap                |
+----------+-------------+---------------------+---------------------+
| /var     | 50+ GB      | Volume logico LVM   | ext4 con journaling |
+----------+-------------+---------------------+---------------------+

Configurazione aggiuntiva di Debian
-----------------------------------

Una volta completata l'installazione e la configurazione di rete è un
buon momento per installare alcuni programmi aggiuntivi utili.

Se si vuole installare il pacchetto dei microcode per i processori intel
è necessario abilitare i repository non-free-firmware in
``/etc/apt/sources.list``::

   deb http://ftp.it.debian.org/debian/ bookworm main contrib non-free-firmware
   deb-src http://ftp.it.debian.org/debian/ bookworm main contrib non-free-firmware

   deb http://security.debian.org/debian-security bookworm/updates main contrib non-free-firmware
   deb-src http://security.debian.org/debian-security bookworm/updates main contrib non-free-firmware

   # bookworm-updates, previously known as 'volatile'
   deb http://ftp.it.debian.org/debian/ bookworm-updates main contrib non-free-firmware
   deb-src http://ftp.it.debian.org/debian/ bookworm-updates main contrib non-free-firmware

Quindi installare i programmi desiderati::

   # apt update
   # apt install vim gedit net-tools dnsutils molly-guard iperf screen etherwake
   # apt install intel-microcode

.. note::

   Il pacchetto intel-microcode è ad esempio necessario nel caso in cui
   all'avvio di Proxmox comparisse l'errore ``TSC_DEADLINE disabled due
   to Errata…``

   Per ulteriori informazioni si veda il `thread relativo sul forum di
   proxmox
   <https://forum.proxmox.com/threads/firmware-bug-tsc_deadline-disabled-due-to-errata-please-update-microcode-to-version-0x25-or-la.37681/>`_

Installazione Proxmox
---------------------

Problemi con i server DELL PowerEdge T630
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Se si presentano problemi all'avvio con il kernel proxmox installato
sopra (cosa che può succedere ad esempio con kernel 4.15.18.1) riavviare
il sistema e, nella schermata di GRUB, selezionare la modalità
``Advanced`` / ``Avanzato`` per poi partire con un kernel più vecchio.

A questo punto si può cercare quale sia il kernel corrispondente alla
ISO proxmox più recente, su https://www.proxmox.com/en/downloads seguire
il link alle informazioni sull'iso e quindi quello alle release notes.

*istruzioni da completare dopo aggiornamento allo stato attuale del
supporto per il server in questione*

Problemi con i server HP
^^^^^^^^^^^^^^^^^^^^^^^^

Con alcuni server HP ed alcuni controller RAID (HWRaid HP P222 P222i P420 o
P420i) si manifestano delle corruzioni dei dati quando viene attivata la
IOMMU, cosa che avviene di default per i kernel successivi alla versione
5.15-23. Si può controllare che controller sono presenti con il comando::

  lspci | grep -i raid

sono indicati come possibilmente problematici quelli citati e lo Smart Array
Gen8.

Per rimediare al problema si deve disabilitare la funzionalità passando come
opzione di avvio del kernel ``intel_iommu=off``; per questo occorre modificare
``/etc/default/grub``, aggiungendo la suddetta opzione alla definizione della
variabile ``GRUB_CMDLINE_LINUX_DEFAULT`` con::

  GRUB_CMDLINE_LINUX_DEFAULT="quiet intel_iommu=off"

ed applicare le modifiche eseguendo ``update-grub``.


Problemi a raggiungere l'interfaccia di Proxmox
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Nel caso in cui l'interfaccia web di proxmox non risponda, controllare
in ``/var/log/daemon.log`` se compare un errore come::

   Apr 13 16:18:24 prox1 pveproxy[3535]: /etc/pve/local/pve-ssl.key: failed to load local private key (key_file or key) at /usr/share/perl5/PVE/APIServer/AnyEvent.pm line 1642.

Nel caso, per correggere usare i seguenti comandi::

   # systemctl restart pve-cluster
   # pvecm updatecerts

seguito da un reboot della macchina

Aggiornamento di Proxmox
^^^^^^^^^^^^^^^^^^^^^^^^

Nel caso in cui sia necessario effettuare l'aggiornamento di Proxmox ad
una versione successiva, si possono seguire le guide del progetto,
scegliendo quella adeguata al proprio caso in
https://pve.proxmox.com/wiki/Category:Upgrade

Installazione del fuss-server
-----------------------------

Quando si effettua l'installazione del fuss-server, l'obiettivo è la
creazione di una VM che possa essere trasportata in un altro host
Proxmox, e quindi essere compatibile con le caratteristiche hardware del
server più piccolo presente nell'intero parco macchine.

Schema di partizonamento
^^^^^^^^^^^^^^^^^^^^^^^^

Lo schema di partizionamento consigliato è:

+-------+------------+-------------------------------------------------+
| swap  | max 8 GB   | stessa quantità della RAM                       |
+-------+------------+-------------------------------------------------+
| /     | 50 GB      |                                                 |
+-------+------------+-------------------------------------------------+
| /var  | min 100 GB |                                                 |
+-------+------------+-------------------------------------------------+
| /srv  | min 100 GB | Ospita il software e le immagini di Clonezilla  |
|       |            | Calibrare la dimensione in base al numero e     |
|       |            | dimensione delle immagini che si vogliono usare |
+-------+------------+-------------------------------------------------+
| /home | rimanente  |                                                 |
+-------+------------+-------------------------------------------------+

Ricordando di usare LVM e che allargare le partizioni è più semplice che
non restringerle, per cui se si lascia dello spazio libero è meglio
sottostimare le dimensioni necessarie ed eventualmente allargarle in
futuro.

Configurazione delle interfacce di rete
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Un esempio di ``/etc/network/interfaces`` adatto al fuss-server è il
seguente::

   # The loopback network interface
   auto lo
   iface lo inet loopback

   # The primary network interface -- WAN
   auto eth0
   iface eth0 inet static
           address  192.168.94.3
           netmask 255.255.255.0
           network 192.168.94.0
           broadcast 192.168.94.255
           gateway 192.168.94.1

   # The secondary network interface - LAN
   auto eth1
   iface eth1 inet static
           address 192.168.0.1
           netmask 255.255.255.0
           network 192.168.0.0
           broadcast 192.168.0.255

In caso si voglia usare un captive portal sarà necessario configurare
una terza interfaccia di rete.

Salvataggio e ripristino di immagini
------------------------------------

Prima di configurare il fuss-server con ``fuss-server create`` si può
creare un dump completo della VM, trasportabile su altri host Proxmox.

In questo modo per creare il FUSS server di altre scuole si può partire
da una macchina quasi ultimata sulla quale si possono sistemare le
caratteristiche specifiche per rete/utenza e completare la
configurazione.

Dump
^^^^

.. note::

   Il device su cui si fa il dump non deve contenere dump preesistenti
   di una VM con lo stesso ID di quella su cui stiamo operando.

   Se così fosse, si possono raggruppare le directory relative al dump
   preesistente (``dump``, ``images``, ``private``, ``templates``) in
   una nuova directory, ad esempio ``old_dumps``.

1. Montare uno storage esterno

   Collegare l'HD esterno al server (possibilmente ad una porta USB 3) e
   assicurarsi che Proxmox monti il device (basta cliccarlo in *Gestione
   dei File*).

   Verificarne il punto di mount, che sarà tipo
   ``/media/<nome_utente_loggato>/<etichetta_hd_esterno>``.

2. Scollegare la ISO di fuss-server dal DVD della macchina virtuale

   Cliccare sulla VM, su Hardaware, doppio click su CD/DVD Drive e
   scegliere "Do not use any media"; confermare.

3. Aggiungere il disco esterno agli storage disponibili.

   * Cliccare su Datacenter, poi su Storage e poi su Add.
   * Scegliere Directory
   * Dare un nome (ID:) allo storage, ad esempio ``BACKUP SERVER``.
   * Inserire nel campo Directory il punto di mount del disco esterno.

4. Effetuare il dump.

   * Assicurarsi che la macchina virtuale sia spenta.
   * Espandere il menù a tendina Content, cliccare su VZDump backup file
     e poi sul tasto azzurro Add.
   * Cliccare sulla VM, su Backup e poi su Backup now.
   * Nel menù Storage selezionare quello appena aggiunto.
   * Nel menù Mode selezionare snapshot.
   * Lasciare selezionato il tipo di compressione LZO (fast).
   * Cliccare sul tasto azzurro Backup.

   Il processo creerà un file denominato ``vzdump-quemu..........lzo``
   nella directory ``dump`` del disco esterno.

5. Scollegare il disco esterno.

   * Cliccare su Datacenter, Storage, sulla riga corrispondente al disco
     esterno, quindi su Remove e confermare con Yes.
   * Smontare il disco dall'host Proxmox.

Restore
^^^^^^^

Nel caso si abbia già un dump di FUSS Server, quando si è arrivato alla
fine dell':ref:`installazione-proxmox-debian` lo si può ripristinare.

.. warning::

   L'host di destinazione dovrà essere configurato con uno schema di
   bridge identico a quello dell'host sul quale è stato creato lo
   snapshot.

* Sul server di destinazione, copiare il file ``.lzo`` del dump nella
  directory ``/var/lib/vz/dump/``.

* Da terminale eseguire l'operazione di restore::

     # cd /var/lib/vz/dump/
     # qmrestore <file_del_dump>.lzo <id_macchina>

  dove ``<id_macchina>`` (ad esempio ``100``) è diverso da eventuali
  altre macchine presenti sul server proxmox.

A questo punto è necessario adattare la macchina virtuale alla nuova
scuola.

* in Options cambiare il nome della macchina virtuale.
* In Hardware eliminare e ricreare le Network Interfaces.
* Adattare eventualmente la quatità di RAM e il numero di CPU.

Quindi, al primo boot della macchina virtuale, cambiare i seguenti
parametri:

* password di root
* hostname della macchina in ``/etc/hosts`` ed ``/etc/hostname``
* indirizzi di rete in ``/etc/network/interfaces``.
