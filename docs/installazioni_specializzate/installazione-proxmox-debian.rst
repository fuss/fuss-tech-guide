.. _`installazione-proxmox-debian`:

Installazione di Proxmox su Debian
==================================

Questa procedura si applicava alle macchine per le quali non viene
riconosciuto il controller RAID (ad esempio i server Fujitsu PRIMERGY
TX1320 M1). Dato che l'installer di Proxmox non supporta il RAID
software occorrerà usare quello di Debian, e poi passare a Proxmox una
volta installata Debian. Per gli altri casi si applica la procedura
illustrata nella sezione :ref:`installazione-diretta-proxmox`.

Viene descritta nel dettaglio l'installazione della versione 6.x di
proxmox basata su Debian Buster; sulle macchine nuove non dovrebbe più
servire, ma nel caso può essere utile consultare anche
https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_11_Bullseye
per verificare eventuali aggiustamenti per la versione attuale.

Installazione di base
---------------------

Si inizi con l'installazione di una Debian Buster da CD, DVD o chiavetta USB,
per la scelta dell'immagine si hanno due possibilità:

* Scaricare e installare ``debian-live-<versione>-amd64-xfce.iso`` da
  https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/

* Scaricare e installare la netinstall
  ``debian-<versione>-amd64-netinst.iso`` o il CD ordinario di
  installazione ``debian-<versione>-amd64-xfce-CD-1.iso`` da
  https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/

L'installazione è quella ordinaria di un sistema base Debian, ma ci sono delle
accortezze da seguire per quanto riguarda la configurazione della rete ed il
partizionamento dei dischi.

La rete deve essere configurata con IP statico (dovrà comunque essere
riconfigurata in seguito da dentro Proxmox). Se si dispone di un solo IP
pubblico e si è dietro un NAT (si presuppone che l'accesso ad internet in tal
caso sia gestito da un router) l'IP della rete interna su cui vengono
reindirizzati i pacchetti provenienti dall'esterno deve essere lasciato al
fuss-server (se si vuole la raggiungibilità dello stesso da remoto). Si usi
pertanto un altro IP nella rete interna usata dal router.

Se per esempio il router ha IP interno ``192.168.112.1`` ed usa la rete
``192.168.112.0/24`` e redirige tutto il traffico dall'esterno sull'IP
``192.168.112.2``, quest'ultimo dovrà essere usato sulla macchina virtuale del
Fuss Server, e non si potrà usare per l'installazione. Pertanto, assumendo che
si sia messo sullo stesso tratto di rete la prima interfaccia del server (si
assume sia ``eth0``) nell'installazione si potrà usare un qualunque altro IP
libero della rete, ad esempio ``192.168.112.102``.

Per il partizionamento dei dischi occorre installare sul RAID software
usando LVM, per potersi trovare nella stessa situazione in cui ci si
troverebbe con l'installazione di Proxmox su un RAID hardware. Questa
parte della procedura è descritta in dettaglio nella sezione
:ref:`debian-raid-software` a cui si rimanda.

La procedura prevede l'installazione con una ``/boot`` separata (installata
direttamente sul primo RAID1, posto su ``/dev/md0``) con tutto il resto su un
volume LVM (installato sul secondo RAID1, posto su ``/dev/md1``). Le
dimensioni delle varie partizioni e volumi logici pertanto sono le seguenti,
illustrate anche nella pagina citata:

* un primo disco RAID1 per ``/boot`` dell'ordine di 1Gb
* un secondo disco RAID1 come volume fisico di LVM con tutto il resto dello
  spazio disco
* un volume logico di swap dell'ordine di un quarto della RAM (ma non superare
  i 4G)
* un volume logico per ``/root`` dell'ordine di 30/50G a seconda che si
  preveda o meno di installare anche l'interfaccia grafica.
* nessun altro volume logico in fase di installazione (verranno creati dopo)

Eventuali correzioni all'installazione iniziale
-----------------------------------------------

Con una installazione ordinaria si dovrebbe ottenere una configurazione già
funzionante, e potete passare direttamente alla sezione `Preparazione
all'installazione di Proxmox`_, ma alcuni possibili interventi di correzione
post-installazione di Debian sono i seguenti:

1. Cambiare le password di root e dell'utente locale creato durante
   l'installazione (es.local-fuss)::

     passwd root
     passwd local-fuss

#. Installare i firmware aggiuntivi, solo se necessario; copiare i firmware
   (``*.deb``) nella cartella ``/opt`` ed installarli con::

     cd /opt
     dpkg -i *.deb

#. Modificare il file di configurazione delle schede di rete, questo passo è
   da eseguire solo se la configurazione della rete eseguita in fase di
   installazione all'interno dell'installer di Debian non ha funzionato
   correttamente, per modificarlo si apra il file ``/etc/network/interfaces``
   con un editor qualunque, ad esempio eseguendo ``vim
   /etc/network/interfaces``; un esempio del file è il seguente::

     # This file describes the network interfaces available on your system
     # and how to activate them. For more information, see interfaces(5).

     # The loopback network interface
     auto lo
     iface lo inet loopback

     # The primary network interface -- WAN
     auto eth0
     iface eth0 inet static
         address 192.168.112.102
         netmask 255.255.255.0
         network 192.168.112.0
         broadcast 192.168.112.255
         gateway 192.168.112.1
         # dns-* options are implemented by the resolvconf package, if installed
         # dns-nameservers 208.67.222.222 208.67.220.220 8.8.8.8

   verificare che il nome della scheda di rete (``eth0`` nell'esempio)
   corrisponda alla scheda di rete rilevata sulla macchina dal comando
   ``ip a``.

#. se si è modificato il file di configurazione delle interfacce si esegua il
   restart dei servizi di rete con::

     service networking restart

#. Modifiche del file dei repository (``/etc/apt/sources.list``); questo passo
   è da eseguire solo se occorre inserire dei repository aggiuntivi o fare
   correzioni rispetto a quelli installati di default dall'installer di Debian,
   si apra il file con ``vim /etc/apt/sources.list`` un esempio del file è::

     deb http://security.debian.org/ buster/updates main
     deb-src http://security.debian.org/ buster/updates main
     #
     deb http://deb.debian.org/debian/ buster-updates main
     deb-src http://deb.debian.org/debian/ buster-updates main
     #
     deb http://deb.debian.org/debian buster main
     deb-src http://deb.debian.org/debian buster main

#. Modifiche alla impostazione dei DNS; questo passo è da eseguire solo se la
   configurazione della rete eseguita in fase di installazione all'interno
   dell'installer di Debian non ha funzionato correttamente, si apra il file
   con ``vim /etc/resolv.conf``, un possibile esempio (usando il server
   di Cloudfare) è::

     # Generated by NetworkManager
     nameserver 1.1.1.1

   e se ne può verificare subito il funzionamento ad esempio tramite ping ad
   un server noto::

     $ ping www.linux.it


Preparazione all'installazione di Proxmox
-----------------------------------------

Una volta completata l'installazione base di Debian come descritto nella
sezione `Installazione di base`_. ci sono una serie di passi preliminari da
effettuare prima di passare ad installare la piattaforma di Proxmox.

Rimuovere i pacchetti non necessari e/o dannosi
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

I seguenti due pacchetti devono essere rimossi con i comandi::

   apt-get --purge remove network-manager
   apt-get --purge remove os-prober

L'uso di ``network-manager`` interferisce con la configurazione delle
interfacce, deve essere sempre rimosso, e si verifichi che non venga
eventualmente reinstallato come dipendenza nel caso si installi una
interfaccia grafica.

Il pacchetto ``os-prober`` è inutile e potenzialmente pericoloso per qualunque
server, perché cerca di controllare il contenuto di tutti i dischi per
identificare quali sono i sistemi operativi da far partire in fase di boot,
compresi eventuali dischi di macchine virtuali, magari attivi o montati, per
cui può causare anche corruzioni pesanti dei filesystem.

Configurare la risoluzione del nome locale
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Prima di passare all'installazione della piattaforma occorre assicurarsi che
l'indirizzo IP assegnato alla macchina sia risolto correttamente nell'hostname
della stessa, altrimenti il servizio di ``pve-manager`` non si avvierà,
bloccando l'installazione. Per questo occorre modificare il file
``/etc/hosts`` installato di default da Debian con:

* Nella seconda riga sostituire l'IP ``127.0.1.1`` con l'IP assegnato al
  server, nel nostro caso ``192.168.112.102``
* Nella seconda riga aggiungere alla fine il nome aggiuntivo ``pvelocalhost``

un esempio del file, per la macchina ``server102`` nel dominio ``dom102.bzn``
è il seguente::

   127.0.0.1       localhost.localdomain localhost
   192.168.112.102        server102.dom102.bzn    server102 pvelocalhost

   # The following lines are desirable for IPv6 capable hosts
   ::1     localhost ip6-localhost ip6-loopback
   ff02::1 ip6-allnodes
   ff02::2 ip6-allrouters

Una volta fatto la modifica va verificata la risoluzione del nome locale; per
la verifica della risoluzione diretta occorre eseguire il comando::

   getent hosts IP.DEL.MIO.SERVER

ad esempio con la configurazione precedente si deve avere::

   # getent hosts 192.168.112.102
   192.168.112.102 server102.dom102.bzn server102 pvelocalhost

per la verifica della risoluzione inversa eseguire il comando::

   hostname --ip-address

e con la configurazione precedente si deve avere::

   # hostname --ip-address
   192.168.112.102

Installare la piattaforma di Proxmox
------------------------------------

Completate le configurazioni preliminari della sezione `Preparazione
all'installazione di Proxmox`_ si può passare all'installazione.

Aggiungere i repository di Proxmox
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il primo passo è aggiungere il repository per l'installazione eseguendo il
comando::

   echo "deb [signed-by=/usr/share/keyrings/proxmox-ve-release-6.x.gpg] http://download.proxmox.com/debian buster pve-no-subscription" \
      > /etc/apt/sources.list.d/pve-install-repo.list

occorre poi aggiungere la chiave che firma i pacchetti del repository
eseguendo il comando::

   wget -O "/usr/share/keyrings/proxmox-ve-release-6.x.gpg" \
       "http://download.proxmox.com/debian/proxmox-ve-release-6.x.gpg"

Installazione della piattaforma
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Una volta aggiornati i dati del repository il primo passo è installare i
pacchetti aggiornati forniti da Proxmox::

   apt update && apt dist-upgrade

occorre aspettare che il download e l'installazione si completino, ci possono
volere anche 15 minuti.

Installare i pacchetti di Proxmox
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Una volta aggiornati i pacchetti presenti si può passare all'installazione
della piattaforma installando il necessario con il comando::

   apt install proxmox-ve postfix open-iscsi

nella scelta della configurazione di postfix si può rispondere "internet site".

Operazioni di pulizia
^^^^^^^^^^^^^^^^^^^^^

Il kernel Debian non serve più, ma quando si fanno gli aggiornamenti scarica
anche quello, per cui si può rimuovere con (si verifichi quale versione è
effettivamente quella installata)::

   apt remove linux-image-amd64 linux-image-4.9.0-3-amd64

Riavvio sulla piattaforma Proxmox
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Una volta fatti i passi precedenti si può riavviare, per sicurezza si può
comunque eseguire un ``update-grub`` verificando che nella lista dei kernel
compaia quello di Proxmox (che ha il suffisso ``pve``). Nel caso si sia
installato Debian su RAID1 software, si verifichi che ``grub`` sia installato
nel su entrambi i dischi, si può verificare la cosa (ed eventualmente
correggere l'installazione) eseguendo ``dpkg-reconfigure grub-pc``
selezionando i dispositivi dei due dischi.

Una volta riavviato il server ci si potrà collegare all'interfaccia web di
gestione con l'indirizzo:

https://IP.DEL.MIO.SERVER:8006

si otterrà una finestra di accesso come la seguente:

.. figure:: /installazioni_specializzate/images/P-001.png

deve essere selezionato come "Realm", come in figura, "Linux PAM standard
authentication" e ci si potrà collegare usando le credenziali dell'utente
``root`` (inserendo ``root`` come username e la relativa password nel campo
"Password").

Apparirà la seguente finestra di assenza di sottoscrizione, a cui si può dare
OK senza nessun problema.

.. figure:: /installazioni_specializzate/images/P-002.png

Finalizzazione della piattaforma
--------------------------------

Il passaggio da Debian a Proxmox lascia alcune configurazioni da sistemare
rispetto alla installazione diretta di quest'ultima. Questi i passi per
effettuare gli aggiustamenti necessari.

Riconfigurare l'interfaccia di rete principale per l'uso del bridge
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Ci si colleghi dall'interfaccia web e si acceda alla sezione di configurazione
della rete con le seguenti azioni:

* Cliccare sul Nodo (es."server102") nel primo pannello
* Cliccare su "Network" nel secondo pannello

ottenendo la schermata seguente:

.. figure:: /installazioni_specializzate/images/N-01.png

Dalla sezione di configurazione della rete dovremo eliminare la configurazione
di ``eth0``, che verrà poi sostituita da quella di ``vmbr0``, con le seguenti
azioni:

* Doppio click su eth0
* Eliminare tutti i campi contenuti e premere su "OK"

la finestra dovrà risultare come la seguente:

.. figure:: /installazioni_specializzate/images/N-03.png

salvando la configurazione della rete risulterà la seguente:

.. figure:: /installazioni_specializzate/images/N-02.png

A questo punto possiamo passare a creare il bridge ``vmbr0``, si prema sul
pulsante "Create" e dal menu si selezioni "Linux Bridge", verrà proposta una
interfaccia di configurazione in cui occorrerà riempire i campi:

* *IP address*: mettere lo stesso indirizzo che si era usato per eth0
* *Subnet mask*: mettere la stessa che si era usata per eth0
* *Gateway*: mettere lo stesso indirizzo che si era usato per eth0
  (quello del router)
* *Bridge ports*: mettere ``eth0``

un esempio di configurazione è riportato nella immagine seguente:

.. figure:: /installazioni_specializzate/images/N-04.png

per attivare le nuove configurazioni occorre riavviare il server, lo si può
fare direttamente dal pulsante "Restart".

Creare il Logical Volume del pool di dati per lo storage thin-lvm (local-lvm)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il primo passo è verificare con ``vgdisplay`` quanto spazio disco libero è
presente sul volume group di LVM (``vg`` se si sono seguite le istruzioni
precedenti)::

   # vgdisplay
   ...
     Alloc PE / Size       12782 / 49,93 GiB
     Free  PE / Size       18897844278 / 788,13 GiB
   ...

Verificato lo spazio disponibile si crei il volume logico ``data`` per il pool;
è prudente lasciarsi un polmone di spazio disco libero per eventuali
estensioni per cui non prenderemo tutto lo spazio disponibile. Nel caso il
comando::

   lvcreate -L 715G -n data vg

creerà un volume da 715 Gb, a questo punto si potrà marcare il nuovo volume
come pool per il *thin provisioning* con il comando::

   lvconvert --type thin-pool vg/data

.. warning:: ``data`` è il nome del logical volume per il pool, si può usare
             un nome qualunque ma questo è quello usato di default
             dall'installer di Proxmox. Invece ``vg`` è del volume group
             che si è usato negli esempi illustrati nella sezione
             :ref:`debian-raid-software` , nel caso dell'installer di
             Proxmox questo è in genere ``pve``.

Predisposto il volume logico per il thin provisioning dall'interfaccia di
Proxmox (Datacenter->Storage->Add->LVM-Thin) si aggiunga un nuovo storage,
verranno richiesti in una finestra:

* ID (usare local-lvm per coerenza con il default dell'installer di Proxmox)
* Volume group: selezionare dal menù a tendina (o scrivere direttamente quello
  usato, ad esempio ``vg``
* Thin pool: scrivere ``data`` o selezionarlo dal menù a tendina (comparirà
  dopo aver impostato il volume group)
* gli altri campi non van toccati

Aggiustamenti finali
--------------------

Non è necessario per il funzionamento, ma si può installare un Ambiente
Desktop (xfce4) con un Display Manager (lightdm) e Terminali più evoluti di
quelli di serie con il comando::

   apt-get install xfce4 lightdm xfce4-terminal gnome-terminal

Come browser si può installare Firefox con il comando::

   apt-get install firefox-esr
