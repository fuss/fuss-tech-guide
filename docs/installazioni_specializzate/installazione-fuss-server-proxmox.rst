.. _`installazione-fuss-server-proxmox`:

Installazione del fuss-server su Proxmox
========================================

In questa pagina si dettagliano tutti i passi da far per installare il
Fuss Server su Proxmox, che sono indipendenti dal modo con cui è
installato quest’ultimo, che lo si sia fatto con l’installer, o che lo
si sia fatto installando prima Debian e passando poi a Proxmox.

Indicazioni generali sul collegamento all’interfaccia web
---------------------------------------------------------

Tutte le volte che si deve fare una operazione dall’interfaccia di
gestione via web di Proxmox ci si dovrà collegare all’indirizzo:

https://IP.DEL.MIO.SERVER:8006

e si otterrà una finestra di accesso come la seguente:

.. figure:: /installazioni_specializzate/images/P-001.png

deve essere selezionato come “Realm” dal menu “Linux PAM standard
authentication” e ci si potrà collegare usando le credenziali
dell’utente ``root``, inserendo ``root`` nel campo “Username” e la
relativa password nel campo “Password”.

Una volta autenticati apparirà la seguente finestra di avviso per
l’assenza di sottoscrizione, a cui si può dare OK senza nessun problema.

.. figure:: /installazioni_specializzate/images/P-002.png

Configurazioni aggiuntive preliminari
-------------------------------------

Sono configurazioni da effettuare e controllare dopo l’installazione.

Sincronizzazione di data e ora
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Su Proxmox 4.4 la sincronizzazione del tempo viene eseguita da
``systemd-timesyncd``, la cui configurazione è mantenuta in
``/etc/systemd/timesyncd.conf``, che di default è vuoto per cui vengono
utilizzati i server NTP del pool Debian.

Se la macchina ha accesso ad Internet non è necessario nessun intervento
ulteriore, altrimenti occorrerà modificare il file per indicare a quale
server NTP rivolgersi (ad esempio ad un server NTP sulla rete interna,
che potrebbe essere fornito ad esempio dal router).

In tal caso occorrerà inserire nel file suddetto un contenuto del tipo::

   [Time]
   Servers=IP.DEL.SERVER.NTP

Alternativamente si può installare direttamente il servizio NTP sulla
macchina con::

   apt-get install ntp

Di nuovo per una sincronizzazione corretta occorre impostare l’accesso
ad un server NTP di riferimento, per farlo si deve:

-  aprire il file di configurazione di ``ntp`` (``/etc/ntp.conf``) ad
   esempio con ``vim /etc/ntp.conf``
-  commentare le voci server esistenti e aggiungere l’indirizzo IP del
   server che si può raggiungere per sincronizzare data e ora

Un esempio del contenuto che si può modificare è il seguente::

   server 0.debian.pool.ntp.org iburst
   server 1.debian.pool.ntp.org iburst
   server 2.debian.pool.ntp.org iburst
   server 3.debian.pool.ntp.org iburst

Se la sincronizzazione di default con ``systemd-timesyncd`` non sta
funzionando si deve intervenire manualmente. Per verificare che
l’orologio sia sincronizzato (e verificare al contempo che il server NTP
indicato sia raggiungibile) si possono eseguire i seguenti comandi::

   service ntp stop
   ntpdate tempo.ien.it
   # sincronizza orologio al clock dell'hw
   hwclock --systohc
   service ntp start
   date

.. _`configurazione-rete-proxmox-per-fuss-server`:

Creazione dei bridge per le interfacce
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Nell’installazione di Proxmox (o in quella di Debian) viene configurata
soltanto la prima interfaccia di rete. Il Fuss Server ne usa almeno una
seconda per la rete locale (supporremo sia ``eth1``) ed eventualmente
una terza per il captive portal (supporremo sia ``eth2``).
L’installazione le lascia non configurate e assumeremo questo come punto
di partenza.

Per poterle utilizzare dalle macchine virtuali occorre configurare un
corrispondente bridge (sarà rispettivamente ``vmbr1`` e ``vmbr2``. Per
farlo ci si colleghi all’interfaccia web, e si acceda alla sezione di
configurazione della rete con le seguenti azioni:

-  Cliccare sul Nodo (es.“server102”) nel primo pannello
-  Cliccare su “Network” nel secondo pannello

A questo punti si prema sul pulsante “Create” e dal menu si selezioni
“Linux Bridge”, verrà proposta una finestra di configurazione in cui il
solo campo che occorre impostare è “Bridge ports” in cui inserire il
nome della relativa interfaccia (ad esempio per ``vmbr1`` si metterà
``eth1`` per la rete interna, per ``vmbr2`` si metterà ``eth2`` per il
captive portal).

Nel caso della configurazione dell’interfaccia del captive portal non è
opportuno configurare un indirizzo, ma si è rivelata necessaria una ulteriore
configurazione specifica dell'interfaccia ethernet ad esso dedicata, per
evitare rallentamenti in upload che si manifestano con alcune interfacce. Per
questo motivo occorre disabilitare nell'interfaccia di rete le funzionalità di
"generic segmentation offload" e "TCP segmentation offload". Questo si fa
manualmente con il comando::

  ethtool -K eth2 gso off gro off tso off

continuando a supporre che l'interfaccia dedicata al Captive Portal sia la
``eth2``; si noti che la configurazione si applica all'interfaccia, non al
bridge.  Per rendere permanente la configurazione il comando può essere
impostato aggiungendo sotto ``/etc/network/if-up.d/`` uno script ``cp-iface``
che lo esegua, con un contenuto del tipo::

  #!/bin/sh

  # disable GRO, GSO and TSO from captive portal interface,
  # to avoid upload slowdown

  ETHTOOL=/sbin/ethtool

  test -x $ETHTOOL || exit 0

  [ "$IFACE" != "eth2" ] || exit 0

  $ETHTOOL -K eth2 gso off gro off tso off


Per la configurazione dell’interfaccia della rete interna se si vuole
raggiungere la macchina Proxmox dalla stessa occorrerà dare un indirizzo
IP anche al bridge, avendo cura di inserirlo al di fuori del range che
poi si utilizzerà per il DHCP del Fuss Server (ed ovviamente diverso da
quello che assegneremo al Fuss Server stesso). In tal caso i campi da
riempire sono:

-  IP address : mettere l’indirizzo desiderato
-  Subnet mask : mettere la subnet mask che si userà sulla rete interna
-  Gateway : deve essere lasciato vuoto
-  Bridge ports : mettere eth1

Un esempio è riportato in figura, la configurazione si finalizza
premendo “Create”.

.. figure:: /installazioni_specializzate/images/N-05.png

Per applicare la nuova configurazione della rete la macchina deve essere
riavviata (si può farlo direttamente usando il pulsante “Restart”
dell’interfaccia web).


.. _`caricare-iso-su-proxmox`:


Caricare sul server le immagini ISO per l’installazione delle macchine virtuali
-------------------------------------------------------------------------------

Il caricamento può essere fatto attraverso un download diretto sul
server (ad esempio con ``wget``) o con la copia diretta del file nella
directory ``/var/lib/vz/template/iso``. I file in questa directory
devono essere immagini ISO di CD/DVD di installazione.

Nel caso del Fuss Server si deve usare la più aggiornata che si scarica
dal seguente indirizzo:

http://iso2.fuss.bz.it/fuss8/server/

(ad esempio “fuss-server-jessie-amd64-201705221525.iso”)

Qualora si abbia l’immagine sul proprio PC la si potrà caricare
direttamente dall’interfaccia web, sullo storage “local”. Per farlo si
eseguano i seguenti passi:

-  dal menù di sinistra, cliccare su “local (server)”
-  dal menù laterale selezionare “Content”
-  dal menù centrale, cliccare su “Upload”

si otterrà la seguente schermata:

.. figure:: /installazioni_specializzate/images/P-007.png

a questo punto cliccare “Select file” e scegliere la ISO scaricata (si
può anche caricare direttamente da un device esterno) ed una volta
selezionata si otterrà:

.. figure:: /installazioni_specializzate/images/P-008.png

a questo punto si potrà cliccare su “Upload”, a caricamento terminato,
si visualizzerà la ISO caricata come in figura:

.. figure:: /installazioni_specializzate/images/P-009.png

.. _`creare-vm-fuss-server`:

Creazione della macchina virtuale per il Fuss Server
----------------------------------------------------

Procedura di creazione della macchina virtuale
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La creazione si attiva cliccando sul tasto “Create VM” di colore azzurro
posto in alto a sinistra, cosa che farà comparire una finestra come
quella in figura:

.. figure:: /installazioni_specializzate/images/P-010.png

che prevede l’esecuzione di una serie di impostazioni di valori divise
in sezioni, seguendo una successione di schermate navigabili avanti ed
indietro con i pulsanti “Next” e “Back”, ed identificate da una
etichetta in alto.

1. Sezione: “General”, inserire soltanto

  - VM ID: (si può accettare il default, 100)
  - Name: un nome identificativo (ad esempio fuss-server-01; si
    consiglia l'uso dello stesso nome del fuss-server)

  .. figure:: /installazioni_specializzate/images/P-011.png

2. Sezione: “OS”

  -  selezionare “Linux 4.X/3.X/2.6 Kernel”

  .. figure:: /installazioni_specializzate/images/P-012.png

3. Sezione: “CD/DVD”

  -  espandere tendina “ISO image:”
  -  Selezionare la ISO precedentemente caricata (ad esempio:
     “fuss-server-jessie-amd64-201705221525.iso”)

  .. figure:: /installazioni_specializzate/images/P-013.png

4. Sezione: “Hard Disk”

  -  Bus/Device = VirtIO
  -  Storage = local-lvm
  -  Disk size (GB)= dimensione del disco in GB, vedi istruzioni al punto
     4.2
  -  Format = Raw disk image (raw) (NON MODIFICABILE)
  -  Cache = Default (No cache)

  .. figure:: /installazioni_specializzate/images/P-014.png

5. Sezione: “CPU”

  -  Socket: pari al numero di processori della macchina, vedi istruzioni
     al punto 4.2
  -  Cores: 1

  .. figure:: /installazioni_specializzate/images/P-015.png

6. Sezione: “Memory”

  -  Memory (MB) : RAM della macchina, scegliere in base alle
     indicazioni del punto 4.2
  -  Togliere il “flag” alla funzione “Ballooning”

  .. figure:: /installazioni_specializzate/images/P-016.png

7. Sezione: “Network”

  -  Bridge = vmbr0
  -  Model = VirtIO (paravirtualized)

  .. figure:: /installazioni_specializzate/images/P-017.png

Dimensionamento per RAM, CPU e disco
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Per il dimensionamento della memoria gli sviluppatori di Proxmox
suggeriscono di lasciare almeno 1G della RAM totale della macchina per
il sistema, si può allocare il rimanente per le macchine virtuali, per
il fuss-server le esigenze effettive possono variare a seconda del
numero di utenti che lo useranno.

La scelta della quantità di RAM dipende anche dall’eventuale uso della
macchina fisica per ospitare altre macchine virtuali. Una buona scelta
di partenza è impiegare dalla metà ai tre quarti del totale, lasciandosi
un polmone di risorse per aumentare la RAM in un secondo tempo (basterà
modificare il valore dall’interfaccia web e riavviare la macchina).

Per il dimensionamento della CPU si verifichi il numero di processori
della macchina e si lo si indichi come numero di cores (l’uso di socket
e cores è indifferente per le prestazioni, conta il totale prodotto dei
due valori, la possibilità di variarli è a favore di chi ha licenze
software per numero di socket, che non ci interessa).

.. tip::

   Per scoprire il numero di processori della macchina si può usare il
   comando ``top``, premere 1 per visualizzare le statistiche separate
   per CPU e contare le righe relative presenti.

Per il dimensionamento del disco, occorre scegliere una dimensione
totale assegnata alle macchine virtuali compatibile con la dimensione
massima disponibile sul pool di spazio disco del volume logico ``data``.
La situazione corrente si ottiene con il comando ``lvs``; ad esempio::

   # lvs
     LV            VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
     data          pve  twi-aotz-- 514,06g             11,48  5,66
     local         pve  -wi-ao---- 200,00g
     root          pve  -wi-ao----  20,00g
     swap          pve  -wi-ao----   4,00g
     vm-101-disk-1 pve  Vwi-aotz-- 100,00g data        59,04

In questo esempio la dimensione assegnata al pool (logical volume
``data``) è di 514,06 G, ed alla macchina virtuale 101 sono stati
assegnati 100G. Lo spazio è aumentabile a piacere (dall’interfaccia), ma
per evitare alla radice la possibilità di esaurire lo spazio su ``data``
non si superi mai una assegnazione pari al 95% della dimensione del pool
(volume logico ``data``).

.. warning::
    le percentuali ``Data%`` e ``Meta%`` fanno riferimento all’uso
    effettivo, sicuramente inferiore a quello massimo teorico (nel caso
    100/514 ~ 20%). L’uso del pool consente il cosidetto *overcommit*,
    cioè allocare più spazio disco di quanto effettivamente disponibile,
    una situazione da evitare assolutamente. Infatti allocando per le
    macchine virtuali più spazio disco di quello disponibile, se poi
    questo viene esaurito, si ottiene una disastrosa perdita di dati.

Pertanto quando si crea il fuss-server, posto che non si preveda di
creare nessun’altra macchina virtuale in seguito, sarà sufficiente usare
un valore inferiore ai 95% della dimensione (nel caso precedente
514,06g) disponibili su ``data``. Si ricordi che ridurre lo spazio disco
allocato in eccesso all’inizio ad una macchina è sempre più complicato
che non estenderlo quando risulti essere poco. Se nel volume group è
ancora disponibile spazio disco (lo sarà se lo si è lasciato in fase di
installazione tenendosi un “polmone” come consigliato) è comunque
possibile allargare ``data`` con il comando::

   lvextend -L +XXg /dev/pve/data

e poi eventualmente utilizzare lo spazio in più ottenuto.


.. _`avviare-vm-fuss-server`:

Avviare la macchina virtuale
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Una volta create la macchina virtuale, comparirà nel panello a destra,
per avviarla, ed avere al contempo l’accesso alla console, si possono
seguire i seguenti passi:

#. Selezionare la VM, ad esempio “100(fuss-server-01)”

  .. figure:: /installazioni_specializzate/images/P-020.png

#. Selezionare in alto a destra il tasto “>__Console”

  .. figure:: /installazioni_specializzate/images/P-021.png

#. Scegliere “Start”

  .. figure:: /installazioni_specializzate/images/P-022.png


.. _`partizionamento-fuss-server`:


Installazione del Fuss Server
-----------------------------

La gran parte delle opzioni di installazione del fuss-server sono già
preimpostate nell’immagine ISO, la sola scelta significativa da fare in
fase di installazione è il partizionamento del disco (virtuale)
assegnato allo stesso.

.. figure:: /installazioni_specializzate/images/SelezioneDisco.png

Nella scelta predefinita dopo aver selezionato il disco viene proposto
direttamente il suo partizionamento. La scelta più sicura, per evitare
problemi di riempimento della radice, è usare filesystem separati per
``/home``, ``/var``, ``/tmp``. Questo però con il partizionamento
diretto rende meno flessibile la eventuale riallocazione dello spazio
disco.

.. figure:: /installazioni_specializzate/images/SelezionePartizioni.png

Si tenga presente infatti che anche avendo disponibile spazio disco nel
virtualizzatore per poter allargare il disco della macchina virtuale,
l’allargamento avverrebbe sul “fondo” pertanto sarebbe facile
ridimensionare soltanto l’ultima partizione (nel caso la ``/home``, che
pur essendo quella più probabile, non è detto sia davvero quella che ha
bisogno dello spazio disco aggiuntivo).

Per questo si suggerisce, per avere maggiore flessibilità, al costo di
una leggera perdita di prestazioni in I/O, di installare usando LVM.
Questo però significa che una volta eseguita la scelta precedente,
occorrerà “tornare indietro” riselezionando “partizionamento guidato”:

.. figure:: /installazioni_specializzate/images/ScegliPartizGuidato.png

e poi selezionando “guidato, usa l’intero disco e imposta LVM”:

.. figure:: /installazioni_specializzate/images/SceltaGuidato.png

ed a questo punto di dovrà ripetere la scelta del disco e dell’uso dei
filesystem separati, e confermare la configurazione di LVM:

.. figure:: /installazioni_specializzate/images/ConfermaScelta.png

ed infine confermare prima le modifiche del disco:

.. figure:: /installazioni_specializzate/images/SceltaConLVM.png

e poi la formattazione finale:

.. figure:: /installazioni_specializzate/images/SceltaFinale.png

A questo dopo un eventuale allargamento del disco della macchina
virtuale sarà sufficiente allargare la partizione finale (che sarà
``/dev/vda5``, logica) che ospita il volume fisico di LVM, estendere
quest’ultimo con ``pvresize``, ed estendere poi il filesystem che si
preferisce con ``resize2fs`` (se si è usato il default di ext4).

Precauzioni post installazione di Proxmox.
------------------------------------------

Proxmox fornisce anche un suo sistema di gestione dei firewall per
l’host per le macchine virtuali, ma il fuss-server ha già la sua
gestione del firewall interna, pertanto il firewall di Proxmox non deve
essere abilitato, perché andrebbe ad interferire.

Per proteggere il sistema della macchina fisica è sufficiente installare
un semplice script di firewall che limiti gli accessi dalla rete interna
alle porte 22 e 8006. In ogni caso questa non deve essere visibile
direttamente da internet (l’indirizzo IP pubblico va reinoltrato, se
necessario, sul fuss-server), per cui la necessità di un firewall è meno
impellente.

Per le macchine che hanno controller RAID hardware si suggerisce di
installare alcuni tool esterni rispetto a Debian (si verifichino i
problemi di licenza) per monitorare lo stato degli stessi. Un sito che
contiene vari di questi programmi, in particolare per i controller meno
recenti e meno supportati, è http://hwraid.le-vert.net/. Occorrerà
comunque (i programmi citati comunicano via email) definire chi riceverà
le relative notifiche.

Si suggerisce inoltre di installare alcuni pacchetti ausiliari di
controllo ed utilità come::

   apt-get tiger etckeeper debsums molly-guard atop iotop iftop
