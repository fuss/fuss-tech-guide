.. _debian-raid-software:

Installazione di Debian su RAID software
========================================

Preparazione della macchina
---------------------------

A volte è possibile che nell'installazione non venga riconosciuto il
controller RAID del server (o che questo sia in realtà un `fake raid
<https://help.ubuntu.com/community/FakeRaidHowto>`_) e che quindi sia
necessario usare i dischi come singoli senza la configurazione RAID del
BIOS. Questo ad esempio è quanto successo con un server Fujitsu PRIMERGY TX1320
M1 con 2 hard disk da 1 Tb ciascuno in RAID1 (mirroring).

Occorre in questo caso fare in modo tramite il BIOS del controller RAID che
gli HD siano semplicemente nello stato "READY" ed eventualmente nel BIOS del
server (se possibile) disattivare il Controller_RAID.

Occorre pertanto, nel caso del Fujitsu PRIMERGY TX1320 M1, entrare nel BIOS
all'avvio con F2, dopo di che dal BIOS eseguire i seguenti passi:

* Selezionare Advanced
* Selezionare SATA Configuration
* Selezionare SATA Mode [AHCI MODE]
* Premere F4 (Save and Exit) oppure selezionare da menù "Save and Exit"

Partizionamento dei dischi
--------------------------

Una volta avviata la macchina con il CD/DVD/Chiavetta USB di
installazione di Debian Buster (si prenda :ref:`l'ultima versione
disponibile <installazione-fuss-server>`) ed arrivati alla schermata di
"Partizionamento dei dischi" si proceda selezionando "Manuale".

.. figure::  /installazioni_specializzate/images/PartizionamentoManuale.png

Comparirà l'elenco dei dischi (in genere ``sda`` e ``sdb``), e al di sotto di
ciascuno di essi, se sono presenti, quella delle eventuali partizioni.

.. figure::  /installazioni_specializzate/images/SceltaDisco.png

Che le partizioni siano presenti o meno occorrerà comunque posizionarsi sulla
riga che elenca il disco e selezionarlo con invio, cosa che porterà alla
richiesta di creare una nuova tabella delle partizioni, cui occorre rispondere
di si (se vi sono partizioni preesistenti saranno cancellate).

.. figure::  /installazioni_specializzate/images/CreaNuovaTabellaPartizioni.png

A questo punto selezionando la voce "SPAZIO LIBERO" che comparirà sotto al
disco si potrà procedere al partizionamento premendo invio, e scegliendo "Crea
una nuova partizione".

.. figure::  /installazioni_specializzate/images/SelezioneSpazioLibero.png

.. figure::  /installazioni_specializzate/images/NuovaPartizione.png

Verrà poi chiesta la dimensione della stessa. La prima partizione servirà per
installarvi il RAID1 che ospiterà ``/boot`` e le sue dimensioni possono essere
fra i 512 Mb ed 1Gb, per cui inseriremo ad esempio "1 Gb", verranno poi
chiesti: il tipo della partizione e va scelto "Primaria", la posizione della
nuova partizione e va scelto "Inizio", ed infine le modalità di uso.

.. figure::  /installazioni_specializzate/images/SelezionaUsareCome.png

.. figure::  /installazioni_specializzate/images/SceltaVolumeFisicoRaid.png

Qui premendo invio sul valore di default (in genere "File system ext4
con Journaling") occorre selezionare nel menu seguente "Volume fisico
per il RAID", e ottenuta la scelta posizonarsi in fondo alla schermata
su "Impostazione della partizione completata" e premere invio.

.. figure::  /installazioni_specializzate/images/ImpostazioneTerminata.png

La sequenza per la prima partizione pertanto è:

* Posizionarsi sul disco (es: ``sda`` )
* Creare una nuova tabella delle partizioni vuota su questo dispositivo?  <Si>
* Posizionarsi sullo spazio libero
* Creare una nuova partizione
* 1 GB
* Primaria;
* Inizio;
* Usare come:	 Volume fisico per il RAID
* Impostazioni della Partizione completata

.. figure::  /installazioni_specializzate/images/RipetizioneSceltaSpazio.png

L'operazione va ripetuta in maniera analoga per creare una seconda
partizione sullo spazio libero restante, lo si dovrà di nuovo
selezionare sulla riga successiva, e ripetere le stesse operazioni della
volta precedente accettando stavolta la dimensione proposta per la
partizione (e riselezionando la nuova partizione come primaria, rispetto
al default presentato di logica), si avrà pertanto la sequenza:

* Posizionarsi sullo spazio libero
* Creare una nuova partizione
* 999 GB (accettare quanto proposto)
* Primaria;
* Inizio;
* Usare come:	 Volume fisico per il RAID
* Impostazioni della Partizione completata

Una volta completato il partizionamento del primo disco le operazioni
dovranno essere ripetute sul secondo disco (ad esempio sdb), avendo cura
di indicare esattamente le stesse dimensioni: è sufficiente indicare le
stesse per la prima partizione, la seconda con il default prende tutto
il resto del disco e sarà uguale in caso di dischi identici (eventuali
piccole differenze in caso di dischi diversi lasceranno inutilizzata la
parte eccedente di quello più grande).

Configurare il RAID software
----------------------------

Una volta completato il partizionamento, come illustrato nella figura
seguente, si potrà passare alla configurazione del RAID software
selezionando la seconda voce del menu.

.. figure::  /installazioni_specializzate/images/SceltaRaidSoftware.png

Verrà richiesto di scrivere i cambiamenti sui dischi, e poi si potrà passare
alla configurazione del raid scegliendo "Creare un device multidisk", e poi il
tipo di raid, che deve essere RAID1.

.. figure::  /installazioni_specializzate/images/CreaDeviceMultidisk.png

.. figure::  /installazioni_specializzate/images/SceltaRaid1.png

Verranno poi chiesti il numero di device attivi (accettare il default di 2) e
quello dei device di riserva (spare, accettare il default di 0), dopo di che
si potranno selezionare i dispositivi che compongono il RAID scegliendo la
prima partizione di entrambi i dischi:

.. figure::  /installazioni_specializzate/images/SceltaDischi.png

Si avrà pertanto la sequenza:

* Alla richiesta "Scrivere i cambiamenti sui dispositivi di memorizzazione e
  configurare il RAID?" selezionare "Si"
* Selezionare "Creare un device multidisk (MD)"
* Selezionare RAID1 dal menu seguente
* Alla richiesta "Numero device attivi per l'array RAID1:" lasciare il default
  di 2
* Alla richiesta "Numero dei device spare per l'array RAID1:" lasciare il
  default di 0
* Alla richiesta Device attivi per l'array RAID1 selezionare ``/dev/sda1`` e
  ``/dev/sdb1``

Si dovrà poi ripetere la sequenza per creare il secondo RAID1 che sarà usato
per LVM, ripetendo le scelte precedenti e selezionando alla fine le due
partizioni restanti.

.. figure::  /installazioni_specializzate/images/RiselezioneDischi.png

La seconda sequenza sarà pertanto:

* Selezionare "Creare un device multidisk (MD)"
* Selezionare RAID1 dal menu seguente
* Alla richiesta "Numero device attivi per l'array RAID1:" lasciare il default
  di 2
* Alla richiesta "Numero dei device spare per l'array RAID1:" lasciare il
  default di 0
* Alla richiesta Device attivi per l'array RAID1 selezionare ``/dev/sda2`` e
  ``/dev/sdb2``

Una volta fatto questo l'impostazione del RAID1 è completa e la si potrà
terminare selezionando la relativa opzione:

.. figure::  /installazioni_specializzate/images/TerminaRaid.png

Configurare LVM (Logical Volume Manager)
----------------------------------------

Una volta finita la configurazione del RAID dovrà essere configurato LVM per
usare il device ``md1`` su cui abbiamo concentrato la gran parte dello spazio
disco, dalla schermata principale che a questo punto sarà la seguente si dovrà
scegliere "Configurare il Logical Volume Manager" e accettare di mantenere
l'attuale partizionamento:

.. figure::  /installazioni_specializzate/images/SceltaLVM.png

.. figure::  /installazioni_specializzate/images/ConfermaLVM.png

A questo punto di avrà accesso al menu di configurazione di LVM, il primo
passo è creare un gruppo di volumi, scegliendo la relativa opzione, ed
inserire il relativo nome(ad esempio ``vg``):

.. figure::  /installazioni_specializzate/images/LVMCreaVG.png

.. figure::  /installazioni_specializzate/images/LVMNomeVG.png

infine occorrerà scegliere il dipositivo (il volume fisico) su cui appoggiarsi
per i dati, che nel nostro caso è ``/dev/md1`` (si deve selezionare solo
questo):

.. figure::  /installazioni_specializzate/images/LVMSelezioneDispositivo.png

La sequenza di azione è pertanto

* Scegliere dal menu "Creare nuovo gruppo di volumi"
* Impostare il nome del gruppo di volumi (es. vg)
* Selezionare il dispositivo ``/dev/md1``

A questo punto si potrà passare alla creazione dei Volumi Logici, ne
serviranno 2, uno per la swap ed uno per la radice, che chiameremo
rispettivamente ``root`` e ``swap``. Dal menu di configurazione di LVM si
dovrà pertanto scegliere "Creare volume logico":

.. figure::  /installazioni_specializzate/images/LVMCreareLV.png

verrà presentata la scelta di quale Volume Group usare, bloccato sull'unico
disponibile, ``vg``, creato in precedenza, proseguendo si potrà inserire il
nome del volume logico e la relativa dimensione. Le dimensioni consigliate
sono una dimensione pari ad un quarto della RAM (ma senza andare oltre i 4G)
per ``swap``, ed una dimensione fra i 30 ed i 50G per ``root``.

I passi sono pertanto i seguenti, da ripetere due volte, prima per la swap e
poi per radice:

* Scegliere dal menu "Creare volume logico"
* Accettare il default per il gruppo di volumi
* Alla richiesta "Nome del volume logico" inserire il nome (prima swap, poi
  root)
* Inserire le dimensioni scelte (es. prima 4G, poi 50G)

Completate le richieste si avrà la schermata del menù analoga alla seguente
(con un volume fisico, un gruppo di volumi e due volumi logici) da cui si
potrà chiudere la configurazione scegliendo "Termina", o verificare le scelte
scegliendo "Mostra i dettagli di configurazione".

.. figure::  /installazioni_specializzate/images/LVMTermina.png

Completare il partizionamento e la formattazione del disco
----------------------------------------------------------

Terminata la configurazione di LVM si dovrà completare la configurazione dei
dischi indicando come formattare i due volumi logici appena creati, e come
formattare il primo RAID1 (``/dev/md0``) che verrà usato per ``/boot``.

Nel caso della swap si dovrà configurare la stessa selezionando il relativo
volume logico come nell'immagine seguente:

.. figure::  /installazioni_specializzate/images/SceltaImpostaSwap.png

una volta scelto il volume si tornerà al menu che ci richiede come usare lo
stesso, e stavolta occorrerà scegliere "area di swap". Nel caso della swap,
che non deve essere montata, questo è tutto.

L'operazione va ripetuta per formattare il filesystem della radice, di nuovo
va scelto il relativo volume logico:

.. figure::  /installazioni_specializzate/images/SceltaImpostaRoot.png

.. figure::  /installazioni_specializzate/images/SceltaImpostaRootExt4.png

e stavolta nel "Come usare" occorrerà selezionare "File system ext4 con
journaling", a questo punto ci verrà presentata anche la scelta del "Punto di
mount" e si dovrà scegliere la radice:

.. figure::  /installazioni_specializzate/images/SceltaImpostaRootMP.png

.. figure::  /installazioni_specializzate/images/SceltaImpostaRootRadice.png

La stessa operazione deve essere ripetuta selezionando il dispositivo "RAID1
dispositivo n° 0" (vedi immagine seguente) e poi scegliendo ancora di usare
come "File system ext4 con journaling" ma selezionando ``/boot`` nel "Punto di
mount".

.. figure::  /installazioni_specializzate/images/SelezioneRAIDboot.png

Riepilogando i passi sono:

* Selezionare il volume logico LV swap
* In "Usare come:" scegliere "area di swap"
* Concludere su "Impostazioni della partizione completata"
* Selezionare il volume logico LV root
* In "Usare come:" scegliere "File system ext4 con journaling"
* In "Punto di Mount:" scegliere "/ - il file system root"
* Concludere su "Impostazioni della partizione completata"
* Selezionare il RAID1 dispositivo n° 0
* In "Usare come:" scegliere "File system ext4 con journaling"
* In "Punto di Mount:" scegliere "/boot - file del boot loader"
* Concludere su "Impostazioni della partizione completata"

Una volta completati questi passi si dovrebbe avere una configurazione analoga
a quella illustrata nella immagine seguente:

.. figure::  /installazioni_specializzate/images/PartizionamentoFinale.png

e si potrà proseguire scegliendo "Terminare il partizionamento e scrivere le
modifiche sul disco" e confermando sulla successiva schermata:

.. figure::  /installazioni_specializzate/images/ConfermaFinale.png

A questo punto si potrà proseguire con il resto dell'installazione.

Finalizzazioni
--------------

La schermata finale richiede l'installazione di Grub, si scelga di installare
sul primo disco, come illustrato nella figura seguente:

.. figure::  /installazioni_specializzate/images/ConfigGrub.png

Terminata la procedura e l'installazione del server, si deve riavviare senza
CD per verificare la ripartenza della macchina. A questo punto occorre poi
assicurarsi che Grub sia installato su tutte e due i dischi, cosa da fare con
i comandi::

   grub-install /dev/sda
   grub-install /dev/sdb

(è importante che Grub sia installato su entrambi i dischi, altrimenti in caso
di rottura del primo la macchina non ripartirà).

Da terminale si può controllare lo stato del RAID con il comando seguente, che
se eseguito poco dopo l'installazione mostrerà il progressivo della
sincronizzazione dei dischi::

   local-utente@appiano:~$ cat /proc/mdstat
   Personalities : [raid1]
   md1 : active raid1 sda2[0] sdb2[1]
         976130880 blocks super 1.2 [2/2] [UU]
         [==========>..........]  resync = 53.3% (520999104/976130880) finish=140.1min speed=54122K/sec

   md0 : active raid1 sda1[0] sdb1[1]
         498368 blocks super 1.2 [2/2] [UU]

una volta completata la sincronizzazione si avrà qualcosa del tipo::

   local-utente@appiano:~$ cat /proc/mdstat
   Personalities : [raid1]
   md1 : active raid1 sda2[0] sdb2[1]
         976130880 blocks super 1.2 [2/2] [UU]

   md0 : active raid1 sda1[0] sdb1[1]
        498368 blocks super 1.2 [2/2] [UU]
