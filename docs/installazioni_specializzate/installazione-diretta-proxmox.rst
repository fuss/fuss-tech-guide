.. _`installazione-diretta-proxmox`:

Installazione diretta di Proxmox
================================

Per i server dove viene riconosciuto il controller RAID si può
installare direttamente la piattaforma Proxmox, si può scaricare una
immagine ISO scrivibile anche su chiavetta. L’immagine è disponibile a
partire dalla pagina:

https://www.proxmox.com/en/downloads/

1. Scelta della ripartizione dello spazio disco sull’installer
--------------------------------------------------------------

In fase di installazione di Proxmox dall’immagine ISO occorre scegliere
la ripartizione dello spazio disco. Questo viene fatto nella schermata
Target Harddisk (la seconda, subito dopo aver accettato la licenza); si
selezionino come valori:

-  **swapsize** 4 (4G swap)
-  **maxroot** 30 (30G di radice, contiene solo il sistema)
-  **maxvz** - (in questo modo tutto il resto del disco sarà inserito
   nel thin-pool di LVM)
-  **minfree** 50 (lasciamo un polmone di 50G)

.. caution::
   con questa scelta lo storage locale (marcato ``local``,
   associato alla directory ``/var/lib/vz``) avrà a disposizione solo lo
   spazio disco della radice, che è sufficiente per tenere le immagini iso
   dei CD per le installazioni, tutto lo spazio non assegnato
   esplicitamente viene lasciato per l’installazione di macchine virtuali.

   Se si vogliono eseguire dei dump delle macchine virtuali sul disco
   locale (i dump potrebbero essere eseguiti su NAS via NFS, ovviamente con
   prestazioni nettamente inferiori), occorre invece aumentare il valore
   del precedente parametro **minfree**, per poter poi creare un ulteriore
   volume logico su cui creare un filesystem da dedicare i dump (le
   dimensioni dovranno corrispondere allo spazio che si prevede di usare
   per le macchine virtali).

2. Configurazione della rete
----------------------------

La configurazione della rete che viene effettuata dall’installer di
Proxomox sulla immagine ISO è comunque statica, anche quando l’indirizzo
IP viene ottenuto da un server DHCP, in tal caso occorre sincerarsi che
l’IP che si va ad usare sia fuori dal range di un eventuale DHCP, si
verifichi per non creare possibili futuri conflitti di IP. E’
preferibile effettuare una impostazione manuale, ma la configurazione si
può comunque cambiare in un secondo momento.

Per la scelta dell’IP vale quanto detto anche al riguardo
dell’installazione di Debian; se si dispone di un solo IP pubblico e si
è dietro un NAT (si presuppone che l’accesso ad internet in tal caso sia
gestito da un router) l’IP della rete interna su cui vengono
reindirizzati i pacchetti provenienti dall’esterno deve essere lasciato
al Fuss Server (se si vuole la raggiungibilità dello stesso da remoto).
Si usi pertanto un altro IP nella rete interna.

Per un esempio si rimanda a quanto illustrato nel punto 1. di
:ref:`installazione-proxmox-debian`

3. Aggiustamento dei repository
-------------------------------

L’installazione diretta di Proxmox configura in
``/etc/apt/sources.list.d/pve-enterprise.list`` il repository
“enterprise” che richiede una sottoscrizione al loro contratto di
supporto. Questa non ci serve pertanto si può cancellare questo file,
oppure commentarne il contenuto (basta apporre un ``#`` all’unica riga
che contiene).

Occorrerà poi aggiungere il repository “community” eseguendo il
comando::

   echo "deb [signed-by=/usr/share/keyrings/proxmox-ve-release-6.x.gpg] http://download.proxmox.com/debian buster pve-no-subscription" \
      > /etc/apt/sources.list.d/pve-install-repo.list

occorre poi aggiungere la chiave che firma i pacchetti del repository
eseguendo il comando::

   wget -O "/usr/share/keyrings/proxmox-ve-release-6.x.gpg" \
       "http://download.proxmox.com/debian/proxmox-ve-release-6.x.gpg"

e, dato che l’immagine dell’installer in genere non contiene gli
aggiornamenti, eseguire i comandi::

   apt-get update
   apt-get dist-upgrade

ed attendere l’installazione degli aggiornamenti; se fra questi vi è
anche il kernel occorrerà riavviare la macchina.
