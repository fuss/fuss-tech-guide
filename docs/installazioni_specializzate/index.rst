***************************
Installazioni specializzate
***************************

Si tratteranno in questa sezione una serie di casi speciali per
l'installazione del Fuss Server, non coperti dall'installazione ordinaria
vista nella sezione :ref:`installazione-fuss-server`

.. toctree::
   :maxdepth: 2

   debian-raid-software
   installazione-proxmox-debian
   installazione-diretta-proxmox
   installazione-fuss-server-proxmox
   clonazione-macchine-con-clonezilla
   installazioni-scuole-bolzano

