Clonazione di macchine con clonezilla
=====================================

I passi necessari per clonare una macchina su tutto un laboratorio sono,
sulla macchina da clonare:

* `Avviare la prima macchina con Clonezilla <#boot-di-clonezilla-via-rete>`_
* `Impostare Clonezilla selezionando il server da usare per il
  salvataggio dell'immagine <#configurazione-di-clonezilla>`_
* `Clonare la macchina <#clone-di-una-macchina>`_

e quindi su ciascuna delle altre macchine:

* `Avviare ciascuna delle altre macchine con Clonezilla
  <#boot-di-clonezilla-via-rete>`_
* `Impostare Clonezilla selezionando il server dal quale ottenere
  l'immagine da ripristinare <#configurazione-di-clonezilla>`_
* `Avviare la procedura di ripristino <#ripristino-di-una-macchina>`_

Le immagini verranno salvate nella directory ``/var/clonezilla`` tramite
ssh, usando l’utente ``clonezilla`` con la password che si trova sul
server in ``/root/clonezilla_cred.txt`` (vedi `Gestione utente/password
clonezilla`_)

Boot di Clonezilla via rete
---------------------------

Innanzitutto è necessario configurare i vari computer per fare boot da
rete (PXE): come fare dipende dai diversi BIOS presenti sui computer, ma
generalmente è sufficiente premere un tasto per ottenere il menù di boot
e selezionare una voce che nomini network, netboot o PXE.

A quel punto si otterrà il menù con le immagini disponibili sul server
FUSS: scegliere Clonezilla Live:

.. figure:: /images/clonezilla/000-boot_menu.png

Configurazione di Clonezilla
----------------------------

Verrà innanzitutto richiesta conferma della fingerprint del server
(essendo su rete locale si può tranquillamente accettare):

.. figure:: /images/clonezilla/a01-ssh_fingerint.png

quindi inserire la password dell’utente clonezilla, che si trova nel
file ``/root/clonezilla_cred.txt`` sul server:

.. figure:: /images/clonezilla/a02-ssh_password.png

Una schermata riassuntiva mostrerà lo spazio disponibile, premere invio
per continuare:

.. figure:: /images/clonezilla/a03-ssh-confirm.png

Si può quindi avviare Clonezilla:

.. figure:: /images/clonezilla/006-avvia_clonezilla.png

Scegliere la modalità di creazione/ripristino da immagine (anziché la
copia diretta tra computer):

.. figure:: /images/clonezilla/007-device-image.png

Dato che il server per le immagini è appena stato caricato, selezionare
skip:

.. figure:: /images/clonezilla/skip_mount.png

E verrà chiesta ulteriore conferma, premere ancora invio per continuare:

.. figure:: /images/clonezilla/a05-mount_confirm.png

Infine, selezionare la modalità principiante per non dover fornire
troppi dettagli:

.. figure:: /images/clonezilla/018-modalita_beginner.png

Clone di una macchina
---------------------

Selezionare l’opzione per creare l’immagine di un disco (notare che se
questa è la prima volta che si usa clonezilla verranno presentate solo
le due prime scelte, “savedisk” e “saveparts”: tutte le altre voci del
menù vengono abilitate quando sono già presenti delle immagini salvate):

.. figure:: /images/clonezilla/101-savedisk.png

Scegliere il nome da dare all’immagine (il default, oppure qualcosa di
comodo da ricordarsi)

.. figure:: /images/clonezilla/102-nome_immagine.png

Scegliere il disco da clonare; di solito l’unico presente:

.. figure:: /images/clonezilla/103-disco_da_clonare.png

Se la macchina è stata spenta correttamente, e se contiene partizioni
windows, scegliere di non effettuare un check del filesystem; può essere
utile invece fare tale check se la macchina contiene solo partizioni
linux ed un precedente tentativo di clone è fallito:

.. figure:: /images/clonezilla/104-check_filesystem.png

Selezionare di controllare che l’immagine salvata sia ripristinabile
(default):

.. figure:: /images/clonezilla/105-controllo_immagine_salvata.png

Selezionare di non crittare l’immagine; non necessario dato che viene
salvata sul fuss-server:

.. figure:: /images/clonezilla/106-non_crittare_immagine.png

Selezionare di riavviare il computer alla fine della copia:

.. figure:: /images/clonezilla/107-reboot_alla_fine.png

Clonezilla mostrerà ora la riga di comando completa che verrà eseguita;
premere invio per continuare:

.. figure:: /images/clonezilla/108-comando_per_ripetere.png

Viene chiesta conferma di procedere, scrivere ``y`` per continuare:

.. figure:: /images/clonezilla/109-prima_conferma.png

Clonezilla procederà alla creazione di un’immagine e al suo salvataggio

.. figure:: /images/clonezilla/109a-clone.png

Fino ad annunciare di essere pronto al riavvio:

.. figure:: /images/clonezilla/110-preparazione_al_reboot.png

Infine, premere invio per permettere il riavvio (dato che clonezilla è
stato avviato via rete non è necessario rimuovere nessun disco):

.. figure:: /images/clonezilla/111-reboot.png

Ripristino di una macchina
--------------------------

Selezionare l’opzione per ripristinare un’immagine da disco:

.. figure:: /images/clonezilla/201-restoredisk.png

Selezionare il nome dell’immagine; se ne è presente più di una è
necessario ricordarsi il nome dato in fase di creazione:

.. figure:: /images/clonezilla/202-selezione_immagine.png

Selezionare il disco su cui ripristinare l’immagine; di solito l’unico
presente:

.. figure:: /images/clonezilla/203-selezione_disco.png

Selezionare di verificare l’immagine prima del ripristino:

.. figure:: /images/clonezilla/204-controllo_immagine.png

Selezionare di riavviare il computer alla fine del ripristino:

.. figure:: /images/clonezilla/205-reboot.png

Clonezilla mostrerà ora la riga di comando completa che verrà eseguita;
premere invio per continuare:

.. figure:: /images/clonezilla/206-comando_per_ripetere.png

Clonezilla inizierà a verificare l’immagine selezionata:

.. figure:: /images/clonezilla/206a-verifica.png


Quindi chiederà per due volte conferma di voler continuare, cancellando
tutto quanto presente eventualmente sul disco locale:

.. figure:: /images/clonezilla/207-prima_conferma.png

.. figure:: /images/clonezilla/208-seconda_conferma.png

A questo punto verrà effettuato il ripristino:

.. figure:: /images/clonezilla/209-ripristino.png

Fino ad annunciare di essere pronto al riavvio:

.. figure:: /images/clonezilla/209-preparazione_al_reboot.png

Infine, premere invio per permettere il riavvio (dato che clonezilla è
stato avviato via rete non è necessario rimuovere nessun disco):

.. figure:: /images/clonezilla/210-reboot.png

Note varie
----------

Gestione utente/password clonezilla
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L’utente ``clonezilla`` viene creato da ``fuss-server create`` che
genera una password casuale e la salva in ``/root/clonezilla_cred.txt``
e lo aggiunge al gruppo ``sshaccess``.

Con ``fuss-server upgrade`` viene verificata (e nel caso ripristinata)
la presenza dell’utente e la sua appartenenza al gruppo. Per la
password, se il file ``/root/clonezilla_cred.txt`` non è presente ne
viene generata ed impostata una nuova.

Se si vuole cambiare password per l’utente ``clonezilla`` si può usare
semplicemente ``passwd``, ma è cura di chi lo fa aggiornare
``/root/clonezilla_cred.txt`` con la nuova password.
