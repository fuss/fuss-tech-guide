.. _restore:

****************
 Dump e restore
****************

Aggiornamento del fuss-server via dump e restore
================================================

A partire dalla versione 8.0.48 e 10.0.28 del fuss-server sono stati
predisposti gli strumenti necessari ed una procedura che consente di
eseguire il dump di un server FUSS 8 e ripristinarlo su di un server
FUSS 10, per poter consentire il passaggio da FUSS 8 a FUSS 10
mantenendo la gran parte dei dati presenti sul server di partenza. Lo
stesso vale per il passaggio da FUSS 10 a FUSS 12.

Operazioni di dump sul server originario (FUSS 10)
==================================================

La procedura prevede che si effettui un backup completo delle home degli
utenti sul server (che deve comprendere oltre a ``/home`` qualunque altra
directory in cui queste possono essere state inserite), da cui poi
ripristinare i dati. Si darà per scontato che questo sia stato fatto con il
programma ``fuss-backup`` e che il ripristino delle ``/home`` avvenga con
questo programma (il ripristino dei dati delle home può essere fatto in un
secondo tempo).

Il primo passo della procedura è eseguire lo script
``/usr/share/fuss-server/scripts/fuss-dump.sh`` sul server di partenza (si
assume che sia un FUSS 8), questo creerà un file
``fuss-server-dump-$DATA.tgz`` nella directory corrente con i dati necessari
al ripristino, che dovrà essere copiato a destinazione sul nuovo server (i
dati dell'archivio restano comunque salvati anche nel server originale sotto
``/var/backups/fuss-dump``).

Operazioni sul nuovo server (FUSS 12)
=====================================

Occorrerà poi installare il sistema operativo sul nuovo server, e configurarne
la rete; è opportuno usare lo stesso hostname del server precedente; cambiarlo
potrebbe infatti dare luogo ad inconvenienti relativi a nomi rimasti in cache
sui client, quindi è meglio mantenere sempre lo stesso hostname.  Inoltre se
sul vecchio server si stanno usando le quote disco, occorrerà che queste siano
abilitate anche sul nuovo.

.. warning::
   La procedura di dump e quella di restore assume che le quote siano attive
   al massimo su un solo filesystem, come normale su un fuss-server, questo
   viene ricavato da ``/etc/fstab`` cercando le opzioni di montaggio
   ``usrquota`` e ``grpquota``, che devono essere attive su un solo filesystem
   (non è necessario sia lo stesso fra macchina originale e nuova macchina).

Per poter utilizzare il ripristino occorre installare il nuovo server FUSS10
normalmente, con ``fuss-server create``, mantenendo però identici il dominio,
la master password ed il workgroup. É opportuno mantenere anche la stessa rete
interna ed il range del DHCP, dato che alcuni dati (in particolare le
impostazioni del firewall e le reservation statiche) possono dipendere da
queste impostazioni.

Pertanto prima dell'esecuzione di ``fuss-server create`` è opportuno copiarsi
i dati della configurazione del server di partenza (il file
``/etc/fuss-server/fuss-server.yaml``, che viene comunque anche incluso
nell'archivo prodotto dallo script di dump come ``fuss-server.yaml.old``) e
modificare soltanto le voci ``external_ifaces:`` e ``internal_ifaces:`` che
possono essere cambiate (come avviene nel passaggio da Jessie a Buster),
adattandole al nuovo server nel caso sia necessario.

Si possono comunque estrarre i dati dal dump che è un semplice tar compresso
con::

  tar -xvzf fuss-server-dump-$DATA.tgz

ed il file di configurazione del fuss server precedente sarà in
``fuss-dump/fuss-server.yaml.old``.

Una volta completata la configurazione di base del fuss-server occorrerà
copiarvi il file con il dump dei dati, ed eseguire lo script di restore con::

  /usr/share/fuss-server/scripts/fuss-restore.sh fuss-server-dump-$DATA.tgz

lo script una volta scompattato l'archivio nella sottodirectory
``fuss-dump`` (sovrascriverà eventuali file omonimi contenuti) fermerà tutti
i servizi, reimporterà i dati, e poi li riavvierà.

Dato che i dati di NFS/Kerberos possono risentire di caching da parte del
server (che gira nel kernel) può essere opportuno riavviare il server dopo
aver eseguito il restore.

A questo punto gli utenti e le configurazioni dei servizi saranno stati
reimportati, ma resterà da ripristinare il contenuto delle home degli utenti,
operazione da fare con le istruzioni date per l'uso di ``fuss-backup``.

.. warning::
   Il nuovo server, essendo stato reinstallato, avrà delle chiavi SSH diverse
   dal vecchio, pertanto collegandosi dai client si potranno avere degli
   errori di mancato riconoscimento delle fingerprint SSH del server dovuti a
   questo cambiamento. Occorrerà cancellare le vecchie chiavi ed accettare le
   fingerprint delle nuove chiavi (si abbia cura di segnarsele in fase di
   reinstallazione).

Come ultima nota, a partire dalla versione 8.0.50 (e 10.0.34) del fuss-server
il file con i default di configurazione
(``/etc/fuss-server/fuss-server-defaults.yaml``) non verrà inserito nel dump,
ma semplicemente copiato a fianco di ``fuss-server.yaml.old`` (come
``fuss-server-defaults.yaml.old``) nella directory ``fuss-dump``, questo
perché nello sviluppo di FUSS 10 sono stati aggiunte alcune
ulteriori variabili di configurazioni non presenti con Fuss 8, la cui mancanza
causerebbe il fallimento di ``fuss-server`` ad una esecuzione successiva al
ripristino. Per questo motivo il file viene comunque salvato in modo da poter
verificare, e riapplicare, eventuali modifiche effettuate rispetto al default
installato dal pacchetto.

.. warning::
   Si tenga presente che la rimozione di ``fuss-server-defaults.yaml`` dai
   file ripristinati totalmente avviene soltanto se si esegue ``fuss-dump.sh``
   nella versione installata con un fuss-server posteriore la 8.0.50, questo
   dovrebbe essere sempre vero se si sono tenute aggiornate le macchine come
   buona pratica, se questo non è avvenuto, il file sarà ancora presente e
   sovrascriverà quello nuovo installato con ``fuss-server create``. Se non si
   è sicuri si salvi il file prima di eseguire il restore.
