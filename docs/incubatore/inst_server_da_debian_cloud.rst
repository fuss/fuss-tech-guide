Proposta 1: Installazione di FUSS Server partendo da Debian Cloud upstream
==========================================================================

**Autore:** Marco Marinello

Se si vuole installare velocemente un server virtualizzato
partendo da una versione aggiornata di Debian la scelta migliore è
sicuramente partire da un disco messo a disposizione direttamente da Debian.

Recandosi su https://cloud.debian.org/images/cloud/ è possibile vedere
un elenco delle immagini cloud disponibili. Scegliere
quella relativa alla corrente distro del server (:code:`buster` al momento)
quindi la penultima voce, prima di :code:`daily`, ovvero l'ultima stabile.
Fra i vari file elencati, copiare il link del :code:`generic-amd64-*.qcow2`,
collegarsi al server Proxmox che lo dovrà ospitare e, dopo aver creato
la macchina virtuale, eseguire

.. code:: bash

          cd /opt
          wget https://cloud.debian.org/images/cloud/buster/20201023-432/debian-10-generic-amd64-20201023-432.qcow2
          # Importa il disco sullo storage local-lvm per la macchina con ID 100
          qm importdisk 100 debian-10-generic-amd64-20201023-432.qcow2 local-lvm

A questo punto rimuovere il vecchio disco (che era stato creato automaticamente
dal wizard) dalla VM attraverso la GUI di Proxmox e "collegare"
il nuovo. Si estenda il disco (solitamente il template consta di 2GB quindi
è consigliabile incrementare almeno di 48 per arrivare a 50GB di root) e se
ne aggiunga un secondo da usare poi per le home. Aggiungere una porta seriale,
le porte di rete necessarie ed un volume Cloud-Init.
Una volta opportunamente valorizzata la tab "Cloud-Init"
sarà sufficiente avviare la VM perché Cloud-Init perfezioni la configurazione.
Una volta che la macchina è avviata attendere che unattended-upgrades installi
gli aggiornamenti quindi eseguire

.. code:: bash

          fdisk /dev/sdb
          # n
          # p
          # 1
          # <enter>
          # <enter>
          # w
          mkfs.ext4 /dev/sdb1
          # Abilita controlli sul disco
          tune2fs -c 50 -i 30d /dev/sda1
          tune2fs -c 50 -i 30d /dev/sdb1
          blkid


Si utilizzi il risultato di blkid per valorizzare il file :code:`/etc/fstab`.
Sostituire anche gli ultimi due valori nel disco di root con `1 0`
ed utilizzare invece `1 1` per il disco con le home.
Si esegua infine :code:`mount -a` e verificare attraverso il comando
:code:`df -h` che il disco sia stato montato correttamente.

Si può ora procedere alla rimozione di Cloud-Init (che creerebbe
solo disguidi d'ora in poi) ed alla configurazione vera e propria:

.. code:: bash

          apt purge cloud-init
          mv /etc/network/interfaces.d/50-cloud-init /etc/network/interfaces
          apt install gnupg2 qemu-guest-agent nfs-common
          apt install -t buster-backports ansible systemd
          wget -O - https://archive.fuss.bz.it/apt.key | apt-key add -
          sed -i 's/main/main contrib/g' /etc/apt/sources.list
          echo 'deb http://archive.fuss.bz.it/ buster main contrib' >> /etc/apt/sources.list
          apt update
          apt install fuss-server fuss-backup


