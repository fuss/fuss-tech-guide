******************
Incubatore di idee
******************

Questa sezione rappresenta una piccola vetrina di idee e proposte in
fase di incubazione che debbono essere ancora approfonditamente testate.
Hanno lo scopo di dare la possibilità

- a chiunque abbia delle proposte, di vederle pubblicate dopo una prima
  fase di approvazione da parte del team di progetto;
- a chiunque voglia, di visionare e/o testare tali proposte aggiungendo
  i propri commenti sulla mailing list **fuss-devel**
  https://www.fuss.bz.it/cgi-bin/mailman/listinfo/fuss-devel.

.. toctree::
   :maxdepth: 2

   inst_server_da_debian_cloud
   migrazione_soft
