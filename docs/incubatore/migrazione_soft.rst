Proposta 2: Migrazione soft da server FUSS 8 a server FUSS 10
=============================================================

**Autore:** Marco Marinello

Al fine di semplificare la migrazione delle scuole dall'ottava versione di
FUSS Server e Client alla decima è possibile spezzare la migrazione in due.
Si può migrare distintamente server e client, non ha importanza l'ordine.
La presente guida si applica a tutti i seguenti contesti:

- Server FUSS 8 e client FUSS 8
- Server FUSS 8 e client FUSS 9
- Server FUSS 8 e client FUSS 10


Backup del vecchio server
-------------------------

Come prima cosa assicurarsi di "portare via" tutti i dati essenziali alla
migrazione. Si suppone :code:`fuss-backup` sia già configurato ed operativo.
Se così non dovesse essere, si segua la relativa guida. Si verifichi
nel file :code:`/etc/fuss-backup/fuss-backup.conf` che la variabile
:code:`PATHS` sia valorizzata correttamente e si aggiunga :code:`/root` e
le altre directory che si vuole backuppare.

.. code:: bash

          PATHS="/etc /home /var/backups /var/lib /var/log /var/mail /var/local /root /srv"


D'ora in poi si suppone che la rete sia scarica (non vi devono essere
operazioni in corso o utenti collegati).

Si torni quindi nella home di root e si esegua

.. code:: bash

          slapcat > dump_pre_mig.ldif

che genererà un backup di LDAP nel file :code:`/root/dump_pre_mig.ldif` .

Si esegua poi, per creare un backup dei gruppi

.. code:: bash

          octofussctl -u root http://localhost:13400/conf > groups_backup.txt <<EOF
          ls users/groups
          EOF

Si ripulisca il file rimuovendo la prima riga "Welcome to the octofussd
client [...]", il primo e l'ultimo :code:`/>`. Si esegua dunque

.. code:: bash

          sed -i 's+^+create users/groups/+' groups_backup.txt

cosicché il file sia pronto per l'importazione nel nuovo server.

Spostare questo file oltre a :code:`/root/dump_pre_mig.ldif` e
:code:`/etc/fuss-backup/fuss-backup.conf` sulla propria macchina quindi
lanciare :code:`fuss-backup` . Se :code:`root` è fra i destinatari si
potrà verificare l'esito del backup col comando :code:`mail`.  Ci si
annoti anche le chiavi `SSH` autorizzate all'accesso e gli IP della
macchina.
Il nuovo server dovrà essere *identico* al vecchio, nulla potrà essere
modificato fra IP, nome di dominio, hostname del server e password di
root.


Installazione del nuovo server
------------------------------

Si parta dall'installazione del fuss-server usando uno qualunque dei
metodi indicati su questa guida.
Qui si prosegue intendendo che :code:`fuss-server` sia installato assieme
a :code:`fuss-backup` ma non sia mai stato eseguito.

Si inizi montando la risorsa su cui era stato fatto il backup dal vecchio
server. Lo si può verificare dal file :code:`fuss-backup.conf` copiato
poc'anzi.

.. code:: bash

          mount 172.16.11.22:/myback /mnt

Si può ora procedere all'estrazione di alcuni file necessari alla
prosecuzione (si suppone si lavori come :code:`root`):

.. code:: bash

          cd
          mkdir from_backup
          cd from_backup
          borg extract --progress /mnt/borgdata::$(borg list /mnt/borgdata|tail -1|cut -d ' ' -f 1) etc/fuss-server etc/fuss-backup etc/fuss-server etc/fuss-backup var/lib/krb5kdc/principal etc/krb5kdc etc/krb5.keytab root

Si copi quindi il vecchio file :code:`fuss-server.yaml` in-place e si
esegua :code:`fuss-server`. Se la configurazione dell'host è identica a
quella precedente non dovrebbero esserci errori e nessuna domanda
dovrebbe essere posta. Se succedesse, si verifichi la correttezza della
configurazione fatta sinora.
Se era configurato il captive portal, è possibile riconfigurarlo subito,
previo accertamento dei requisiti del file network come descritti nella
relativa guida.

.. code:: bash

          cp etc/fuss-sevrer/fuss-server.yaml /etc/fuss-server
          fuss-server create
          # Per captive portal (se richiesto) - verificare corrispondenza interfacce
          cp etc/fuss-server/fuss-captive-portal.conf /etc/fuss-server
          fuss-server cp

A questo punto è consigliabile un riavvio per accertarsi che tutto funzioni.


Importazione degli utenti
*************************

.. warning::

            Ci si accerti che la versione di octonet sia >= 10.0.4-2
            e octofussd >= 10.0.15-1


Creazione dei gruppi
~~~~~~~~~~~~~~~~~~~~

Per non incorrere in errori, come prima cosa si ricrei i gruppi che si
aveva sul vecchio server. Il file :code:`groups_backup.txt` dovrebbe essere
stato estratto dal backup in :code:`/root/from_backup/root/groups_backup.txt`

.. code:: bash

          octofussctl -u root http://localhost:13400/conf < /root/from_backup/root/groups_backup.txt


Conversione del file LDIF da OctoNet
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ci si colleghi dalla propria macchina ad OctoNet via browser e si esegua
l'accesso. Si vada quindi ad `Utenti e Gruppi > Converti file LDIF`.
Si carichi il file precedentemente esportato e si scarichi l'output in CSV.
Si modifichi quindi il file CSV con LibreOffice e si rimuova la colonna
`password` che contiene quelle generate automaticamente e si salvi.


Importazione del nuovo CSV
~~~~~~~~~~~~~~~~~~~~~~~~~~

Ora che si è in possesso della lista degli utenti è possibile importare
questi nel nuovo server. Nuovamente da `Utenti e Gruppi` si scelga
`Importa da CSV`. Si carichi il file CSV appena preparato, si selezioni
la prima riga (intestazione) per renderla ignorata e si associ le
colonne.
Ci si accerti di aver associato la `Hashed password` altrimenti gli utenti
verranno creati senza password.
Si autorizzi l'importazione e si attenda pazientemente il termine.
La barra di progresso dovrebbe segnalare l'avanzamento dell'operazione.

Ora tutti gli utenti dovrebbero essere stati ricreati con le relative
home (vuote).


Ripristino dei file di Kerberos
*******************************

Una volta creati tutti gli utenti si possono spostare in-place i vecchi
file di Kerberos con le credenziali.

.. code:: bash

          systemctl stop octofussd octonet fuss-manager krb5-admin-server krb5-kdc nfs-common nfs-kernel-server
          cp /etc/krb5.keytab{,-fs_old}
          cp /var/lib/krb5kdc/principal{,-fs_old}
          cp /root/from_backup/etc/krb5.keytab /etc/
          cp /root/from_backup/var/lib/krb5kdc/principal /var/lib/krb5kdc/

Si riavvii ora il server per tornare ad una situazione pulita dal punto
di vista dei servizi. Ora è possibile accendere un client e provare
l'accesso con un utente importato. Se tutto è stato eseguito
correttamente, si dovrebbe accedere e visualizzare la propria home
(vuota).


Estrazione delle home
*********************

Ultimo step è il ripristino delle home dal backup. Dato che è un
processo lungo, è consigliabile eseguirlo in :code:`screen`.

.. code:: bash

    cd /
    borg extract --progress /mnt/borgdata::$(borg list /mnt/borgdata|tail -1|cut -d ' ' -f 1) home

Configurazione del nuovo backup
*******************************

Nel riconfigurare :code:`fuss-backup` è consigliabile non usare di nuovo
la directory :code:`borgdata` per non intaccare il vecchio backup.
Si preferisca piuttosto qualcosa come :code:`borgdata10`.



Ripristino di altri dati o configurazioni precedenti
****************************************************

Il server dovrebbe essere ora pienamente operativo. È possibile tuttavia
ripristinare ulteriori dati (come le immagini di clonezilla) o
configurazioni (come quelle del firewall) dal backup.

Questi sono lasciati come esercizio al lettore.
