.. _`configurazione-fuss-server`:

Configurazione del server
=========================

Una volta completata l'installazione di base della macchina si deve avere cura
di configurare correttamente la rete, identificando le interfacce di rete
interna (quella rivolta verso le macchine dell'aula) ed esterna (quella da cui
si accede ad internet). Si deve anche impostare l'hostname ed il nome e
dominio della macchina su cui si lavora.

.. warning::

   Impostare l'hostname della macchina al suo valore finale prima di eseguire
   procedura di configurazione è importante, cambiare l'impostazione in
   seguito è problematico. In genere lo si fa in fase di installazione del
   sistema operativo, ma qualora questo non fosse quello voluto la correzione
   deve essere applicata **prima** di lanciare il comando
   ``fuss-server``. L'hostname della macchina infatti viene usato nel playbook
   di configurazione per la creazione di vari file che poi vengono
   referenziati, cambiarlo successivamente può non avere impatto immediato nel
   funzionamento del server, ma può averlo nella successiva esecuzione (ad
   esempio per aggiornamento) di ``fuss-server``.


Per eseguire la creazione della configurazione è necessario eseguire il
comando::

   # fuss-server create

questo, se non è già stata eseguita in precedenza una configurazione,
chiederà una serie di dati. In particolari saranno richiesti:

* la rete locale che verrà utilizzata (es. 192.168.1.0/24);
* il nome a dominio della rete (es. scuola.bzn);
* il workgroup per il dominio windows (es. SCUOLA);
* un intervallo di indirizzi per il DHCP (es. 192.168.1.10 192.168.1.100);
* la master password del server (usarne una lunga e complicata!);
* la località (es. Bolzano);
* l'interfaccia della rete esterna (es. ens18);
* l'interfaccia della rete interna (es. ens19);

Il programma per la richiesta dei dati userà l'interfaccia testuale nel
terminale.

Alla fine dell'inserimento verrà eseguito un controllo di correttezza
dei dati inseriti; in caso di problemi verrà riaperta una nuova finestra
di immissione con i soli dati problematici, fino a quando non si ottiene
una configurazione completamente valida.

Infine il programma procederà automaticamente alla configurazione di
tutto quanto necessario, compresa l'eventuale installazione di
dipendenze. 

Qualora non si voglia che questo avvenga, ad esempio perché si vogliono
modificare da subito i parametri di default con cui avviene l'installazione,
si può eseguire al posto di ``fuss-server create`` il comando ``fuss-server
configure`` che esegue solo la richiesta delle precedenti informazioni e
genera il corrispondente file di configurazione
``/etc/fuss-server/fuss-server.yaml``. Se si vogliono modificare delle
variabili di controllo che non siano quelle definite in questo file, questa
vanno ridefinite in ``/etc/fuss-server/fuss-server-default.yaml`` (vedi `File
dei default del Fuss Server`). Una volta fatte le modifiche si potrà passare
alla configurazione lanciando allora ``fuss-server create``.

Nel caso in cui il programma si interrompesse a causa di problemi
temporanei (ad esempio una caduta della connessione ad internet) è
possibile rilanciarlo con: ``fuss-server create``; ovviamente non verrà
richiesta la configurazione, ma il programma procederà automaticamente
con la sua fase automatica.
