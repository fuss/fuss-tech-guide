I principali file di configurazione
===================================

Sotto ``/etc/fuss-server`` sono installati una serie di file che mantengono
dati di configurazione usati dai servizi del server, dettagliati nei
paragrafi seguenti, gestiti tramite ``octofuss``. Ovviamente non devono
essere cancellati, pena il malfunzionamento dei servizi cui fanno
riferimento.

File di configurazione del Fuss Server
--------------------------------------

Nel file ``/etc/fuss-server/fuss-server.yaml`` si trova la
configurazione generale del Fuss Server. Il formato è un `file di
variabili di ansible
<http://docs.ansible.com/ansible/playbooks_variables.html#variable-file-separation>`_
ovvero un semplice dizionario chiave - valore in yaml, ad esempio::

   chiave: valore
   altra_chiave: nuovo valore

Deve contenere i valori predefiniti descritti in
:ref:`configurazione-fuss-server`; contrariamente agli altri file può
essere cancellato, e in quel caso viene ricreato a partire da un esempio
vuoto.

Per modificare le impostazioni in tale file, si può anche lanciare::

   # fuss-server configure -r

(l'opzione ``-r`` è necessaria, perché se le impostazioni sono presenti il
programma non le richiede).

Per applicare le modifiche lanciare::

   # fuss-server upgrade

che farà la verifica di coerenza dei valori presenti nel file e li
applicherà al sistema.

Dato che il file può venire riscritto dal comando fuss-server, è
opportuno non usarlo per configurazioni aggiuntive, per le quali è
invece presente il file descritto nella sezione successiva.

.. _`file-default-fuss-server`:

File dei default del Fuss Server
--------------------------------

Sotto ``/etc/fuss-server/`` viene installato anche secondo file,
``fuss-server-defaults.yaml``, sempre in formato yaml, nel quale si
possono impostare altre variabili per personalizzare l'installazione.

Questo file può essere vuoto (la versione di default installata dal
pacchetto contiene solo commenti), ma non deve essere cancellato,
altrimenti ``fuss-server`` darà errore non trovandolo.

La tabella seguente descrive le variabili che possono essere definite in
questo file per controllare alcune caratteristiche assunte
dall'installazione. 

.. list-table::
   :header-rows: 1

   * - Variabile
     - Significato
     - Contenuto
     - Default
   * - ``smb_pass_policy``
     - Abilita la scadenza delle password su Samba
     - Booleano
     - no
   * - ``pass_age``
     - Abilita la scadenza delle password su LDAP dopo i giorni indicati
     - Intero
     - (vuoto)
   * - ``bad_provider``
     - Disabilita la validazione DNSSEC in caso di provider problematico
     - Booleano
     - no
   * - ``dans_maxchild``
     - Numero massimo di processi figli di e2guardian.
     - Intero
     - 600
   * - ``proxy_win_exclude``
     - Configura Squid per escludere i client Windows.
     - Booleano
     - yes
   * - ``dans_exclude_localnet``
     - Esclude la rete locale dal filtraggio di e2guardian.
     - Booleano
     - yes
   * - ``clonezilla_dir``
     - Directory per clonezilla
     - Stringa
     - ``/srv/clonezilla``
   * - ``dhcp_default_lease_time``
     - Durata di default di una lease dhcp.
     - Intero
     - 86400
   * - ``dhcp_max_lease_time``
     - Durata massima di una lease dhcp.
     - Intero
     - 172800
   * - ``standard_ldap_groups``
     - Gruppi standard  da predefinire su LDAP
     - Lista
     - vedi ``configurable.yml``
   * - ``squid_filedescriptors``
     - Massimo numero di filedescriptor per squid
     - Intero
     - 10240
   * - ``e2guardian_install``
     - Se installare o meno e2guardian
     - Booleano
     - yes
   * - ``e2_nofile``
     - Limite di file aperti per e2guardian
     - Intero
     - 8192
   * - ``e2_workers``
     - Limite di httpworker per e2guardian
     - Intero
     - 1024
   * - ``samba_logon_path``
     - Valore di ``logon path`` di samba
     - Stringa
     - (vuoto)
   * - ``samba_logon_home``
     - Valore di ``logon home`` di samba
     - Stringa
     - (vuoto)
   * - ``samba_logon_drive``
     - Valore di ``logon drive`` di samba
     - Stringa
     - (vuoto)
   * - ``samba_logon_script``
     - Valore di ``logon script`` di samba
     - Stringa
     - (vuoto)
   * - ``squid_require_internet_group``
     - Se appartenere al gruppo ``internet`` è richiesto per navigare
     - Booleano
     - yes

É possibile modificare questi valori di default, in modo che la relativa
configurazione non venga cambiata in caso di aggiornamento del fuss-server, ad
esempio se si vuole riabilitare l'accesso ad internet da parte di macchine
Windows si può cambiare il valore di ``proxy_win_exclude`` da ``yes`` a
``no``, o se si vuole riabilitare il filtro dei contenuti sulla rete locale
(non necessario nelle scuole del progetto per la presenza di un filtraggio a
monte) si può modificare ``dans_exclude_localnet`` da ``yes`` a ``no``.

Tutte le volte che si effettua una modifica di una di queste variabili, perché
la relativa configurazione venga applicata, occorrerà eseguire ``fuss-server
upgrade``. Dato che ``fuss-server-defaults.yaml`` è marcato come file di
configurazione, non verrà sovrascritto in caso di aggiornamento del
pacchetto e le modifiche fatte verranno mantenute.

Server DHCP
-----------

Il file ``/etc/dhcp/dhcp-reservations`` contiene le assegnazioni
statiche degli indirizzi per il client , che viene incluso nella
configurazione ed installato vuoto in fase di creazione del fuss-server.
Il contenuto di questo file viene gestito tramite octofuss ed usa il
formato previsto da ``dhcp.conf`` per le "_reservation_", vale a dire una
serie di voci nella forma::

   host nomeclient {
           hardware ethernet 00:XX:XX:XX:XX:XX;
           fixed-address nomeclient.dominio.it;
   }

se nomeclient.dominio.it è definito nel DNS, oppure::

   host nomeclient {
           hardware ethernet 00:XX:XX:XX:XX:XX;
           fixed-address IP.AD.DR.ES;
   }

usando un IP fuori dal range del DHCP (che di default prende gli
indirizzi fra 1/4 e 3/4 della rete usata per la LAN). Se il file
``/etc/dhcp/dhcp-reservations`` viene eliminato, il servizio non
viene avviato.

.. _file-configurazione-firewall:

Firewall
--------

I file di configurazione per il firewall sono tutti nel formato::

   valore:descrizione

con l'eccezione di quello che descrive macchine interne consentite e
servizi esterni raggiungibili, che è nella forma::

   host:servizio:descrizione

Le righe che iniziano con un # sono considerate commenti e vengono
ignorate, inoltre quanto non compare all'interno del blocco marcato
``ANSIBLE MANAGED BLOCK`` non viene sovrascritto dalla riesecuzione del
comando di installazione del fuss-server.

Se si specifica un host questo deve essere o un nome a dominio o un
indirizzo IP, si tenga conto però del fatto che il firewall effettua la
risoluzione dell'indirizzo IP al suo avvio, pertanto usare un nome a
dominio è rischioso, specie quando ad esso possono corrispondere a
diversi indirizzi (ad esempio www.google.com) perché il firewall farà
riferimento soltanto a quello risolto in fase di avvio.

Quando si specifica un servizio invece questo deve essere nella forma
porta/protocollo (ad esempio 123/udp o 2628/tcp). Si raccomanda di usare
le minuscole ed i valori numerici.

La descrizione può essere omessa, verrà comunque inserita una
descrizione standard.

I file utilizzati sono i seguenti; vengono abilitati su firewall nella
sequenza in cui sono indicati nella tabella (questo significa che un
host nel primo file non potrà raggiungere comunque nessun servizio
esterno, neanche quelli resi accessibili con gli altri file):


.. list-table::
   :header-rows: 1

   * - File
     - Contenuto
     - Formato
   * - ``/etc/fuss-server/firewall-denied-lan-hosts``
     - Host che **non** possono raggiungere alcun servizio sulla rete
       internet
     - IP-address
   * - ``/etc/fuss-server/firewall-allowed-lan-hosts``
     - Elenco di host che possono accedere ai servizi della rete internet
       senza alcun filtro e/o limitazione
     - IP-address
   * - ``/etc/fuss-server/firewall-allowed-wan-hosts``
     - Elenco di host sulla rete internet che possono essere acceduti senza
       alcun filtro e/o limitazione
     - IP-address
   * - ``/etc/fuss-server/firewall-allowed-wan-services``
     - Servizi sulla rete internet che possono essere raggiunti senza
       limitazioni e/o controllo. Devono essere indicate le porte da
       utilizzare per il servizio
     - porta/protocollo
   * - ``/etc/fuss-server/firewall-external-services``
     - Servizi offerti dal server sulla rete esterna
     - porta/protocollo
   * - ``/etc/fuss-server/firewall-allowed-wan-host-services``
     - Servizi sulla rete internet raggiungibili da specifici host della
       rete interna senza alcun limite.
     - IP-address:porta/protocollo
   * - ``/etc/fuss-server/firewall-custom-rules``
     - Regole iptables custom.
     - iptables <options>

Si ricordi che dopo aver eseguito modifiche manuali sui suddetti file
occorre rilanciare il firewall perché esse diventino effettive, con::

   /etc/init.d/firewall restart

Nel caso si sia installato anche il Captive Portal il firewall usa
l'ulteriore file ``/etc/fuss-server/fuss-captive-portal.conf``, per ottenere
la rete usata dallo stesso e abilitare gli accessi necessari. Detto file
non deve essere cancellato, pena la mancanza dei suddetti accessi ed il
conseguente non funzionamento del Captive Portal in caso di riavvio del
firewall.

squid (web proxy)
-----------------

Il file ``/etc/squid3/squid-added-repo.conf`` permette di aggiungere
siti web all'acl ``repositories`` ai quali l'accesso è sempre permesso
senza autenticazione.

I contenuti devono essere della forma::

   acl repositories url_regex XXX

dove ``XXX`` è l'espressione regolare che rappresenta il sito che
vogliamo abilitare; esempi di sintassi sono presenti in ``squid.conf``,
come::

   acl repositories url_regex ^http://.*.debian.org/

.. _file-configurazione-dansguardian:

e2guardian (filtro contenuti)
-----------------------------

Nel file ``/etc/fuss-server/content-filter-allowed-sites`` viene mantenuta una
lista dei siti cui viene comunque garantito accesso da e2guardian (successore
di Dansguardian). Questa lista può essere gestita anche dall'interfaccia web
di *OctoNet* dal menu *Filtro web*, che consente anche di configurare altri
file di configurazione sotto ``/etc/e2guardian/lists/``.

.. attention::
   il filtro contenuti richiede risorse significative sul server; nelle
   scuole in cui è già presente una protezione a monte può convenire
   escludere gli IP della rete locale dal filtraggio aggiungendo a tali
   file i range dei PC da escludere o i singoli IP se tali PC hanno IP
   statico.

dhcpd
-----

Il file ``/etc/dhcp/dhcpd-added.conf`` permette di aggiungere parametri
di configurazione del demone dhcp oltre a quanto gestito dal
fuss-server.

Tale file viene caricato alla fine di ``/etc/dhcp/dhcpd.conf``, e ne
condivide la sintassi.

bind (DNS)
----------

Il file ``/etc/bind/named.added.conf.local`` permette di aggiungere
parametri di configurazione di bind oltre a quanto gestito dal
fuss-server.

Tale file viene caricato alla fine di ``/etc/bind/named.conf.local``, e ne
condivide la sintassi.
