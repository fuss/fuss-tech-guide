Troubleshooting - Risoluzione di problemi
=========================================

Reset password amministrative del fuss-server
---------------------------------------------

Cambio password di root
^^^^^^^^^^^^^^^^^^^^^^^

Per cambiare la password di root del fuss-server non è necessaria nessuna
procedura particolare, basta eseguire semplicemente il comando ``passwd`` da
``root``.

Cambio password generale dei servizi (master password)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La situazione è più complessa qualora invece si voglia cambiare la "master
password" che si è usata in fase di creazione del server (quella che viene
mantenuta nella variabile ``pass`` del file
``/etc/fuss-server/fuss-server.yaml``).

In tal caso le operazioni da eseguire sono molte, in quanto essa viene usata
estensivamente nella configurazione di tutti i servizi, pertanto si è fornito
uno script di gestione apposito (``mpwchange.sh``, installato come gli altri
script in ``/usr/share/fuss-server/scripts``) che consente di effettuarle in
una volta sola.

Per effettuare il cambio è sufficiente eseguire::

  /usr/share/fuss-server/scripts/mpwchange.sh vecchia_password nuova_password

lo script controlla in ``/etc/fuss-server/fuss-server.yaml`` che la vecchia
password corrisponde e poi la cambia per i vari servizi (ed in
``/etc/fuss-server/fuss-server.yaml`` stesso).


.. ::
    ATTENZIONE: questo script purtroppo non funziona nel caso la
    ``vecchia-password`` contenga uno o più dei seguenti ``"caratteri
    speciali":   &, \, /, $``

    Per server dove la ``LA MASTER PASSWORD`` contiene caratteri
    speciali "non graditi" da LDAP (quelli appena indicati), sarà
    necessario procedere col cambio della MASTER-PASSWORD seguendo la
    procedura qui sotto indicata:

Qualora il comando desse un errore, si possono eseguire i singoli passi
manualmente come sotto indicato:

1. "Preparare" il seguente comando facendo attenzione a riempire
   correttamente i seguenti 5 campi contenuti i dati di quella specifica
   scuola e la nuova password::

      ldappasswd -v -x -h 127.0.0.1 -D "cn=admin,dc=___,dc=___" -W -A -s "_NUOVA-PASSWORD_" "cn=admin,dc=__,dc=__"


   Campi da riempire (5)::

      * dc= *1* (es.appiano)
      * dc= *2*	 (es.blz)
      * "_NUOVA-PASSWORD_" (dove la nuova password rimane all'interno delle virgolette " ")
      * dc= *3* (es.appiano)
      * dc= *4*  (es.blz)

   Esempio::

      "_NUOVA-PASSWORD_"= LUNA.69

   Il comando da lanciare è nell'esempio::

      ldappasswd -v -x -h 127.0.0.1 -D "cn=admin,dc=appiano,dc=blz" -W -A -s "LUNA.69" "cn=admin,dc=appiano,dc=blz"

2. N.B.: Viene richiesta (3 volte) solamente la ``vecchia-password`` ,
   in quanto la ``nuova password`` viene “passata” già dal comando dal
   parametro ``-s`` ::

      * Enter LDAP Password:		*(vecchia-password)
      * Re-enter old password: 		*(vecchia-password)
      * Enter LDAP Password:		*(vecchia-password)

   L'operazione si sarà conclusa correttamente se verrà restituito il
   seguente OUTPUT ::

     Result: Success (0)

3. Modificare i seguenti file, sostituendo la ``vecchia-password`` con
   la ``nuova-password``. ::

      vim /root/.ldapvirc


4. Lanciare il seguente comando seguito dalla nuova password al posto
   del campo ``_________``::

      smbpasswd -w _________

   Verrà restituito un output come il seguente::

      Setting stored password for "cn=admin,dc=appiano,dc=blz" in secrets.tdb

5. Modificare i seguenti file, sostituendo (2 volte) la
   ``vecchia-password`` con la ``nuova-password`` . ::

      vim /etc/smbldap-tools/smbldap_bind.conf


6. Lanciare il seguente comando e inserire (2 volte) la
   ``nuova-password``::

      /usr/sbin/smbldap-passwd admin

   ::

      Changing UNIX and samba passwords for admin
      * New password: ____________
      * Retype new password:____________


7. Modificare il seguente file sostituendo la ``password``::

      vim /etc/octofuss/octofuss.conf


8. Riavviare il servizio::

      service octofussd restart


9.

   a. Lanciare il seguente comando seguito dalla nuova password al posto
      del campo ``_________`` ::

         octofussd --reset-root-password ___________


      Verrà restituito un output come il seguente::

         System check identified some issues:
         WARNINGS:
         ?: (1_7.W001) MIDDLEWARE_CLASSES is not set.
         HINT: Django 1.7 changed the global defaults for the MIDDLEWARE_CLASSES. django.contrib.sessions.middleware.SessionMiddleware, django.contrib.auth.middleware.AuthenticationMiddleware, and django.contrib.messages.middleware.MessageMiddleware were removed from the defaults. If your project needs these middleware then you should configure this setting.
         Operations to perform:
         Apply all migrations: data
         Running migrations:
         No migrations to apply.


   b. Lanciare il seguente comando (ATTENZIONE: modalità "interattiva" di
      kerberos!)::

          kadmin.local

      ATTENZIONE: si entra in una modalità "interattiva" di kerberos e
      verrà visualizzata la seguente riga::

         kadmin.local:


   c. dopo la quale bisognerà copiare il seguente comando::

         cpw root/admin

      Alla fine sul terminale si dovrà visualizzare la seguente riga::

         kadmin.local: cpw root/admin


   d. Premere il tasto INVIO: verrà chiesta 2 volte la nuova password::

         * Enter password for principal ``root/admin@APPIANO.BLZ`` : ____________
         * Re-enter password for principal ``root/admin@APPIANO.BLZ`` : ____________

      a schermo poi si apre il seguente prompt::

         kadmin.local:


   e. Uscire da questa modalità interattiva digitando::

         exit


10. Editare il seguente file per sostituire la ``MASTER-PASSWORD``::

      vim /etc/fuss-server/fuss-server.yaml


Procedure facoltative consigliate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. Se si vuole che la ``master password`` coincida con la ``password di
   root``, aggiornare anche quest'ultima::

      passwd root

      Nuova password:_________

      Reimmettere la nuova password: ________

#. Si potrà poi andare nella history ed eliminare le tracce dei comandi
   contenenti la nuova password. La procedura è la seguente:

   1. Visualizzare la lista dei comandi ed il loro ``numero progressivo``
      lanciando il comando::

         history

   2. Eliminare lo specifico comando (poniamo che sia il numero 764 della
      lista) con::

         history -d 764


   3. Confermare le modifiche con::

         history -w

#. A questo punto eventualmente aggiornare il server.

Ripristino LDAP da un backup
----------------------------

Lo script di backup del pacchetto ``fuss-backup`` (vedi
:ref:`backup_fuss_backup`) si occupa di effettuare un dump dei dati
della directory LDAP, vedremo ora quale è la procedura per il suo
ripristino.

I dump del database LDAP vengono salvati da ``fuss-backup`` in
``/var/backups/slapd`` ad ogni esecuzione (cancellando quelli più vecchi) in
file compressi con ``gzip`` nella forma ``slapd-$DATA.ldif.gz*``, ma si
possono anche generare in qualunque momento con il comando ``slapcat >
backup.ldif``.

È preferibile ripristinare i dati del server a partire da un dump generato
con ``slapcat`` perché questo contiene tutte le informazioni presenti
nell'LDAP originale (compresi i metadati, quali ad esempio i timestamp delle
varie modifiche). La procedura di ripristino è la seguente::

  systemctl stop slapd.service
  rm -rf /var/lib/ldap/*
  slapadd < backup.ldif
  chown openldap:openldap /var/lib/ldap/*
  systemctl start slapd.service

(dove ``backup.ldif`` è il file del backup, se si usa uno dei dump di
``fuss-backup`` questo prima deve essere decompresso).

Troubleshooting LDAP
--------------------

L'utente non fa login su Windows
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cose banali da controllare prima:

-  l’utente esiste?
-  è stato fatto correttamente il join al dominio?
-  nome del dominio e dell’host sono corretti?
-  capslock?
-  cavo di rete? :-)

Potrebbe a questo punto essere un problema di LDAP, o meglio di come è
stato creato/modificato l’utente su LDAP Se windows vi dice che l’utente
è disabilitato, probabilmente il record ``sambaAcctFlags`` non è
impostato correttamente. Il flag D indica che l’utente è disabilitato,
un utente normalmente dovrebbe riportare il flag U o al massimo i flag
UX

Per correggere la situazione si usi il comando::

   smbldap-usermod -H U nomeutente

o si usi ``ldapvi``

Nel caso in cui gli utenti da migrare siano molti si può creare un file
``docenti.txt`` contenente l’elenco dei nomi utenti dei docenti, uno per
riga, e quindi usare il comando::

   for utente in $(cat docenti.txt); do smbldap-usermod -H U $utente; done


Troubleshooting DNS
-------------------

Mancata risoluzione di macchine reinstallate con lo stesso nome
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il fuss-server utilizza il DNS dinamico per creare automaticamente delle voci
con i nomi delle macchine all'interno della zona locale della scuola, con la
direttiva ``ddns-update-style standard`` in ``/etc/dhcp/dhcpd.conf``.  Il
meccanismo prevede che ogni volta che un client ottiene un indirizzo dinamico
dal DHCP venga inserita una voce con il suo nome (che viene invitato dal
client stesso, e corrisponde al suo hostname) all'interno della zona locale e
della zona inversa del DNS in modo da potercisi riferire direttamente per
nome.

I dati vengono inseriti nel file ``/var/cache/bind/db.local`` che contiene i
dati della zona DNS usata per la rete interna della scuola (quelli della
risoluzione interna in ``/var/cache/bind/db.192.168.XX.YY`` che dipende dal
range di indirizzi assegnati), dove compariranno delle voci nella forma::

  test-client             A       192.168.13.57
                          DHCID   ( AAIBP7QJ7mJsQfNKCJli4K991QOr0lDOCeqRUWvz1A1U
                                SUE= ) ; 2 1 32

La voce è composta dall'IP assegnato e da un campo ``DHCID`` che è un hash
identificativo del client (in genere hostname e MAC address, ma dipende dal
client stesso) che serve ad evitare che un altra macchina possa tentare di
intrufolarsi nel DNS assumendo lo stesso nome e che due macchine cui si è
assegnato (erroneamente) lo stesso nome si sovrascrivano reciprocamente la
voce sul DNS.

Questi record vengono in genere cancellati automaticamente al rilascio dell'IP
da parte del client, ma può capitare, quando questo non avviene correttamente
(ad esempio perché viene spento senza shutdown), che restino nel file. Di
norma non costituiscono un problema fintanto che non si reinstalla un client
con lo stesso nome, che avrà un DHCID diverso per cui non sarà in grado di
riutilizzare il nome, né di rimuovere la voce.

Una soluzione di emergenza può essere quella di aggiungere a
``/etc/dhcp/dhcpd.conf`` la direttiva ``update-conflict-detection no``
immediatamente sotto la precedente ``ddns-update-style standard``, questo
consente la riscrittura, ma comporta che i controlli di conflitti non ci sono
più, e si potranno ottenere situazioni in cui i problemi, manifestandosi in
forma casuale, sono molto più difficili da diagnosticare e
riconoscere. Pertanto è una soluzione di emergenza da non usare mai per più
dello stretto tempo necessario a risolvere un problema immediato, la soluzione
corretta è quella di rimuovere i record che danno il problema, con la
procedura illustrata di seguito.

Per la rimozione occorre modificare manualmente i file di zona citati in
precedenza (``/var/cache/bind/db.local`` e
``/var/cache/bind/db.192.168.XX.00``), ma dato che l'assegnazione è dinamica,
il contenuto di questi file non è detto sia aggiornato alla situazione
corrente (i dati temporanei sono mantenuti in forma binaria in corrispondenti
file ``.jnl``), ed inoltre i dati potrebbero ulteriormente aggiornati durante
la modifica, per cui prima di iniziare occorre "congelare" la situazione con
il comando::

  rndc freeze

che salva tutti i dati temporanei e blocca gli aggiornamenti del DNS da parte
del DHCP.

Si potranno a quel punto cercare dentro ``/var/cache/bind/db.local`` le voci
dinamiche da rimuovere analoghe a quella illustrata. Si tenga presente che in
alcuni casi queste sono introdotte da una riga del tipo::

  $TTL 3600       ; 1 hour

che indica il tempo di vita delle voci elencate di seguito (il valore indicato
può esser diverso a seconda delle impostazioni date al server DHCP). Questo
valore viene impostato, tutte le volte che varia, all'inizio di un blocco di
voci e si applica fino alla successiva reimpostazione, per questo può aiutare
a identificare le voci dinamiche rispetto a quelle statiche impostate in fase
di creazione del file con l'installazione del fuss server, che hanno un valore
di ``604800``.

Quando si rimuove una voce si abbia cura di toglierlo solo quando risulta
inutilmente replicato. Se si decide di fare una pulizia completa di tutte le
voci dinamiche va tolto evitando che vada ad applicarsi alle restanti voci
statiche.  In generale cancellare tutte le voci relative ad assegnazioni
dinamiche non è un problema (verranno ricreate al rinnovo o alla richiesta
successiva) ma la risoluzione dei nomi cancellati ovviamente diventerà
indisponibile fino ad allora. Per questo si consiglia di cancellare solo le
voci che contengono i nomi che danno problemi.

Si faccia inoltre attenzione a non cancellare o modificare invece le voci
"fisse" del file, come i nomi ``ns``, ``proxy``, ``octofuss`` ecc. ed in
generale tutti quelli che si sono inseriti manualmente nel file per le
assegnazioni statiche.

Una volta rimosse le voci da ``/var/cache/bind/db.local`` si cancellino le
voci corrispondenti con la risoluzione inversa in
``/var/cache/bind/db.192.168.XX.00``, che nel caso dell'esempio precedente
saranno qualcosa del tipo::

  57                      PTR     test-client.fusslab.blz.


Una volta completate le modifiche occorre aggiornare il seriale dei file di
zona (entrambi), per questo occorre cercare la riga identificata dal commento
``; serial`` nel record ``SOA`` che è all'inizio del file, ed aumentare di uno
il valore in essa indicata; se ad esempio 811 era il valore in
``/var/cache/bind/db.local`` precedente alle modifiche, si dovrà indicare al
suo posto 812, con qualcosa del tipo::

  $ORIGIN .
  $TTL 604800     ; 1 week
  fusslab.blz             IN SOA  ns.fusslab.blz. root.marcela.fusslab.blz. (
                                812        ; serial
                                604800     ; refresh (1 week)
                                86400      ; retry (1 day)
                                2419200    ; expire (4 weeks)
                                604800     ; minimum (1 week)
                                )


Fatto questo si potrà "scongelare" la zona con il comando::

  rndc thaw

e l'aggiornamento dinamico riprenderà a funzionare.


Mancata risoluzione di macchine con assegnazione statica
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Una delle funzionalità fornite dal fuss-server è quella di consentire delle
assegnazioni statiche (reservation) degli IP in base al MAC address delle
macchine. Queste vengono mantenute nel file
``/etc/dhcp/dhcp-reservations`` e gestite tramite ``octonet``.

Fino alla versione 10.0.13 ``octofussd`` si limita a gestire questa
assegnazione statica, le macchine elencate nel file non venivano inserite nel
DNS. Pertanto le macchine elencate ottengono un indirizzo IP (quello
selezionato dalla funzionalità) ma il loro nome resta assente dal DNS.

A partire dalla versione 10.0.14 è stato aggiunto il supporto per
l'inserimento (in fase di creazione) e la cancellazione (in fase di rimozione)
sul DNS delle macchine indicate per l'assegnazione statica. Non è ancora
disponibile il supporto per la modifica dei dati (pertanto in caso di
necessità si cancelli e si ricrei una voce).

Il supporto disponibile a partire dalla versione 10.0.14 riguarda però
soltanto le nuove voci, per quelle già presenti il DNS non viene
aggiornato. Per gestire questo caso (o se si sta mantenendo l'uso una versione
precedente la 10.0.14) le voci devono essere inserite o eliminate a mano.

L'operazione prevede la modifica manuale dei file di zona
(``/var/cache/bind/db.local`` e ``/var/cache/bind/db.192.168.XX.00``) che sono
gestiti in maniera dinamica; per questo (si riveda quanto già detto al
riguardo nella sezione precedente, prima di modificarli occorre "congelare" la
situazione con il comando::

  rndc freeze

a questo punto si potranno aggiungere i dati, in ``/var/cache/bind/db.local``
va messa la risoluzione diretta, usando un TTL adeguato, con qualcosa del
tipo::

  $TTL 604800     ; 1 week
  static1              A       192.168.0.100

(avendo cura di usare il nome e l'indirizzo che ci sono in
``/etc/dhcp/dhcp-reservations``) dove la voce del TTL può essere omessa
se la riga viene inserita sotto un blocco di definizione che ha all'inizio la
stessa.

La modifica va fatta anche nella zona inversa (in
``/var/cache/bind/db.192.168.XX.00``) con qualcosa del tipo::

  $TTL 604800     ; 1 week
  100                      PTR     static1.fusslab.blz.

e vale lo stesso avviso relativamente al TTL, fatte le modifiche occorrerà di
nuovo aumentare il seriale (si rimanda a quanto detto nella sezione
precedente) e poi si potrà "scongelare" la zona con il comando::

  rndc thaw

a questo punto le risoluzioni saranno disponibili.

A partire dal ``fuss-server`` 10.0.24 è disponibile lo script ``dnsreserv.py``
(in ``/usr/share/fuss-server/scripts``) che rilegge il contenuto di
``/etc/dhcp/dhcp-reservations`` ed esegue un DDNS update per tutti i
nomi ivi contenuti, questo consente di evitare l'intervento manuale per
reinserirli, ma la eventuale cancellazione di nomi spuri non viene eseguita e
in tal caso si deve ricorrere alla precedente procedura manuale.

Corruzione del repository dei backup
------------------------------------

Può accadere che, se il programma viene interrotto in maniera inopportuna, il
repository che mantiene i dati del backup risulti corrotto. In tal caso si
potranno ottenere degli errori (riportati nella mail riassuntiva inviata al
completamento di ogni backup) del tipo::

  borg.helpers.IntegrityError: Segment entry checksum mismatch [...]

In questo caso è necessario eseguire una verifica manuale, ed eventualmente
riparare il repository dei dati.

Per farlo è necessario montare il disco remoto su cui si fanno i backup,
questo si può fare (utilizzando i dati contenuti nella configurazione del
fuss-backup) con i comandi::

  . /etc/fuss-backup/fuss-backup.conf
  mount $DISK $BASEDIR

poi si potrà verificare lo stato del repository, con::

  borg check $BASEDIR/$BACKUP_DIR

e se questo riporta degli errori si potranno sistemare con::

  borg check --repair $BASEDIR/$BACKUP_DIR

accettando che si possano perdere dei dati (sono dati del backup, si possono
ripristinare facendone uno subito dopo la riparazione). Completata la
riparazione ci si ricordi di smontare il disco remoto con ``umount $BASEDIR``.
