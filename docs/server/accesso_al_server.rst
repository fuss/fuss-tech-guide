Accesso al server
=================

L'accesso al server è consentito, oltre che dalla console, via SSH,
anche per l'utente root, con autenticazione a chiavi. Le chiavi devono
essere installate nel file ``/root/.ssh/authorized_keys``.

Gli utenti normali possono accedere al server in SSH con username e
password con un loro utente solo se fanno parte del gruppo
``sshaccess``. Il gruppo viene creato come gruppo locale sul server
all'esecuzione di ``fuss-server`` ed SSH è configurato per l'accesso in
``/etc/ssh/sshd_config`` con la riga::

  AllowGroups root sshaccess

che consente l'accesso a root ed ai membri del gruppo.

Questa impostazione viene effettuata dal comando ``fuss-server`` con
usando una direttiva ``blockinfile`` di ansible, le eventuali modifiche
effettuate all'interno del blocco verranno sovrascritte ad una
successiva invocazione del comando. L'uso della direttiva
``AllowGroups`` è incompatibile con l'uso di ``AllowUsers`` che pertanto
non deve essere usata.

Per fornire l'accesso SSH agli utenti sarà sufficiente aggiungerli al
gruppo locale ``sshaccess`` con il comando: ``adduser utente
sshaccess``.

.. note:: se l'utente localadmin non riesce a entrare in SSH sul server
          controllate che appartenga al gruppo ``sshaccess``
