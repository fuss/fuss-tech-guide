Aggiornamenti
=============

Dalla versione 8.0 il funzionamento di fuss-server create è stato
cambiato ed è ora possibile lanciare più volte fuss-server create: le
modifiche già applicate rimangono costanti.

Aggiornamenti di sistema
------------------------

Gli aggiornamenti di sistema possono essere eseguiti manualmente con la
modalità ordinaria di Debian, vale a dire collegandosi al server come root ed
eseguendo i comandi::

    # apt update
    # apt upgrade

rispondendo positivamente alla richiesta di installazione dei pacchetti
aggiornati.

Aggiornamenti minori di fuss-server
-----------------------------------

Nel caso ci siano degli aggiornamenti di minor version (10.0.x →
10.0.(x+1)) di fuss-server per applicarli è sufficiente, da root:

* scaricare ed installare il pacchetto aggiornato::

     # apt update
     # apt install fuss-server

* riapplicare le nuove configurazioni::

     # fuss-server upgrade

Aggiornamenti dei file di configurazione
----------------------------------------

``fuss-server upgrade`` ripristina i contenuti di molti file di
configurazione ai valori desiderati per il fuss-server.

Per alcuni servizi per i quali è più comune dover mantenere delle
modifiche locali sono presenti dei file apposta, inclusi dal file
generale e non toccati da ``fuss-server``, ma questo non è vero in ogni
caso.

Tuttavia, quando un file di configurazione viene modificato ne viene
salvata una copia di backup nel formato ``<nome del file>.<un
numero>.YYYY-MM-DD@HH:MM:SS~``; da questo file si possono recuperare
eventuali personalizzazioni da ripristinare.

Per trovare i file modificati subito dopo il lancio di fuss-server si
può usare il comando::

   # find /etc -mmin -10

che trova tutti i file modificati negli ultimi 10 minuti.

Per trovare tutti i file di backup generati da ``fuss-server update``
nel 2020 si può invece usare::

   # find /etc/ -name "*.*.2020-*@*~"

o, per essere più precisi e trovare ad esempio le modifiche di giugno
2020::

   # find /etc/ -name "*.*.2020-06*@*~"

.. warning::

   Copiare semplicemente il vecchio file di configurazione su quello
   nuovo è una pratica sconsigliata: generalmente gli aggiornamenti
   introducono modifiche di tali file al fine di migliorare il
   funzionamento del fuss-server o risolvere bug, e annullare tali
   modifiche annulla tale vantaggio.

   Piuttosto è opportuno verificare le modifiche tra le due versioni del
   file, ad esempio con::

      # diff <nome file> <nome file backup>

   e riapplicare solo quanto effettivamente necessario per
   l'installazione locale.
