Pulizia delle Home
==================

Fuss-server imposta un cronjob notturno ``home-cleanup``, tutte le sere
alle 22:00, che effettua una pulizia delle home, cancellando i file::

   .config/xfce4/xfconf/xfce-perchannel-xml/displays.xml
   .cache/chromium/*
   .cache/google-chrome/*
   .cache/mozilla/firefox/*

di tutti gli utenti LDAP, ad eccezione di ``admin`` e ``nobody``, ed
ignorando gli utenti locali.

A partire dalla versione 10.0.33 l'orario di questo cronjob è
modificabile tramite variabili in
``/etc/fuss-server/fuss-server-defaults.yaml``::

   home_cleanup_time_hour: 22
   home_cleanup_time_minute: 0

