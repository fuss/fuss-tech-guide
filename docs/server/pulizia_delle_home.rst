Pulizia delle Home
===========================

Fuss-server imposta un cronjob notturno:

-  ``home-cleanup``, tutte le sere (/usr/share/fuss-server/scripts/home-cleanup)

-  ``home-cleanup-weekly``, il sabato sera (/usr/share/fuss-server/scripts/home-cleanup.weekly)

alle 22.00, che effettua una pulizia delle home, cancellando i file::

   .config/xfce4/xfconf/xfce-perchannel-xml/displays.xml
   .cache/chromium/*
   .cache/google-chrome/*
   .cache/mozilla/firefox/*

di tutti gli utenti LDAP, ad eccezione di ``admin`` e ``nobody``, ed
ignorando gli utenti locali.

A partire dalla versione 10.0.33 l'orario di questo cronjob è
modificabile tramite variabili in
``/etc/fuss-server/fuss-server-defaults.yaml``::

   home_cleanup_time_hour: 22
   home_cleanup_time_minute: 0


Pulizia di ulteriori file
-------------------------------------

Se si ha la necessità di cancellare con cadenza quotidiana o settimanale 
determinati file nelle home degli utenti, questi ultimi possono essere aggiunti 
in coda alla lista nei rispettivi script.

File Singleton* dei browser ``Google Chrome`` e ``Chromium``
*******************************************************************

Per eliminare dalle home i file `Singleton* <https://fuss-user-guide.readthedocs.io/it/latest/problemi-vari.html#problema-di-apertura-dei-browser-google-chrome-e-chromium>`_, che si creano
utilizzando i browser **Google Chrome** e **Chromium** ,
ma non vengono cancellati se si chiude la sessione senza 
aver chiuso prima i browser, impedendone l'apertura su un nuovo client,
si consiglia di aggiungere ad ``home-cleanup-weekly`` le righe: ::

   .config/chromium/Singleton*
   .config/google-chrome/Singleton*
   

