Modifiche della netmask della LAN
=================================

Su un fuss-server configurato è possibile modificare la netmask della rete
interna. Ma dato che varie configurazioni sono basate sulla netmask che si è
impostato in fase di ``fuss-server create`` (che sarà quella originale),
occorrerà seguire la seguente procedura per propagare correttamente i
cambiamenti a tutte le configurazioni, anche quando si sia solo cambiata la
netmask e non l'indirizzo IP del server (può comunque cambiare ad esempio la
reverse zone del DNS).

La modifica della configurazione della rete interna (IP e netmask) dipende da
come è stato installato il fuss-server, per una installazione manuale si deve
cambiare la configurazione della rete in ``/etc/network/interfaces`` o
``/etc/network/interfaces.d/``, impostando la nuova netmask ed il nuovo
indirizzo sulla LAN, e applicare le nuove impostazioni riavviando il server
(si può riavviare la rete, ma questa è la modalità più pulita).

Per una installazione dall'immagine cloud-init si *deve* eseguire la modifica
dall'interfaccia di Proxmox nella voce Cloud-init, modificando indirizzo e
netmask per l'interfaccia usata per la rete interna (in genere
net1). Riavviando la macchina la configurazione verrà applicata, ma si tenga
conto che verranno rigenerate la chiavi SSH, per cui occorrerà rimuovere le
precedenti e riaccettare le nuove, verificando le fingerprint che saranno
stampate sulla console al riavvio. Modificare a mano le configurazioni rischia
una riscrittura ad un successivo riavvio, con conseguenze spiacevoli.

Dato che durante l'operazione di modifica della netmask della rete interna la
configurazione del fuss-server non sarà coerente, è opportuno effettuarla
assicurandosi che non ci siano client attivi e collegati.

Una volta modificata la configurazione della rete del server si potrà cambiare
la configurazione in ``/etc/fuss-server/fuss-server.yaml``, è in genere
preferibile farlo manualmente modificando le voci ``localnet:`` per riflettere
la nuova configurazione della rete interna e ``dhcp_range:`` per il nuovo
range del DHCP. In ogni caso questi valori, qualora non compatibili con la
nuova impostazione della rete, verrebbero comunque richiesti alla successiva
esecuzione di fuss-server. Si può ricreare da zero il file con ``fuss-server
configure -r``, ma comunque lo si modifichi si abbia cura di non toccare
nessuno degli altri valori.

.. note::

   Senza modifiche a ``fuss-server.yaml`` verrà sempre richiesto all'inizio
   dell'esecuzione un nuovo valore per la rete locale, essendo cambiata la sua
   netmask; nel caso in cui gli indirizzi destinati al DHCP non siano compresi
   nella netmask selezionata, verrà chiesto anche un nuovo range.


L'esecuzione di ``fuss-server upgrade`` è sufficiente ad aggiustare tutte le
configurazioni del fuss server eccetto quelle relative alla risoluzione dei
nomi e degli indirizzi della rete interna che devono essere
resettate. Pertanto prima di eseguire il comando occorrerà cancellare i
relativi dati con::
  rm /var/cache/bind/db.*

A questo punto si potrà eseguire  ``fuss-server upgrade`` ed alla sua
conclusione riavviare la macchina per essere sicuri che tutti i servizi
ripartano con le nuove configurazioni. 

Si perderanno in questo modo tutti nomi registrati sul DNS dal DHCP; per i
client ordinari questi verranno reinseriti al loro avvio, occorrerà invece
ricaricare gli eventuali nomi ed indirizzi assegnati con le dhcp-reservation
eseguendo lo script ``/usr/share/fuss-server/scripts/dnsreserv.py``.

Si tenga presente però che cambiando l'IP del fuss-server, tutti i client con
``fuss-client`` fino alla versione 12.0.41 che sono stati collegati in
precedenza al server non funzioneranno più correttamente perché mantengono un
riferimento al precedente IP del server in moltissime configurazioni.

A partire dalla versione 12.0.42 del ``fuss-client`` sono state introdotte
delle modifiche che sostituiscono detto IP con il nome ``proxy`` del dominio
locale, la cui risoluzione viene sempre fornita dal server, e viene quindi
aggiornata al nuovo valore una volta completata la modifica della netmask. 

Nel caso i client siano rimasti con una versione del fuss-client inferiore
alla 12.0.42 è pertanto opportuno eseguire l'aggiornamento all'ultima
versione, ed eseguire poi un ``fuss-client -U`` prima di proseguire con il
cambio della netmask e dell'IP sulla rete interna del server. Di nuovo si
raccomanda di mantenere tutti i client spenti durante il suddetto cambiamento.
