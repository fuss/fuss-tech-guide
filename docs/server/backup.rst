.. _backup_fuss_backup:

Backup con fuss-backup
======================

Il pacchetto ``fuss-backup`` installa un nuovo programma di backup basato su
`borg <https://borgbackup.readthedocs.io/>`_ che fornisce al contempo anche le
funzionalità di dump dei database (LDAP, eventuali MySQL e Postgres)
installati sul server. Per questi ultimi viene creato un dump sotto
``/var/backups`` (rispettivamente nelle sottodirectory ``slapd``, ``mysql``,
``pgsql``) in cui vengono mantenute gli ultimi sette dump testuali per un
accesso immediato. La directory ``/var/backups`` è inserita di default nel
backup su disco esterno o NAS.

Pianificazione del backup
-------------------------

Per la pianificazione del backup viene installato dal pacchetto il file di
CRON ``/etc/cron.d/fuss-backup`` in cui è programmata l'esecuzione dello
script di backup ogni lunedì alle 13:15. Il backup può essere eseguito
manualmente (invocando il comando ``fuss-backup``) in qualunque momento.

Il dispositivo esterno viene montato prima dell'esecuzione del backup e
smontato al suo completamento. Questo consente, in caso di dischi esterni, una
rimozione ed eventuale rotazione.

Configurazione
--------------

Per il funzionamento del backup è richiesta la presenza di una configurazione
valida in ``/etc/fuss-backup/fuss-backup.conf``. In particolare deve essere
impostata la variabile ``START=yes`` e indicato nella variabile ``DISK`` il
dispositivo su cui viene eseguito lo stesso (maggiori dettagli in seguito).

.. warning:: l'installazione del pacchetto installa un file di configurazione
             che blocca l'esecuzione del backup (``START=no`` e ``DISK``
             vuota), occorre pertanto effettuare la configurazione manualmente
             in quanto non è noto a priori il nome del dispositivo esterno su
             cui si fanno i backup.

Variabili di configurazione
^^^^^^^^^^^^^^^^^^^^^^^^^^^

La tabella seguente riporta le variabili presenti nel file di configurazione
ed il relativo significato. Nel file sono state commentate con ulteriori
dettagli.

.. list-table::
   :widths: auto
   :header-rows: 1

   * - Variabile
     - Significato
     - Default

   * - START
     - se diversa da ``yes`` il backup non viene eseguito
     - no

   * - DISK
     - il dispositivo su cui viene effettuato il backup
     - vuoto

   * - PATHS
     - le directory di cui viene eseguito il backup (stringa separata da
       spazi)
     - ``/etc`` ``/home`` ``/var/backups`` ``/var/lib`` ``/var/log``
       ``/var/mail`` ``/var/local``

   * - MAILTO
     - indirizzi cui inviare l'email dei risultati
     - root

   * - RETENTION
     - numero minimo di backup (1 per giorno) da tenere in archivio (uso
       interno)
     - 7

   * - BASEDIR
     - directory dove montare il dispositivo di archiviazione
     - ``/mnt/backup``

   * - BACKUP_DIR
     - sotto-directory della precedente dove creare l'archivio
     - ``borgdata``

   * - RECOVERDIR
     - directory dove montare i backup per il recupero
     - ``/mnt/recover``

Esempio di configurazione
^^^^^^^^^^^^^^^^^^^^^^^^^

Per configurare ed attivare il backup modificando
``/etc/fuss-backup/fuss-backup.conf`` sono pertanto sono indispensabili i
seguenti passi:

* definire ``START=yes``
* definire un valore opportuno per ``DISK``, se ad esempio si dispone di un
  disco esterno se ne dovrà indicare il nome di dispositivo, ad esempio::

    DISK=/dev/sdc1

  se si dispone di un NAS occorrerà indicare l'indirizzo IP a cui è
  raggiungibile e la directory che questo esporta in NFS con la sintassi
  ``IP.DEL.NAS.LOCALE:/directory/esportata/dalnas``, ad esempio nel caso del
  NAS-QNAPTVS653 usato per le prove si avrà::

    DISK="192.168.10.5:/share/CACHEDEV1_DATA/Public"

Per trovare la directory esportata sul NAS-QNAPTVS653 ci si può collegare allo
stesso con SSH ed esaminare il contenuto del file ``/etc/exports``, il cui
contenuto riflette le directory definite nella sezione "Shared folders" del
pannello di controllo accessibile via WEB (nel caso illustrato quella
disponibile era Public).

Il formato del file ``/etc/exports`` prevede una directory esportata per
riga (se ne dovrà scegliere una qualora ne esistano diverse), con campi
separati da spazi; il primo campo è la directory da usare nella
variabile ``DISK`` (nel caso in esempio appunto
``/share/CACHEDEV1_DATA/Public``).


Come ulteriore configurazione si possono elencare le directory del server di
cui si vuole il backup modificando la variabile ``PATHS``, da specificare nella
forma di un elenco separato da spazi, ad esempio::

  PATHS="/etc /home /var/backups /var/lib /var/log /var/mail /var/local /opt/altridati"

È opportuno anche configurare un indirizzo di posta cui verranno inviate le
email con le notifiche del backup, sia predisponendo un opportuno alias per
l'utente ``root`` che indicando una lista di destinatari (sempre separata da
spazi) nella variabile ``MAILTO``, ad esempio::

  MAILTO="root localsysadmin@fuss.bz.it"

Infine qualora si voglia mantenere uno storico più consistente si può
aumentare il valore della variabile ``RETENTION`` che indica il numero minimo
di backup mantenuti (nel numero massimo di uno a giorno, nel caso in cui in
uno stesso giorno vengano eseguiti più backup verrà mantenuto solo il più
recente). Si tenga presente che ``borg`` supporta la deduplicazione, e che non
esiste il concetto di backup incrementale, ogni backup è sempre completo.

Esclusione
----------

I file che corrispondono al file di esclusione ``fuss-backup.exclude``
(installato di default per escludere MPT, AVI e immagini ISO) non vengono
inclusi nel backup. Se il file è assente il backup viene eseguito senza
escludere nulla.

Nella forma più elementare il formato del file di esclusione prevede un
pattern di shell (ex. ``*.iso``) per riga, i file il cui pathname corrisponde
al pattern vengono esclusi. Per maggiori dettagli (ed esempi di pattern più
complessi con uso di espressioni regolari) si rimanda alla documentazione
ottenibile con il comando ``borg help patterns``.

Il default installato dal pacchetto prevede che vengano esclusi dai backup
alcune tipi di file (identificati per estensione, in particolare ``*.iso``,
``*.mp3`` e ``*.avi``, e tutte le directory ``.cache`` che contenendo una
grande quantità di file possono rendere molto lenta l'esecuzione del backup.

In caso di necessità il programma può essere interrotto, quanto salvato da
``borgbackup`` fino all'ultimo checkpoint (di default sono ogni mezz'ora) non
verrà perso.


Ripristino
----------

Il ripristino può essere effettuato manualmente montando il disco (o la
directory condivisa via NFS sul NAS) che contiene il repository dei backup, ed
usando il comando ``borg`` per recuperare direttamente i file da uno dei
salvataggi. Si legga la documentazione dei comandi ``borg list`` (per elencare
i backup disponibili) e ``borg extract`` (per estrarre i dati) con i relativi
esempi qualora si voglia usare questa strada.

Per semplificare le operazioni il comando ``fuss-backup`` può essere
utilizzato in forma interattiva con i comandi ``fuss-backup mount`` e
``fuss-backup umount`` per montare e smontare i dati del backup in modo che
possano essere acceduti direttamente attraverso il filesystem.

Con ``fuss-backup mount`` viene montato il disco (o la directory NFS)
contenente il repository dei dati secondo quando configurato nella variabile
``BASEDIR`` (il default è ``/mnt/backup``) e poi viene reso disponibile via
FUSE il contenuto dei backup nella directory configurata dalla variabile
``RECOVERDIR`` (il default è ``/mnt/recover``).
In questo modo si può accedere direttamente ai file presenti nel backup resi
disponibili attraverso altrettante directory presenti sotto ``/mnt/recover``
nella forma ``fuss-server-DATA-ORA`` (data e ora fanno riferimento al momento
di esecuzione del backup), ad esempio si potrà avere qualcosa del tipo::

  root@fuss-server:~# ls /mnt/recover/ -l
  totale 0
  drwxr-xr-x 1 root root 0 gen 25 09:48 fuss-server-2017-01-20T17:39:56
  drwxr-xr-x 1 root root 0 gen 25 09:48 fuss-server-2017-01-25T09:40:51

per cui se si vogliono recuperare i file del backup del 20 Gennaio si dovranno
cercare i file sotto ``fuss-server-2017-01-20T17:39:56`` mentre per quelli del
25 si dovrà cercare sotto ``fuss-server-2017-01-25T09:40:51``.

Al di sotto di ciascuna directory apparirà poi l'albero delle directory
che si sono coperte con il backup secondo il valore della variabile
``PATHS``. L'albero delle directory viene riportato a partire dalla
radice per cui nel caso dei valori di default di ``PATHS`` illustrato in
precedenza, avremo sotto ``fuss-server-2017-01-25T09:40:51`` le
directory ``etc``, ``home`` e ``var``. A questo punto si potranno
copiare/esaminare i file dal backup navigando il filesystem sotto
``/mnt/recover`` con un qualunque strumento di gestione dei file.

Una volta completate le operazioni di recupero si deve procedere a smontare le
directory ed i dati, con il comando ``fuss-server umount``.

.. warning:: ci si ricordi sempre di eseguire il comando ``fuss-backup
 umount``, altrimenti l'esecuzione ordinaria dei backup fallirà.
