.. _fuss_zone:

FUSS Zone
=========

.. warning:: Pagina in elaborazione

``fuss_zone`` è un'impostazione di fuss-server introdotta nella versione
13 e parzialmente nella versione 12.0.33 che permette la configurazione
di FUSS Server e Client con parametri specifici per un particolare
gruppo di scuole.

Può essere usato ad esempio per impostare la raccolta di dati statistici
sui PC (client e server), o per fare riferimento a servizi specifici
disponibili per una determinata zona, come istanze di nextcloud o
rustdesk.

Configurazione e uso
--------------------

``fuss_zone`` viene impostata come variabile in
``/etc/fuss-server/fuss-server.yaml``; a partire dalla versione 13 di
fuss-server verrà chiesto di sceglierla in un elenco di valori noti (con
la possibilità di aggiungere un valore personalizzato).

Il valore viene poi pubblicato dal server all'indirizzo
http://proxy/fuss-data-conf/fuss-zone.yaml ; da cui è disponibile ai
FUSS Client.

Zone note
---------

L'elenco delle zone riconosciute da fuss-server, e le relative variabili
custom, sono disponibili nel file
``/usr/share/fuss-server/fuss_zones.yaml``, nel pacchetto fuss-server,
visibile online all'indirizzo
https://gitlab.fuss.bz.it/fuss/fuss-server/-/blob/master/ansible/fuss_zones.yaml.

Le variabili personalizzate per zona di fuss-client sono similmente
disponibili nel file ``/usr/share/fuss-client/fuss_zones.yaml``, nel
pacchetto fuss-client.

Per far aggiungere la propria zona all'elenco, è sufficiente farne
richiesta [qui verranno inserite le modalità consigliate per farlo].

Dati delle zone
---------------

La configurazione di fuss-server imposta esclusivamente la variabile
``fuss_zone``, le variabili custom per ciascuna zona sono mantenute in un
dizionario con l'elenco delle zone, in cui per ciascuna zona sono poi indicate
le variabili custom per essa definite ed il relativo valore. Al momento sono
utilizzate soltanto le seguenti variabili custom.

.. list-table::
   :header-rows: 1

   * - Variabile
     - Significato
     - Contenuto
     - Default (Prov-BZ)
   * - ``relay_config``
     - Abilita la configurazione di un relayhost per le email
     - Booleano
     - yes
   * - ``relay_server``
     - Indirizzo IP del server di relayhost
     - Stringa
     - 10.0.101.101
   * - ``notify_domain``
     - Dominio a cui vengono inviate le email di notifica
     - Stringa
     - schools.fuss.bz.it

Le seguenti variabili custom sono usate dal fuss-client

.. list-table::
   :header-rows: 1

   * - Variabile
     - Significato
     - Contenuto
     - Default (Prov-BZ)
   * - ``glpi_server``
     - Server a cui inviare dati di inventario delle macchine
     - Stringa
     - https://inventory.fuss.bz.it/
