************************
Gestione dei FUSS server
************************

.. toctree::
   :maxdepth: 2

   server/configurazione_server
   server/accesso_al_server
   server/principali_file_configurazione
   server/aggiornamenti
   server/backup
   server/pulizia_delle_home
   server/modifiche_della_netmask
   server/troubleshooting
   server/fuss_zone




..  LocalWords:  octofuss Fuss ansible variable separation yaml fuss upgrade
..  LocalWords:  configure defaults Dansguardian OctoNet DHCP client dhcp DNS
..  LocalWords:  conf reservation host nomeclient ethernet fixed address  eth
..  LocalWords:  Firewall firewall MANAGED udp tcp list table header rows apt
..  LocalWords:  restart Portal version root update install nslcd localhost
..  LocalWords:  workgroup windows images width align center SSH username NAS
..  LocalWords:  sshaccess AllowGroups blockinfile AllowUsers adduser BLOCK
..  LocalWords:  localadmin borg dump LDAP MySQL Postgres slapd mysql pgsql
..  LocalWords:  CRON yes warning PATHS home MAILTO RETENTION BASEDIR NFS iso
..  LocalWords:  borgdata RECOVERDIR Shared folders Public deduplicazione ls
..  LocalWords:  exclude shell pathname patterns repository extract mount xr
..  LocalWords:  umount filesystem drwxr gen var Reset passwd pass mpwchange
..  LocalWords:  sh gzip slapcat ldif nell'LDAP timestamp systemctl service
..  LocalWords:  rm rf slapadd chown openldap playbook Squid dans localnet
..  LocalWords:  guardian lease proxy win dansguardian squid all'acl acl url
..  LocalWords:  repositories regex dhcpd bind find mmin name diff borgbackup
..  LocalWords:  checkpoint ldappasswd Success vim smbpasswd Setting stored
..  LocalWords:  for secrets admin Changing and passwords Retype new reset
..  LocalWords:  octofussd System identified issues kadmin local kerberos cpw
..  LocalWords:  principal exit freeradius history Troubleshooting join txt
..  LocalWords:  dell’host capslock smbldap usermod nomeutente ldapvi cat MAC
..  LocalWords:  done ddns style hostname DHCID hash shutdown conflict rndc
..  LocalWords:  detection freeze hour weeks thaw octonet TTL static PTR
