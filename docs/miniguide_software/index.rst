******************
Miniguide software
******************

Questa sezione comprende guide su argomenti vari di utilità nelle
scuole relativi ad installazioni per hardware particolari.

.. toctree::
   :maxdepth: 2

   abilitare-invio-posta
   problemi-cd-dvd
   impostazioni-chromium
   installazione-iprase
   share-samba-sottocartelle
   fuss-cru
   ocsinventory
   
