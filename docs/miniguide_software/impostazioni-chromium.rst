Impostazioni chromium
=====================

Premessa
--------

La seguente miniguida è nata dall'esigenza di risolvere due problemi:

1. All'avvio del browser chromium viene richiesto
   insistentemente di inserire la password di sblocco del
   portachiavi
2. Diversi utenti vorrebbero che le credenziali di accesso al
   proxy venissero memorizzate senza dover essere digitate ogni
   volta.


Soluzione al problema 1
-----------------------

a. Da root

 Modificare il lanciatore di chromium::

   vim /usr/share/applications/chromium.desktop

 Modificare all'interno del file il comando che deve essere::

    Exec=/usr/bin/chromium -password-store=basic  %U


b. Da utente normale

- Creare un lanciatore trascinando l'icona del browser chromium dal menù
  al pannello e confermando.

- Cliccare col tasto destro sul lanciatore e selezionare ``Proprietà``.
  Si apre la finestra:

.. figure:: /images/impostazioni-chromium/finestra-avviatore.png

- Cliccare su ``Modifica l'elemento selezionato``

- Aggiungere alla riga Comando la stringa ``-password-store=basic``
  (eventualmente anche ``--incognito`` se si vuole navigare in modo
  "anonimo").

.. figure:: /images/impostazioni-chromium/modifica-avviatore.png


Soluzione al problema 2
-----------------------

- L'offerta di salvataggio delle credenziali proxy si presenta in modo
  imprevedibile e difficilmente replicabile; a volte si verifica
  cancellando la cartella ./config/chromium e deselezionando ``Accesso
  automatico`` in Impostazioni > Persone > Password.

- Il modo più semplice e certo di ottenere il risultato è quello di
  utilizzare un'estensione scaricabile da ``Web store`` che memorizza le
  credenziali proxy, che non verranno più richieste.

  Ovviamente l'utente deve ricordarsi di aggiornarle quando la password
  dell'account viene cambiata.
