Configurazione per l'uso dell'agent di OCS inventory
====================================================

L'agent può essere installato sulle macchine fisiche (quindi proxmox e i
client) all'interno della rete di una scuola. L'agent viene installato sui
client in maniera automatica da ``fuss-client`` (a partire dalla versione 11.0.22 per Fuss 11, 12.0.7 per Fuss 12) sia
in fase di collegamento del client al server, che in caso di aggiornamento.
È necessario che sia presente sul FUSS server la corretta configurazione, da
impostare con le modalità che illustreremo più avanti.

Per installarlo su Proxmox, o su qualunque altra macchina (Debian)
raggiungibile dal FUSS Server, occorrerà invece lanciare sullo stesso
il playbook ``ocs-agent-inst`` (installato in
``/usr/share/fuss-server/``) che viene fornito con il pacchetto
``fuss-server`` a partire dalla versione 12.0.10 ma disponibile anche
per server FUSS 10.
Lo script è usabile anche per un client, ma è preferibile evitarne
l'uso, dato che la configurazione viene comunque generata da
``fuss-client``, in modo leggermente diverso, per essere aggiornabile
automaticamente da future versioni del pacchetto.

Per poter funzionare, l'agent ha bisogno di raggiungere via rete la macchina su
cui è ospitato il server di OCS inventory; questo viene contattato via web e
pertanto è necessario come primo passo abilitarne la raggiungibilità. Questa
operazione deve essere fatta preliminarmente sul FUSS server aggiungendo una
opportuna regola di accesso per il proxy, che consenta di arrivare al server
OCS inventory senza necessità di autenticazione; allo scopo basterà aggiungere
al file ``/etc/squid/squid-added-repo.conf`` la riga::

  acl repositories url_regex ocs.inventory.server

dove ``ocs.inventory.server`` è l'indirizzo del server OCS inventory che si
intende utilizzare. Dopo la modifica si dovrà eseguire un reload di
``squid`` con ``systemctl reload squid``.

Il passo successivo prevede di:

   - (se il certificato del server è autofirmato), scaricarlo dal
     sito, nominarlo ``server.crt`` e copiarlo sul server FUSS nella
     cartella ``/var/www/fuss-data-conf/``
  
   - creare nella stessa cartella il file ``ocsinventory.yml`` (con
     sintassi YAML) contenente i paramenti di configurazione da usare
     successivamente sui client. Questo file verrà poi scaricato su
     ciascun client al momento del lancio del ``fuss-client``.

Il file prevede la definizione di tre variabili, le prime due sono
obbligatorie, la terza è opzionale:

   -  ``ocs_server`` : l'indicazione della URL del server, ad esempio https://ocs.inventory.server/ocsinventory

   -  ``ocs_tag`` : il tag che identifica la scuola, ad esempio il codice (LASIS) numerico della scuola

   - ``ocs_cert`` : (opzionale) il nome del certificato da usare per
      la connessione SSL (necessario solo se il server ha un
      certificato autofirmato); se definita detto file deve essere
      installato sotto ``/var/www/fuss-data-conf/`` .

Un esempio di questo file è il seguente::

  # template for ocsinventory variables needed for fuss-client
  ocs_server: https://ocs.inventory.server/ocsinventory
  ocs_tag: change-me
  ocs_cert: server.crt

Per evitare che un aggiornamento eseguito su un client durante la
configurazione trovi un file ``ocsinventory.yml`` scritto
parzialmente, con conseguenti errori, si suggerisce di crearlo in un
altra directory, spostandolo a destinazione una volta completo
(insieme al file del certificato preventivamente caricato sul server,
quando necessario) con::

  mv ocsinventory.yml /var/www/fuss-data-conf/

.. note:: si consiglia, se possibile, di configurare il server di OCS inventory
          con un certificato valido (ad esempio usando Let's Encrypt) evitando
          l'uso di un certificato autofirmato; in tal caso la variabile
          ``ocs_cert`` non è necessaria, come non lo è il certificato.

A questo punto, per l'installazione dell'agent è sufficiente eseguire
``fuss-client``.  Il file di configurazione (``ocsinventory.yml``),
quando presente, viene scaricato nell'esecuzione del comando (sia
quando su usa ``-a`` o ``-H`` per collegare un nuovo client, che
quando si usa ``-U`` per un aggiornamento), e viene salvato in
``/etc/fuss-client/``. Quando tale file viene trovato, viene poi
eseguita la configurazione e l'installazione di
``ocsinventory-agent``. Se invece il file non viene trovato sul
server, il programma prosegue senza installare nulla.

Nel caso in cui ci si dimenticasse di copiare sul server FUSS il
certificato ``server.crt`` prima di aver lanciato il ``fuss-client``,
si può correggere il problema (dopo aver copiato il certificato sul
server) rilanciando il comando ``fuss-client -U``.


Installazione di ocs agent su una macchina generica (non joinata)
------------------------------------------------------------------------------
Per installare l'agent su una **macchina generica** si può invece
utilizzare il playbook ``ocs-agent-inst`` presente sul FUSS
server. Questo esegue l'installazione di ``ocsinventory-agent`` su una
macchina generica da indicare come parte dell'inventario Il comando
deve cioè essere eseguito sul FUSS server come::

  /usr/share/fuss-server/scripts/ocs-agent-inst -i <client.address.or.ip>,

avendo cura di mettere una virgola dopo l'indirizzo del client in modo
che venga riconosciuto come elenco di indirizzi e non come nome di file.

Si tenga presente che il playbook richiede l'accesso via SSH alla macchina
indicata; qualora non si abbia un accesso diretto con le chiavi, occorrerà richiedere
l'uso della password (di root) aggiungendo l'opzione ``-K``.

Se si vuole installare l'agent su di un **server FUSS**, il comando da
eseguire è::

  /usr/share/fuss-server/scripts/ocs-agent-inst --connection=local -i 127.0.0.1,
  
