Installazione del pacchetto giochi iprase
===========================================

Premessa
--------
È una raccolta di giochi didattici per bambini della scuola
dell’obbligo. IPRASE, aderendo al progetto sperimentale nell’ambito
delle attività a cofinanziamento del Fondo Sociale Europeo sulla
Didattica assistita dalle nuove tecnologie, ha realizzato una
sperimentazione nella scuola dell’obbligo per tre anni consecutivi a
partire dall’anno scolastico 2001-2002. Obiettivo dell’iniziativa era
quello di fornire strumenti utili all’introduzione delle nuove
tecnologie nella didattica e nella prassi di lavoro quotidiana dei
docenti. La sperimentazione era rivolta a insegnanti ed alunni della
scuola elementare e della scuola media. Si proponeva l’utilizzo di
giochi ed eserciziari da fare al computer e riguardanti conoscenze ed
abilità nelle seguenti discipline: Italiano, Geografia, Matematica.

Nonostante siano trascorsi diversi anni, i giochi sono ancora molto
apprezzati. Il loro uso e pacchettizzazione è stato consentito.

Aggiunta dell'architettura i386
--------------------------------

Per funzionare, l'applicazione (basata su eseguibili .exe), richiede
l'utilizzo di wine e l'architettura i386, che si può aggiungere con il
comando::

    dpkg --add-architecture i386

Per aggiornare la cache coi pacchetti i386 si lancia::

    apt update


Infine si installa l'applicazione con::

    apt install iprase

Installazione in locale con pacchetto .deb
------------------------------------------

Nel caso si sia scaricato in locale il pacchetto iprase_1.1.deb,
aggiungere l'architettura i386 e dopo apt update e apt dist-upgrade è
sufficiente lanciare::

    dpkg -i iprase_1.1.deb
