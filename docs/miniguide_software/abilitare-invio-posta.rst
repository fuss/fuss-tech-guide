Configurazione del fuss-server per abilitare l'invio di posta
=============================================================

La spedizione diretta di messaggi di posta elettronica dalle macchine della
rete interna (LAN) del Fuss Server è disabilitata per motivi di sicurezza.
Qualora per esigenze specifiche (ad esempio l'uso della funzionalità di "scan
to mail" delle stampanti multifunzione), sia necessario consentire ad una
macchina o un apparato di inviare posta, si possono utilizzare le seguenti
istruzioni per potersi appoggiare al Fuss Server per l'invio.

Sul Fuss Server è infatti installato il server SMTP Postfix, che però è
configurato per accettare posta da inviare solo da ``localhost``. La direttiva
``mynetworks``, che controlla quali macchine possono inviare posta passando
dal server, si trova nel file ``/etc/postfix/main.cf`` ed il suo valore di
default è il seguente::

  mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128

Per consentire ad altre macchine o apparecchiature come una stampante
multifunzione di inviare posta occorre anzitutto avere l'indirizzo IP; si deve
inoltre avere cura di impostatalo in maniera statica in modo che non cambi ad
un eventuale riavvio della stampante.

Una volta che l'indirizzo IP sia noto, occorrerà aggiungerlo all'elenco di
quello consentiti da ``mynetworks``. La direttiva, come dice il nome richiede
una lista di reti pertanto se si indica un solo IP occorrerà usare la
notazione CIDR aggiungendo il suffisso ``/32``. L'elenco delle reti deve
essere fornito come lista separata da spazi, si può andare a capo e scriverlo
su più righe, avendo cura di iniziare ogni riga di estensione con degli
spazi.

Per esempio se si hanno stampanti multifunzione che utilizzano lo "scan to
mail" con indirizzi IP ``192.168.0.10``, ``192.168.0.15``, e ``192.168.0.20``
per consentirgli di spedire posta verso l'esterno passando dal Fuss Server
occorrerà modificare la precedente configurazione in::

  mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
     192.168.0.10/32 192.168.0.15/32 192.168.0.20/32

e riavviare il servizio con ``service postfix restart``.

A questo punto si potrà configurare la stampante (o altro apparato) indicando
come server SMTP il fuss server stesso.

.. note:: si consiglia per sicurezza di usare l'indirizzo IP del server, che
          continuerà a funzionare anche qualora la stampante non sia in grado
          di risolvere i nomi assegnati alle macchine sulla rete interna.
