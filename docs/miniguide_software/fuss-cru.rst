Aggiornamento dei fuss-client tramite fuss-cru
==============================================

Per aggiornare i fuss-client da una versione fuss alla successiva si può
usare il pacchetto ``fuss-cru``. Le versioni 11.* di tale pacchetto
supportano l'aggiornamento da fuss 10 (buster) a fuss 11 (bullseye).

.. warning:: Non è supportato l'aggiornamento di massa di un'aula da
   fuss 11 a fuss 12 (bookworm), per il quale si raccomanda invece la
   reinstallazione tramite fuss-fucc / clonezilla.

* Innanzitutto assicurarsi di aver aggiornato ``fuss-server`` almeno
  alla versione 10.0.45, e di aver lanciato ``fuss-server upgrade``:
  questo è necessario per abilitare il caching dei pacchetti debian e
  rendere più veloce l'aggiornamento.
* Installare sul fuss-server il pacchetto ``fuss-cru``; questo creerà
  automaticamente un nuovo script in octonet.
* aggiungere un'esecuzione dello script clientupgrade su octonet e
  programmarla come di consueto.

.. note::

   Ricordarsi che ``octonet-client`` cerca gli script da eseguire al
   boot ed ogni 5 minuti, e quindi l'esecuzione non è istantanea.
