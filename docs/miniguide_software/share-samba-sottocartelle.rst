Share Samba con struttura di sottocartelle
==========================================

La struttura di share Samba supportata da OctoNet permette solo una
lista piatta di condivisioni.

In casi particolari può essere necessaria una struttura gerarchica, come
ad esempio la seguente, in cui ci sono due cartelle, ``docenti`` e
``alunni``, e:

* alla cartella docenti può accedere solo chi appartiene al gruppo
  docenti;
* la cartella alunni ha al suo interno le cartelle di classe a cui
  possono accedere:

  * gli alunni appartenenti al gruppo classe, alla sola cartella della
    loro classe;
  * i docenti, alle cartelle delle classi al cui gruppo appartengono
    (come assegnazione di gruppo secondario da OctoNet).

In questo caso è necessario creare delle cartelle con i seguenti
permessi sul filesystem::

   root@serverlnx:~# ls -l /home/SAMBA/pubblica/ -a
   totale 16
   drwxrwsr-x 4 root insegnanti 4096 mar 12 15:41 .
   drwxr-xr-x 9 root root       4096 mar 12 15:41 ..
   drwxrwsr-x 5 root insegnanti 4096 mar 12 15:41 alunni
   drwxrws--- 2 root insegnanti 4096 mar 12 15:41 docenti

(``2775`` per le directory cui devono accedere anche gli alunni
(``/home/SAMBA/pubblica/`` e ``/home/SAMBA/pubblica/alunni``) e ``2770``
per docenti dove non devono arrivare); e::

   root@serverlnx:~# ls -l /home/SAMBA/pubblica/alunni/
   totale 12
   drwxrws--- 2 root 1a 4096 mar 12 15:41 1A
   drwxrws--- 2 root 1b 4096 mar 12 15:41 1B
   drwxrws--- 2 root 1c 4096 mar 12 15:41 1C

(permessi ``2770`` e gruppo assegnato alla classe).

La configurazione di samba, da aggiungere in ``shares.conf`` sarà
allora::

   [pubblica]
   path = /home/SAMBA/pubblica
   guestok = no
   readonly = no
   create mask = 0660
   directory mask = 2770
   readlist = @Domain+Users
   writelist = @insegnanti
