Problemi con CD e DVD
=====================

Montaggio dei CD-ROM
--------------------

Problema nel montaggio
^^^^^^^^^^^^^^^^^^^^^^

I CD-ROM vengono montati in automatico in ``/media/cdrom0``.
Questo è dovuto al fatto che in ``/etc/fstab`` si trova la seguente riga::

  /dev/sr0       /media/cdrom0   udf,iso9660 user,noauto     0       0

Per alcuni DVD didattici, come ad esempio i DVD della Zanichelli, questo
è un problema serio, perchè lo script che viene lanciato richiede
rigidamente che il DVD sia montato in ``/media/<nome-utente>``.

Inoltre, il contenuto del CD non ha l'utente come proprietatrio, e tutti
i file e le directory sono assegnate a ``nobody:nogroup`` (cioè non sono
assegnati).

Soluzione del problema
^^^^^^^^^^^^^^^^^^^^^^

Per poter fare ciò, bisogna fare in modo che in ``/etc/fstab`` non venga
letta la riga sopra indicata.

Lo script seguente risolve il problema::

    #! /bin/sh
    if [ ! -f /etc/fstab-old ]
    then
        cp /etc/fstab /etc/fstab-old
    fi
    sed -i 's/^\/dev\/sr0/#\/dev\/sr0/' /etc/fstab

Riavviato il PC il DVD-ROM è montato in ``/media/<nome-utente>/<nome-cd>``

DVD Zanichelli
--------------

I DVD della Zanichelli della serie “Idee per insegnare con il
digitale” si possono vedere con la nostra distribuzione, a patto di
installare prima alcuni pacchetti, da cui dipende l'esecuzione del
programma.

Lo script che segue permette l'installazione dei pacchetti che servono::

    #! /bin/sh
    echo "deb http://ftp.de.debian.org/debian/ jessie main" >>/etc/apt/sources.list
    apt update && apt upgrade
    apt install libpng12-0 gstreamer0.10-fluendo-mp3 gstreamer1.0-alsa gstreamer0.10-alsa

Aprire un terminale e come utente, digitare il seguente comando::

    sh /media/<nome-utente>/<nome-cd>/BooktabZ-StartCD_lnx.sh

Installazione dei DVD-ROM Oxford su FUSS 9
------------------------------------------

Premessa
^^^^^^^^

La seguente procedura è valida solo per macchine a 64 bit in quanto il
software non è compatibile con Debian 9 a 32 bit.

E' stata elaborata per i DVD-ROM della serie ``New Treetops`` ma è
valida anche per altri prodotti Oxford come ``High Five`` .

Problema nel montaggio
^^^^^^^^^^^^^^^^^^^^^^

I DVD-ROM vengono montati in automatico in ``/media/cdrom0`` e di
default non sono eseguibili.
Questo è dovuto al fatto che in ``/etc/fstab`` si trova la seguente riga::

  /dev/sr0       /media/cdrom0   udf,iso9660 user,noauto     0       0

Soluzione del problema
^^^^^^^^^^^^^^^^^^^^^^

Bisogna fare in modo che il DVD-ROM venga rimontato in modalità eseguibile.
Inserire il cdrom e da terminale lanciare da root::

  mount -o remount,exec,ro  /media/cdrom0

Installazione
^^^^^^^^^^^^^

Per installare, entrare in ``/media/cdrom0`` e lanciare l'eseguibile
``setup-linux-x64`` che apre il wizard di installazione.
Terminata l'installazione, cliccare su ``Finish`` nel Wizard e su ``Ok``
nella finestra README. A questo punto si può smontare il DVD-ROM.

Avvio del programma
^^^^^^^^^^^^^^^^^^^

Nella home di chi installa viene creata una cartella ``~/Oxford
University Press/`` che contiene una sottocartella per ciascun DVD-ROM
della serie.

Per avviare ad esempio il secondo volume si segue il percorso::

   ~/Oxford University Press/New Treetops 2a/linux-x64/ # Ovviamente inserire i backslash se si lavora da terminale.

e si lancia l'eseguibile ``oup`` (nel volume 3 si lancia ``New Treetops 3a``).

Installazione adobe-flashplugin
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La visualizzare dei video dell'applicazione richiede adobe-flashplugin.
Fortunatamente durante l'installazione viene creata una sottocartella
nascosta (~/Oxford University Press/New Treetops
3a/.flash_installers/linux-x64/) che contiene alcuni pacchetti debian di
adobe-flashplugin.
Se è presente, rimuovere flashplayer-mozilla::

   apt remove flashplayer-mozilla

Installare uno dei pacchetti adobe-flashplugin (è stato testato il file
del comando che segue)::

   dpkg -i adobe-flashplugin_11.2.202.280-0precise1_amd64.deb

Cartella condivisa
^^^^^^^^^^^^^^^^^^

La cartella contenente le installazioni dai DVD-ROM può essere copiata
in una cartella condivisa dai docenti ed eventualmente dagli alunni.
Il proprietario della cartella è preferibile sia root per evitare
cancellazioni involontarie, mentre il gruppo proprietario deve avere
solo permessi di lettura ed esecuzione.
In alternativa si lascia permesso di lettura ed esecuzione a tutti.

Eventuale lanciatore da scrivania
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Se si preferisce lanciare le applicazioni da scrivania si possono creare dei lanciatori::

   1) Cliccare col tasto destro

   2) Selezionare ``Crea avviatore``

   3) Scegliere un nome ed il comando con l'accortezza di inserire i backslash prima degli spazi vuoti.
      Ad esempio il comando per lanciare New Treetops 2a sarà::

      ~/Oxford\ University\ Press/New\ Treetops\ 2a/linux-x64/oup

   sostituendo alla tilde ~ il percorso completo della home dell'utente o della cartella condivisa.
